## Project Name

dict_buddy

## Description

dict_buddy is a dictionary app written in C++17 based on Qt and [lexicon_buddy](https://bitbucket.org/intermittence/lexicon_buddy). It supports mdict and stardict dictionary files index, prefix search and query. The Merriam-Webster [Merriam-Webster's Collegiate® Dictionary with Audio](https://www.dictionaryapi.com/products/api-collegiate-dictionary) and [Merriam-Webster's Collegiate® Thesaurus](https://www.dictionaryapi.com/products/api-collegiate-thesaurus) JSON APIs are also supported.

## Platform

- macOS Mojave
- (todo Windows 10)

## Dependency

- Qt 5.13 ([LGPL](http://doc.qt.io/qt-5/lgpl.html))
- (todo make BASS optional) BASS audio library (not free, [Licensing](https://www.un4seen.com/))
- [lexicon_buddy](https://bitbucket.org/intermittence/lexicon_buddy) ([dependency](https://bitbucket.org/intermittence/lexicon_buddy/src/master/README.md) and [License](https://bitbucket.org/intermittence/lexicon_buddy/src/master/LICENSE))

## Screenshots
prefix_search:
![prefix_search](doc/prefix_search.png)
offline example:
![eng](doc/eng.png)
offline example:
![cjk](doc/cjk.png)
Merriam-Webster's Collegiate® Dictionary with Audio (JSON API), click 🔈 to play pronunciation:
![coll](doc/coll.png)
Merriam-Webster's Collegiate® Thesaurus (JSON API):
![thes](doc/thes.png)
dictionary management
![management](doc/management.png)

## Build Instruction

todo Read the [instruction](Build/Build_Instruction.md).

### Author and Contact

cqyzzhgl@gmail.com