# ====================================================
# function for get shared library version on mac
# ====================================================
# ++++++++++ parameter ++++++++++
# lib_path: full path of shared library
function(mac_shared_lib_version lib_path compat_ver cur_ver ver_found)
	execute_process(COMMAND otool "-L" "${lib_path}" OUTPUT_VARIABLE otool_STDOUT)
	# ===================================================
	# output example of static library, just ignore
	# Archive : /usr/local/lib/liblzo2.a
	# /usr/local/lib/liblzo2.a(lzo1.o):
	# /usr/local/lib/liblzo2.a(lzo1_99.o):
	# ......
	
	# output example of shared library
	# /usr/local/lib/libcryptopp.dylib:
	# 	/usr/local/opt/cryptopp/lib/libcryptopp.dylib (compatibility version 8.1.0, current version 8.1.0)
	# 	/usr/lib/libc++.1.dylib (compatibility version 1.0.0, current version 400.9.4)
	# 	/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 1252.200.5)
	# ===================================================
	# find 1st "compatibility version"
	string(FIND ${otool_STDOUT} "compatibility version" p0)
	# find 1st "current version"
	string(FIND ${otool_STDOUT} "current version" p1)
	# find first ")"
	string(FIND ${otool_STDOUT} ")" p2)

	if((${p0} EQUAL -1) OR (${p1} EQUAL -1) OR (${p2} EQUAL -1))
		set(${ver_found} FALSE PARENT_SCOPE)
	else()
		math(EXPR compat_ver_p "${p0}+22")
		math(EXPR compat_ver_len "${p1}-${p0}-22-2")
		math(EXPR cur_ver_p "${p1}+16")
		math(EXPR cur_ver_len "${p2}-${p1}-16")
		string(SUBSTRING ${otool_STDOUT} ${compat_ver_p} ${compat_ver_len}  compat_ver__)
		string(SUBSTRING ${otool_STDOUT} ${cur_ver_p} ${cur_ver_len}  cur_ver__)
		set(${compat_ver} ${compat_ver__} PARENT_SCOPE)
		set(${cur_ver} ${cur_ver__} PARENT_SCOPE)
		set(${ver_found} TRUE PARENT_SCOPE)
	endif()
endfunction()

function(VERSION_2_SOVERSION VERSION SOVERSION)
	string(FIND ${VERSION} "." len)
	if(NOT (${len} EQUAL -1))
		string(SUBSTRING ${VERSION} "0" ${len} SOVERSION__)
		set(${SOVERSION} ${SOVERSION__} PARENT_SCOPE)
	endif()
endfunction()

# helper macro for find_library_target
macro(add_imported_lib CNAME)
	add_library(${CNAME}::${CNAME} UNKNOWN IMPORTED GLOBAL)
	set_target_properties(${CNAME}::${CNAME} PROPERTIES
		IMPORTED_LOCATION "${${CNAME}_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${${CNAME}_INCLUDE_DIR}"
	)
	# set shared lib version if it's a shared lib on APPLE
	if(APPLE)
		mac_shared_lib_version(${${CNAME}_LIBRARY} compat_v cur_v ${CNAME}_ver_found)
		if(${${CNAME}_ver_found})
			VERSION_2_SOVERSION(${cur_v} so_v_)
			set_target_properties(${CNAME}::${CNAME} PROPERTIES
				VERSION ${cur_v}
				SOVERSION ${so_v_}
			)
		endif()
	endif()
endmacro()

# helper function for find_library_target
macro(find_library_target_report CNAME)
	get_property(${CNAME}_ver____ TARGET ${CNAME}::${CNAME} PROPERTY VERSION)
	if(${CNAME}_ver____)
		message("-- ${CNAME}::${CNAME} version ${${CNAME}_ver____} found")
	else()
		message("-- ${CNAME}::${CNAME} found")
	endif()
endmacro()

# function find_library_target may use PkgConfig
find_package(PkgConfig)
# ====================================================
# function for finding library (single configuration)
# ====================================================
# ++++++++++ function parameter ++++++++++
# header_filename: an actual library header file at root of include directory, used to verify include directory
# ++++++++++ hints and search results ++++++++++
# if search failed with default configuration, 
# user can provide cache ${CNAME}_ROOT (work only on UNIX-like system, CNAME = Library Canonical Name) 
# or <${CNAME}_INCLUDEDIR ${CNAME}_LIBRARYDIR> as hints for search location.
# this routine will save search results as cache variables ${CNAME}_INCLUDE_DIR and ${CNAME}_LIBRARY. 
# ++++++++++ search priority ++++++++++
# 1 user hints of <${CNAME}_INCLUDEDIR ${CNAME}_LIBRARYDIR>
# 2 user hints of ${CNAME}_ROOT
# 3 using pkg-config
# 4 search on platform default location
# todo impl version and "REQUIRED" features
function(find_library_target CNAME library_file_basename header_filename) 
	message("-- searching ${CNAME}")
	set(${CNAME}_SEARCH_LOCATION_READY FALSE)
	set(${CNAME}_FOUND_LOCAL FALSE)
	set(${CNAME}_FOUND FALSE PARENT_SCOPE)
	# check last search results first
	if(${CNAME}_INCLUDE_DIR AND ${CNAME}_LIBRARY)
		add_imported_lib(${CNAME})
		set(${CNAME}_FOUND_LOCAL TRUE)
		set(${CNAME}_FOUND TRUE PARENT_SCOPE)
		find_library_target_report(${CNAME})
		return()
	endif()
	# check existence of <${CNAME}_ROOT | <${CNAME}_INCLUDEDIR ${CNAME}_LIBRARYDIR>>
	# and prepare the library and include search location
	if(${CNAME}_INCLUDEDIR AND ${CNAME}_LIBRARYDIR)
		set(${CNAME}_INCLUDEDIR__ ${${CNAME}_INCLUDEDIR})
		set(${CNAME}_LIBRARYDIR__ ${${CNAME}_LIBRARYDIR})
		set(${CNAME}_SEARCH_LOCATION_READY TRUE)
	elseif(${CNAME}_ROOT)
		if(UNIX)
			file(TO_CMAKE_PATH ${${CNAME}_ROOT} ${CNAME}_ROOT__)
			set(${CNAME}_INCLUDEDIR__ "${${CNAME}_ROOT__}/include")
			set(${CNAME}_LIBRARYDIR__ "${${CNAME}_ROOT__}/lib")
			set(${CNAME}_SEARCH_LOCATION_READY TRUE)
		else()
			# todo this do not work on win32?
		endif()
	endif()
	if(${CNAME}_SEARCH_LOCATION_READY)
		find_path(
			${CNAME}_INCLUDE_DIR
			NAMES "${CNAME}/${header_filename}"
			PATHS ${${CNAME}_INCLUDEDIR__}
			#PATH_SUFFIXES ${CNAME}
		)
		find_library(
			${CNAME}_LIBRARY
			NAMES ${library_file_basename}
			PATHS ${${CNAME}_LIBRARYDIR__}
		)
		if((NOT (${CNAME}_INCLUDE_DIR STREQUAL "${CNAME}_INCLUDE_DIR-NOTFOUND"))
			AND
			(NOT (${CNAME}_LIBRARY STREQUAL "${CNAME}_LIBRARY-NOTFOUND")))
			add_imported_lib(${CNAME})
			set(${CNAME}_FOUND_LOCAL TRUE)
			set(${CNAME}_FOUND TRUE PARENT_SCOPE)
			find_library_target_report(${CNAME})
		else()
			message(SEND_ERROR "-- ${CNAME} search failed\n")
		endif()
	else()
		# try pkg-config
		if(PKG_CONFIG_FOUND)
			# message("-- using pkg_check_modules searching ${CNAME}")
			pkg_check_modules(PC_${CNAME} QUIET ${CNAME})
			if(PC_${CNAME}_VERSION)
				find_path(
					${CNAME}_INCLUDE_DIR
					NAMES "${CNAME}/${header_filename}"
					PATHS ${PC_${CNAME}_INCLUDE_DIRS}
					#PATH_SUFFIXES ${CNAME}
				)
				find_library(
					${CNAME}_LIBRARY
					NAMES ${library_file_basename}
					PATHS ${PC_${CNAME}_LIBRARY_DIRS}
				)
				if((NOT (${CNAME}_INCLUDE_DIR STREQUAL "${CNAME}_INCLUDE_DIR-NOTFOUND"))
					AND
					(NOT (${CNAME}_LIBRARY STREQUAL "${CNAME}_LIBRARY-NOTFOUND")))
					set(${CNAME}_FOUND_LOCAL TRUE)
					set(${CNAME}_FOUND TRUE PARENT_SCOPE)
					# set(${CNAME}_VERSION ${PC_${CNAME}_VERSION})
					VERSION_2_SOVERSION(${PC_${CNAME}_VERSION} so_v_)
					add_library(${CNAME}::${CNAME} UNKNOWN IMPORTED GLOBAL)
					set_target_properties(
						${CNAME}::${CNAME} PROPERTIES
						IMPORTED_LOCATION "${${CNAME}_LIBRARY}"
						INTERFACE_COMPILE_OPTIONS "${PC_${CNAME}_CFLAGS_OTHER}"
						INTERFACE_INCLUDE_DIRECTORIES "${${CNAME}_INCLUDEDIR}"
						VERSION "${PC_${CNAME}_VERSION}"
						SOVERSION ${so_v_}
					)
					find_library_target_report(${CNAME})
				endif()
			endif()
		endif()
		# no pkg-config or pkg-config can't find the ${CNAME} library
		if(NOT ${CNAME}_FOUND_LOCAL)
			# todo search on platform default location
			find_path(${CNAME}_INCLUDE_DIR
				NAMES "${CNAME}/${header_filename}"
			)
			find_library(${CNAME}_LIBRARY
				NAMES ${library_file_basename}
			)
			if((NOT (${CNAME}_INCLUDE_DIR STREQUAL "${CNAME}_INCLUDE_DIR-NOTFOUND")) 
				AND
				(NOT (${CNAME}_LIBRARY STREQUAL "${CNAME}_LIBRARY-NOTFOUND")))
				add_imported_lib(${CNAME})
				set(${CNAME}_FOUND_LOCAL TRUE)
				set(${CNAME}_FOUND TRUE PARENT_SCOPE)
				find_library_target_report(${CNAME})
				return()
			endif()
			# can't find library without search location hints
			message(SEND_ERROR "-- search ${CNAME} failed at default dir, try providing hints by specifying ${CNAME}_ROOT (UNIX-like only) or <${CNAME}_INCLUDEDIR ${CNAME}_LIBRARYDIR>\n")
		endif()
	endif()
endfunction()

# ====================================================
# function for finding library (single configuration)
# ====================================================
# ++++++++++ function parameter ++++++++++
# header_filename: an actual library header file at root of include directory, used to verify include directory
# ++++++++++ hints and search results ++++++++++
# if search failed with default configuration, 
# user can provide cache ${CNAME}_ROOT (work only on UNIX-like system, CNAME = Library Canonical Name) 
# or <${CNAME}_INCLUDEDIR ${CNAME}_LIBRARYDIR> as hints for search location.
# this routine will save search results as cache variables ${CNAME}_INCLUDE_DIR and ${CNAME}_LIBRARY. 
# ++++++++++ search priority ++++++++++
# 1 user hints of <${CNAME}_INCLUDEDIR ${CNAME}_LIBRARYDIR>
# 2 user hints of ${CNAME}_ROOT
# 3 using pkg-config
# 4 search on platform default location
# todo impl version and "REQUIRED" features
function(find_library_target_flat CNAME library_file_basename header_filename) 
	message("-- searching ${CNAME}")
	set(${CNAME}_SEARCH_LOCATION_READY FALSE)
	set(${CNAME}_FOUND_LOCAL FALSE)
	set(${CNAME}_FOUND FALSE PARENT_SCOPE)
	# check last search results first
	if(${CNAME}_INCLUDE_DIR AND ${CNAME}_LIBRARY)
		add_imported_lib(${CNAME})
		set(${CNAME}_FOUND_LOCAL TRUE)
		set(${CNAME}_FOUND TRUE PARENT_SCOPE)
		find_library_target_report(${CNAME})
		return()
	endif()
	# check existence of <${CNAME}_ROOT | <${CNAME}_INCLUDEDIR ${CNAME}_LIBRARYDIR>>
	# and prepare the library and include search location
	if(${CNAME}_INCLUDEDIR AND ${CNAME}_LIBRARYDIR)
		set(${CNAME}_INCLUDEDIR__ ${${CNAME}_INCLUDEDIR})
		set(${CNAME}_LIBRARYDIR__ ${${CNAME}_LIBRARYDIR})
		set(${CNAME}_SEARCH_LOCATION_READY TRUE)
	elseif(${CNAME}_ROOT)
		if(UNIX)
			file(TO_CMAKE_PATH ${${CNAME}_ROOT} ${CNAME}_ROOT__)
			set(${CNAME}_INCLUDEDIR__ "${${CNAME}_ROOT__}/include")
			set(${CNAME}_LIBRARYDIR__ "${${CNAME}_ROOT__}/lib")
			set(${CNAME}_SEARCH_LOCATION_READY TRUE)
		else()
			# todo this do not work on win32?
		endif()
	endif()
	if(${CNAME}_SEARCH_LOCATION_READY)
		find_path(
			${CNAME}_INCLUDE_DIR
			NAMES "${header_filename}"
			PATHS ${${CNAME}_INCLUDEDIR__}
			#PATH_SUFFIXES ${CNAME}
		)
		find_library(
			${CNAME}_LIBRARY
			NAMES ${library_file_basename}
			PATHS ${${CNAME}_LIBRARYDIR__}
		)
		if((NOT (${CNAME}_INCLUDE_DIR STREQUAL "${CNAME}_INCLUDE_DIR-NOTFOUND"))
			AND
			(NOT (${CNAME}_LIBRARY STREQUAL "${CNAME}_LIBRARY-NOTFOUND")))
			add_imported_lib(${CNAME})
			set(${CNAME}_FOUND_LOCAL TRUE)
			set(${CNAME}_FOUND TRUE PARENT_SCOPE)
			find_library_target_report(${CNAME})
		else()
			message(SEND_ERROR "-- ${CNAME} search failed\n")
		endif()
	else()
		# try pkg-config
		if(PKG_CONFIG_FOUND)
			# message("-- using pkg_check_modules searching ${CNAME}")
			pkg_check_modules(PC_${CNAME} QUIET ${CNAME})
			if(PC_${CNAME}_VERSION)
				find_path(
					${CNAME}_INCLUDE_DIR
					NAMES "${header_filename}"
					PATHS ${PC_${CNAME}_INCLUDE_DIRS}
					#PATH_SUFFIXES ${CNAME}
				)
				find_library(
					${CNAME}_LIBRARY
					NAMES ${library_file_basename}
					PATHS ${PC_${CNAME}_LIBRARY_DIRS}
				)
				if((NOT (${CNAME}_INCLUDE_DIR STREQUAL "${CNAME}_INCLUDE_DIR-NOTFOUND"))
					AND
					(NOT (${CNAME}_LIBRARY STREQUAL "${CNAME}_LIBRARY-NOTFOUND")))
					set(${CNAME}_FOUND_LOCAL TRUE)
					set(${CNAME}_FOUND TRUE PARENT_SCOPE)
					# set(${CNAME}_VERSION ${PC_${CNAME}_VERSION})
					VERSION_2_SOVERSION(${PC_${CNAME}_VERSION} so_v_)
					add_library(${CNAME}::${CNAME} UNKNOWN IMPORTED GLOBAL)
					set_target_properties(
						${CNAME}::${CNAME} PROPERTIES
						IMPORTED_LOCATION "${${CNAME}_LIBRARY}"
						INTERFACE_COMPILE_OPTIONS "${PC_${CNAME}_CFLAGS_OTHER}"
						INTERFACE_INCLUDE_DIRECTORIES "${${CNAME}_INCLUDEDIR}"
						VERSION "${PC_${CNAME}_VERSION}"
						SOVERSION ${so_v_}
					)
					find_library_target_report(${CNAME})
				endif()
			endif()
		endif()
		# no pkg-config or pkg-config can't find the ${CNAME} library
		if(NOT ${CNAME}_FOUND_LOCAL)
			# todo search on platform default location
			find_path(${CNAME}_INCLUDE_DIR
				NAMES "${header_filename}"
			)
			find_library(${CNAME}_LIBRARY
				NAMES ${library_file_basename}
			)
			if((NOT (${CNAME}_INCLUDE_DIR STREQUAL "${CNAME}_INCLUDE_DIR-NOTFOUND")) 
				AND
				(NOT (${CNAME}_LIBRARY STREQUAL "${CNAME}_LIBRARY-NOTFOUND")))
				add_imported_lib(${CNAME})
				set(${CNAME}_FOUND_LOCAL TRUE)
				set(${CNAME}_FOUND TRUE PARENT_SCOPE)
				find_library_target_report(${CNAME})
				return()
			endif()
			# can't find library without search location hints
			message(SEND_ERROR "-- search ${CNAME} failed at default dir, try providing hints by specifying ${CNAME}_ROOT (UNIX-like only) or <${CNAME}_INCLUDEDIR ${CNAME}_LIBRARYDIR>\n")
		endif()
	endif()
endfunction()