## todo Generate Build System

### todo Handle Dependency

1. Follow [lexicon_buddy](https://bitbucket.org/intermittence/lexicon_buddy)'s instruction to build and install lexicon_buddy package. 
2. Install Qt 5.13.
3. Install BASS audio library.
4. Check following example for generate build system.

### Example of Generate Build System

- Generate Ninja (you should install Ninja first) Build System on macOS: [example](example_macos_generate_ninja.sh) 
- todo Generate Visual Studio 2019 Project (multiple configuration) on Windows 10:

## Build

Use cmake command-line or build system to build.