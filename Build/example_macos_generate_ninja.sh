#!/bin/zsh
# build type for single configuration generators
build_t="-DCMAKE_BUILD_TYPE:STRING=Debug"
# select generator name and path
cmake_generator="-GNinja"
cmake_generator_p="-DCMAKE_MAKE_PROGRAM:FILEPATH=/usr/local/bin/ninja"
# pass cmake cache variables
qt="-DQt5_DIR:PATH=~/Qt5.13.2/5.13.2/clang_64/lib/cmake/Qt5"
lexicon_buddy_DIR="-Dlexicon_buddy_DIR:PATH=/usr/local/Cellar/lexicon_buddy/0.1.0/lib/cmake/lexicon_buddy"
openssl="-DOPENSSL_ROOT_DIR:PATH=/usr/local/opt/openssl"
ICU="-DICU_ROOT:PATH=/usr/local/opt/icu4c"
bass_header="-Dbass_INCLUDEDIR:PATH=~/Desktop/files/cpplib/bass24-osx"
bass_lib="-Dbass_LIBRARYDIR:PATH=~/Desktop/files/cpplib/bass24-osx"
# check sh script src and build arguments
# generate
if [ "$#" -eq "2" ]; then
	src_dir=$1
	build_dir=$2
	cmake $build_t $cmake_generator $cmake_generator_p $qt $lexicon_buddy_DIR $openssl $ICU $bass_header $bass_lib -S $src_dir -B $build_dir
else
	echo "two arguments (src and build dir) required"
fi