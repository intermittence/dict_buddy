//
//  dict_pref_dialog.h
//
//  Created by Lohengrin on 2019/9/10.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef DICT_PREF_DIALOG_H
#define DICT_PREF_DIALOG_H

#include <QDialog>
#include <set>
#include <map>
#include <iostream>
using std::string;
using std::set;
using std::map;
namespace Ui
{
class dict_pref_dialog;
}

namespace lexicon::buddy
{
template <class offline_handler, class online_handler>
class manager;
}

class offline_handler;
class online_handler;
using dict_manager = lexicon::buddy::manager<offline_handler, online_handler>;

class QCheckBox;
class QLineEdit;
class QHBoxLayout;
class QVBoxLayout;
class QPushButton;

class dict_pref_dialog : public QDialog
{
	Q_OBJECT

public:
	explicit dict_pref_dialog(dict_manager* d, QWidget* parent = nullptr);
	void align();
	~dict_pref_dialog();
public slots:
	void open();
	void apply_pref();
	void close_pref();
	void ok_pref();
	void s_service_modified();
	void s_res_modified();
	void s_apikey_modified(const QString& text);
	void remove();

private:
	static constexpr int SERVICE_ROWS_COUNT = 4;
	static constexpr int TAIL_ROWS_COUNT = 1;
	void load_webster();
	void init_load_offline();
	void clear_offline();
	Ui::dict_pref_dialog* ui;
	dict_manager* dm_p;
	bool service_dirty{};
	bool offline_dirty{};
	QVBoxLayout* v_layout;
	QHBoxLayout* collegiate_hbox0;
	QHBoxLayout* collegiate_hbox1;
	QHBoxLayout* thesaurus_hbox0;
	QHBoxLayout* thesaurus_hbox1;
	QCheckBox* collegiate_activation;
	QLineEdit* collegiate_apikey;
	QCheckBox* thesaurus_activation;
	QLineEdit* thesaurus_apikey;
	QPushButton* apply_button;
	QPushButton* ok_button;
	QPushButton* cancel_button;
	set<QString> modified_keys;
	bool coll_key_validity = true;
	bool thes_key_validity = true;
};

#endif // DICT_PREF_DIALOG_H