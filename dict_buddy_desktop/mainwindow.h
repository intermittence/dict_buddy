//
//  mainwindow.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/1.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "use_dict.h"
#include <dict_buddy_core/audio_system.h>
#include <dict_buddy_core/online_record_parser.h>
#include <lexicon_buddy/lexicon/buddy_forward.h>
namespace Ui
{
class MainWindow;
}
class QMessageBox;
class dict_pref_dialog;
// class PredictForm;
class PredictFrame;
namespace dict_core
{
class PredictModel;
} // namespace dict_core

using lexicon::buddy::lang;
using lexicon::buddy::search_ctx;
using dict_manager = lexicon::buddy::manager<offline_handler, online_handler>;
using lexicon::buddy::query_cluster;
using lexicon::online::response_type;
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(const char* dict_dir, const char* index_dir, const char* cfg_path,
						QWidget* parent = nullptr);
	~MainWindow() override;
	void mainInit();
	bool eventFilter(QObject* obj, QEvent* event) override;
	void displayText();
	uint8_t get_doc_id() { return doc_id_; }
	uint8_t update_doc_id() { return ++doc_id_; }
	void new_doc_prepare();
	static constexpr uint8_t res_id_size = 4;
	string_view gen_res_id();
	search_ctx get_ctx() { return ctx_; }
	auto get_audio_sys() { return &au_; }
	QTextBrowser* get_textbrowser();
	static constexpr uint8_t predict_limit = 5;

signals:
	void remote_webster_html_ready(uint8_t id, const QString& html_, const dict_core::webster_parser& wp_);
	void remote_webster_html_error(uint8_t id, const QString& html_);
	// target is a href start with '#'
	void remote_webster_html_ready_t(uint8_t id, const QString& html_, const dict_core::webster_parser& wp_,
									 const string& target);
	void remote_audio_ready(uint8_t id, online_handler::S state, const string& name,
							const response_type& http_response);
	void remote_img_ready(uint8_t id, const QUrl& link, const char* format, const string& dat);
	void update_import_box_text(const QString& msg);
	void complete_import_box(const QString& msg);

public slots:
	// append html
	void s_remote_webster_html_ready(uint8_t id, const QString& html_, const dict_core::webster_parser& wp_);
	// todo
	void s_remote_webster_html_error(uint8_t id, const QString& html_);
	// append html and jump
	void s_remote_webster_html_ready_t(uint8_t id, const QString& html_, const dict_core::webster_parser& wp_,
									   const string& target);
	void s_remote_audio_ready(uint8_t id, online_handler::S state, const string& name,
							  const response_type& http_response);
	void s_remote_img_ready(uint8_t id, const QUrl& link, const char* format, const string& dat);
	// when user typing characters
	void input_update(const QString&);
	// when user press enter
	void query();
	void lineEditFocus(bool focused);
	void s_anchor_clicked(const QUrl& link);
	void s_update_import_box_text(const QString& msg);
	void s_complete_import_box(const QString& msg);

protected:
	void dragEnterEvent(QDragEnterEvent* e) override;
	void dropEvent(QDropEvent* e) override;

private:
	friend online_handler;
	void workAfterSelect();
	char res_id[res_id_size]{'0'};
	// actually it means text of documnet != text of the inputbox
	bool text_edited_{true};
	search_ctx ctx_{};
	Ui::MainWindow* ui;
	PredictFrame* pf_{};
	// main window has direct access to model
	dict_core::PredictModel* pmodel_{};
	query_cluster::predict_type prediction_{};
	uint8_t doc_id_{};
	dict_core::audio_sys au_{};
	// destruct dict_manager first!
	dict_manager m_;
	QMessageBox* about_box_;
	QMessageBox* import_box_;
	struct doc_info
	{
		// qualified id with prefix '#'
		std::vector<string> qualified_metaid_v;
		void append_metaid(const std::vector<string>& arg);
		bool find_metaid(string_view id);
		void clear_all() { qualified_metaid_v.clear(); }
	};
	doc_info d_info_;
	QMenu* editMenu_;
	QMenu* helpMenu_;
	QMenu* toolsMenu_;
	// app_preferences* preferences_;
	dict_pref_dialog* dict_pref;
	void webster_jump(const string& str);
	void webster_suggest(const string& str);
	void init_about();
	void show_about();
	void init_import();
	//	void init_preferences();
	//	void show_preferences();
	void init_dict_pref();
	void show_dict_pref();
	void init_menu();
};

#endif // MAINWINDOW_H