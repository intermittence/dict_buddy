//
//  predictlistview.cpp
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/5.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include "predictlistview.h"

PredictListView::PredictListView(QWidget* parent) : QListView(parent) {}
void PredictListView::setProperties_type1() {
	/*
	QString style_sheet = "background-color: rgb(97, 98, 98);"
						  "selection-background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, "
						  "stop:0 rgba(0, 0, 0, 255), stop:1"
						  "rgba(255, 255, 255, 255));"
						  "gridline-color: rgb(252, 0, 20);";
	*/
	// setStyleSheet(style_sheet);
	setViewportMargins(0, 0, 0, 0);
	setSelectionRectVisible(true);
	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	resize(type1_width, 0);
}

// QSize PredictListView::sizeHint() const { return QSize(300, model()->rowCount() * sizeHintForRow(0) + 20);
// }