//
//  predictframe.h
//
//  Created by Lohengrin on 2019/9/17.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef PREDICTFRAME_H
#define PREDICTFRAME_H

#include <QFrame>
#include <QItemSelectionModel>
#include <lexicon_buddy/lexicon/buddy_forward.h>
namespace Ui
{
class PredictFrame;
}
using lexicon::buddy::query_cluster;
namespace dict_core
{
class PredictModel;
} // namespace dict_core
class PredictListView;

class PredictFrame : public QFrame
{
	Q_OBJECT

public:
	explicit PredictFrame(QWidget* parent = nullptr);
	~PredictFrame();
	void setModel(dict_core::PredictModel* model);
	void setSelectionModel(QItemSelectionModel* selectionModel);
	// QSize sizeHint() const override;
	void adjustListView() {
		//	listview_->adjustSize();
		this->adjustSize();
	}
	void selectUp();
	void selectDown();
	bool selectionModelSelected();
	QModelIndex getSelectIndex() { return selectionmodel_->currentIndex(); }
	void show_prediction(const query_cluster::predict_type& prediction);
	bool isSelected() { return selectionmodel_->hasSelection(); }
	void show_if_not_empty();

private:
	Ui::PredictFrame* ui;
	dict_core::PredictModel* predictmodel_{};
	PredictListView* listview_{};
	QItemSelectionModel* selectionmodel_{};
};

#endif // PREDICTFRAME_H