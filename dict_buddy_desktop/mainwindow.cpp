//
//  mainwindow.cpp
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/1.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <dict_buddy_core/predictmodel.h>
#include "predictframe.h"
#include "use_dict.h"
#include <dict_buddy_core/core_util.h>
#include "desktop_util.h"
//#include "app_preferences.h"
#include "dict_pref_dialog.h"
#if (defined(__APPLE__) && defined(__MACH__))
#include "mactitlebar.h"
#endif
#include <iostream>
#include <algorithm>
#include <QKeyEvent>
#include <lexicon_buddy/lexicon/buddy.h>
#include <QString>
#include <QStringView>
#include <QDesktopServices>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QMimeData>

using std::make_unique;
using lexicon::online::URL;
using lexicon::online::urlstr_2_URL;

MainWindow::MainWindow(const char* dict_dir, const char* index_dir, const char* cfg_path, QWidget* parent)
	: QMainWindow(parent), ui(new Ui::MainWindow), pf_(new PredictFrame(this)),
	  pmodel_(new dict_core::PredictModel()), m_{dict_dir, index_dir, cfg_path, true, true} {
	// init ctx
	ctx_.lang_pair.first = lang::all;
	ctx_.lang_pair.second = lang::all;
	// load cfg
	m_.load_webster_setting();
	// ui
	ui->setupUi(this);
	ui->lineEdit->installEventFilter(this);
	ui->lineEdit->setAttribute(Qt::WA_MacShowFocusRect, 0);
	ui->textBrowser->setOpenLinks(false);
	ui->textBrowser->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	// set_scroll_bar_style(ui->textBrowser->verticalScrollBar(), "QScrollBar {width:0px;}");
	pf_->hide();
	pf_->setModel(pmodel_);
	pf_->setGeometry(2, 62, 20, 20);
	pf_->raise();
	// init audio
	au_.agent_.init_device();
#if (defined(__APPLE__) && defined(__MACH__))
	MacManager::removeTitlebarFromWindow(winId());
#endif
	// signals and slots
	connect(ui->lineEdit, &std::remove_pointer_t<decltype(ui->lineEdit)>::textEdited, this,
			&std::remove_pointer_t<decltype(this)>::input_update);
	connect(ui->lineEdit, &std::remove_pointer_t<decltype(ui->lineEdit)>::focused, this,
			&std::remove_pointer_t<decltype(this)>::lineEditFocus);
	// when user press enter
	connect(ui->lineEdit, &std::remove_pointer_t<decltype(ui->lineEdit)>::returnPressed, this,
			&std::remove_pointer_t<decltype(this)>::query);
	// handle url click event: play back and ...
	connect(ui->textBrowser, &std::remove_pointer_t<decltype(ui->textBrowser)>::anchorClicked, this,
			&std::remove_pointer_t<decltype(this)>::s_anchor_clicked);
	connect(this, &std::remove_pointer_t<decltype(this)>::remote_webster_html_ready, this,
			&std::remove_pointer_t<decltype(this)>::s_remote_webster_html_ready);
	connect(this, &std::remove_pointer_t<decltype(this)>::remote_webster_html_error, this,
			&std::remove_pointer_t<decltype(this)>::s_remote_webster_html_error);
	connect(this, &std::remove_pointer_t<decltype(this)>::remote_webster_html_ready_t, this,
			&std::remove_pointer_t<decltype(this)>::s_remote_webster_html_ready_t);
	connect(this, &std::remove_pointer_t<decltype(this)>::remote_audio_ready, this,
			&std::remove_pointer_t<decltype(this)>::s_remote_audio_ready);
	connect(this, &std::remove_pointer_t<decltype(this)>::remote_img_ready, this,
			&std::remove_pointer_t<decltype(this)>::s_remote_img_ready);
	// init_preferences();
	init_dict_pref();
	// init about dialog
	init_about();
	// init import message box
	init_import();
	connect(this, &std::remove_pointer_t<decltype(this)>::update_import_box_text, this,
			&std::remove_pointer_t<decltype(this)>::s_update_import_box_text);
	connect(this, &std::remove_pointer_t<decltype(this)>::complete_import_box, this,
			&std::remove_pointer_t<decltype(this)>::s_complete_import_box);
	// menu for macos
	init_menu();
	// accept drap and drop
	setAcceptDrops(true);
}

void MainWindow::new_doc_prepare() {
	update_doc_id();
	ui->textBrowser->clear();
	d_info_.clear_all();
}

void MainWindow::doc_info::append_metaid(const std::vector<string>& arg) {
	for (auto& i : arg) {
		qualified_metaid_v.emplace_back(i);
	}
}

bool MainWindow::doc_info::find_metaid(string_view id) {
	auto end = qualified_metaid_v.end();
	return !(end == std::find(qualified_metaid_v.begin(), end, id));
}

void MainWindow::s_remote_webster_html_ready(uint8_t id, const QString& html_,
											 const dict_core::webster_parser& wp_) {
	if (id != doc_id_) return;
	auto scroll_bar_pos = get_scroll_bar_pos(ui->textBrowser->verticalScrollBar());
#ifndef NDEBUG
	cout << html_.toStdString() << endl;
#endif
	ui->textBrowser->append(html_);
	d_info_.append_metaid(wp_.get_qualified_metaids());
	set_scroll_bar_pos(ui->textBrowser->verticalScrollBar(), scroll_bar_pos);
	img_extract_tasks img_tasks{doc_id_, m_.get_http_engine(), this};
	for (auto& i : wp_.get_img_urls()) {
		img_tasks.add_target(i);
	}
	img_tasks();
	m_.notify_http_thread();
}

void MainWindow::s_remote_webster_html_error(uint8_t id, const QString& html_) {
	if (id != doc_id_) return;
	auto scroll_bar_pos = get_scroll_bar_pos(ui->textBrowser->verticalScrollBar());
#ifndef NDEBUG
	cout << html_.toStdString() << endl;
#endif
	ui->textBrowser->append(html_);
	set_scroll_bar_pos(ui->textBrowser->verticalScrollBar(), scroll_bar_pos);
}

void MainWindow::s_remote_webster_html_ready_t(uint8_t id, const QString& html_,
											   const dict_core::webster_parser& wp_, const string& target) {
	if (id != doc_id_) return;
	s_remote_webster_html_ready(id, html_, wp_);
	ui->textBrowser->scrollToAnchor(QString::fromStdString(target.substr(1)));
}

void MainWindow::s_remote_audio_ready(uint8_t id, online_handler::S state, const string& name,
									  const response_type& http_response) {
	if (id != doc_id_) return;
	auto get_url_last_ext = [&name]() -> dict_core::string_view {
		auto pos_ = name.find_last_of('.');
		if (pos_ != name.npos) {
			return {name.data() + pos_, name.size() - pos_};
		} else {
			return {};
		}
	};
	auto mime_view = http_response[boost::beast::http::field::content_type];
	auto ext = dict_core::get_extension_from_literal(get_url_last_ext());
	if (!dict_core::is_MIME_valid(ext, {mime_view.data(), mime_view.size()})) return;
	// compare handler's identity against buffer's and position of current playing buffer
	// todo current play?
	auto h_id = audio_ring_buffer_map(name);
	if (au_.ring_.get_ctx(state.pos) != h_id.first or au_.ring_.get_name(state.pos) != h_id.second or
		state.pos != au_.play_p_)
		return;
	// stop and free audio sys
	au_.agent_.stop_strm();
	au_.agent_.free_strm();
	auto& buff_dst = au_.ring_[state.pos];
	au_.rs_[state.pos] = std::remove_pointer_t<std::decay_t<decltype(au_.rs_)>>::ready;
	buff_dst = http_response.body();
	// std::swap(buff_dst, http_response.body());
	au_.agent_.play_back(buff_dst.data(), buff_dst.size());
}

void MainWindow::s_remote_img_ready(uint8_t id, const QUrl& link, const char* format, const string& dat) {
#ifndef NDEBUG
	cout << "image arrived, size: " << dat.size() << endl << "url: " << link.toString().toStdString() << endl;
#endif
	if (id != doc_id_) return;
	QImage img;
	// avoid redundant memory copy with QByteArray::fromRawData
	img.loadFromData(QByteArray::fromRawData(dat.data(), dat.size()), format);
	auto tb = ui->textBrowser;
	tb->document()->addResource(QTextDocument::ImageResource, link, img);
	// auto html = ui->textBrowser->document()->toHtml();
	// ui->textBrowser->setHtml(html);
	// ui->textBrowser->moveCursor(QTextCursor::Start);
	tb->zoomIn();
	tb->zoomOut();
}

void MainWindow::webster_suggest(const string& str) {
	constexpr auto pos_dict_char = sizeof(WEBSTER_SUGGEST_SCHEME);
	constexpr auto pos_id_begin = pos_dict_char + 2;
	char dict = str[pos_dict_char];
	switch (dict) {
		case (char)dict_core::webster_dictionary::Collegiate: {
			new_doc_prepare();
			text_edited_ = true;
			string word = str.substr(pos_id_begin);
			online_handler::S s;
			s.dict = dict_core::webster_dictionary::Collegiate;
			m_.query_webster_Collegiate_Dictionary(
				word, {doc_id_, online_handler_category::webster_json, this, nullptr, word, s});
			m_.notify_http_thread();
			return;
		}
		case (char)dict_core::webster_dictionary::Collegiate_Thesaurus: {
			new_doc_prepare();
			text_edited_ = true;
			string word = str.substr(pos_id_begin);
			online_handler::S s;
			s.dict = dict_core::webster_dictionary::Collegiate_Thesaurus;
			m_.query_webster_Collegiate_Thesaurus(
				word, {doc_id_, online_handler_category::webster_json, this, nullptr, word, s});
			m_.notify_http_thread();
			return;
		}
		case (char)dict_core::webster_dictionary::Learner_Dictionary:
		default:
			return;
	}
}

void MainWindow::webster_jump(const string& str) {
	constexpr auto mini_size = sizeof(WEBSTER_ENTRY_IDATT_METAID_SCHEME) + 4 - 1;
	constexpr auto pos_dict_char = sizeof(WEBSTER_ENTRY_IDATT_METAID_SCHEME) + 2 - 1;
	constexpr auto pos_id_begin = pos_dict_char + 2;
	auto get_word = [str]() {
		// todo test
		auto pos0 = str.find("%3A", pos_id_begin);
		if (pos0 == str.npos)
			return str.substr(pos_id_begin);
		else
			return str.substr(pos_id_begin, pos0 - pos_id_begin);
	};
	if (d_info_.find_metaid(str)) {
		ui->textBrowser->scrollToAnchor(QString::fromStdString({str.data() + 1, str.size() - 1}));
	} else {
		if (str.size() > mini_size) {
			char dict{};
			if (string_view{str.data() + 1, sizeof(WEBSTER_ENTRY_IDATT_METAID_SCHEME) - 1} ==
				WEBSTER_ENTRY_IDATT_METAID_SCHEME) {
				dict = str[pos_dict_char];
				switch (dict) {
					case (char)dict_core::webster_dictionary::Collegiate: {
						new_doc_prepare();
						text_edited_ = true;
						string word{get_word()};
						const string& jump_target = str; //{str.substr(1)};
						online_handler::S s;
						s.dict = dict_core::webster_dictionary::Collegiate;
						// todo handler construction with string_view
						m_.query_webster_Collegiate_Dictionary(
							word,
							{doc_id_, online_handler_category::webster_json, this, nullptr, jump_target, s});
						m_.notify_http_thread();
						return;
					}
					case (char)dict_core::webster_dictionary::Collegiate_Thesaurus: {
						new_doc_prepare();
						text_edited_ = true;
						string word{get_word()};
						const string& jump_target = str; //{str.substr(1)};
						online_handler::S s;
						s.dict = dict_core::webster_dictionary::Collegiate_Thesaurus;
						m_.query_webster_Collegiate_Thesaurus(
							word,
							{doc_id_, online_handler_category::webster_json, this, nullptr, jump_target, s});
						m_.notify_http_thread();
						return;
					}
					case (char)dict_core::webster_dictionary::Learner_Dictionary:
					default:
						return;
				}
			}
		}
	}
}

/// @brief when user click an audio link, there are two ways to play: direct play audio which stored in
/// document cache or download (if not ready) and play the audio stored in the @class dict_core::audio_sys
/// dedicated ring buffer
/// @note there's an ABA problem in async audio playback from ring buffer. if user quickly submit a lot of
/// playback tasks, a buffer in the ring may be marked with name of A, then B (which is allowed since B!=A),
/// and finally A. when A, B and another A's async download are complete, their handlers will be invoked. but
/// if A and B download data from different domain, B may be delayed and there will two A async complete the
/// download and play audio (ABA becomes error-prone AA from async handlers's perspective). for two handlers
/// of different name, only one whose name match buffer's name will write data to buffer and play but it's not
/// the case for ABA. when audio system is play a audio in the memory, ABA problem could cause the
/// deallocation or change of that memory during playback. solution is audio system copy data from buffer
/// before playback or every async handler must stop audio system before manipulate the buffer
void MainWindow::s_anchor_clicked(const QUrl& link) {
	auto str = link.toString().toStdString();
	// process href with form like [#]<scheme>.<dict>.<id>
	if (str[0] == '#')
		return webster_jump(str);
	else if (string_view{str.data(), str.find_first_of('.')} == WEBSTER_SUGGEST_SCHEME)
		return webster_suggest(str);
	// process href which is an url
	auto sch = link.scheme();
	if (sch == DOC_AU_SCHEME) {
		// play audio
		auto audio_cache =
			ui->textBrowser->document()->resource(dict_core::custom_resource::res_audio, link).toByteArray();
		au_.agent_.stop_strm();
		au_.agent_.free_strm();
		au_.play_p_ = au_.end_;
		au_.agent_.play_back(audio_cache.data(), audio_cache.size());
	} else if (sch == WEBSTER_S_SCHEME_QT) {
		using res_type_ = std::remove_pointer_t<std::decay_t<decltype(au_.rs_)>>;
		auto url_stdstring = link.toString().toStdString();
		auto identity = audio_ring_buffer_map(url_stdstring);
		// search the buffer
		auto& r = au_.ring_;
		auto pos_ = r.get_element_p(identity.first, identity.second);
		auto http_client_ = m_.get_http_engine();
		auto download_play = [this, &url_stdstring, &http_client_](std::size_t pos, const URL url) {
			if (url.scheme == "http" or url.scheme == "https") {
				// head pos_ is passed to the void* state as memory location difference against au_;
				au_.rs_[pos] = res_type_::poll;
				http_client_->task_enqueue(
					url,
					online_handler{
						doc_id_, online_handler_category::webster_audio, this, &au_, url_stdstring, {pos}});
				m_.notify_http_thread();
			}
		};
		auto url_map = [&url_stdstring]() -> URL {
			URL u = urlstr_2_URL(url_stdstring);
			auto p_s = scheme_map(u.scheme);
			if (!p_s) return {};
			u.scheme = p_s;
			if (u.scheme == "http") {
				u.port = lexicon::online::PORT_HTTP;
			} else if (u.scheme == "https") {
				u.port = lexicon::online::PORT_HTTPS;
			} else {
				return {};
			}
			return u;
		};
		au_.agent_.stop_strm();
		if (pos_ == r.end_) {
			// take a sit in buffer
			auto head = r.get_head();
			r.direct_write_identity(head, identity.first, string{identity.second});
			r.update_head();
			au_.rs_[head] = res_type_::empty;
			// set as playback target
			au_.play_p_ = pos_;
			URL u = url_map();
			if (u.port.empty()) return;
			au_.play_p_ = head;
			// async download and playback
			download_play(head, u);
		} else {
			// set as playback target
			au_.play_p_ = pos_;
			// audio found in buffer. check the state
			switch (au_.rs_[pos_]) {
				case res_type_::empty: {
					URL u = url_map();
					if (u.port.empty()) return;
					download_play(pos_, u);
				} break;
				case res_type_::poll:
					// do nothing
					break;
				case res_type_::ready:
					// play
					au_.agent_.play_back(r[pos_].data(), r[pos_].size());
					break;
				default:
					break;
			}
		}
	} else if (sch == "http" or sch == "https") {
		QDesktopServices::openUrl(link);
	}
}

void MainWindow::query() {
	pf_->hide();
	if (!text_edited_) return;
	text_edited_ = false;
	// prepare for new doc
	new_doc_prepare();
	auto input = ui->lineEdit->text();
	auto std_str = input.toStdString();
	online_handler::S s;
	// english words only
	auto just_ascii = dict_core::ascii_only(std_str);
	if (just_ascii) {
		auto std_str_url_enc =
			dict_core::url_encode_str(reinterpret_cast<unsigned char*>(std_str.data()), std_str.size());
		s.dict = dict_core::webster_dictionary::Collegiate;
		m_.query_webster_Collegiate_Dictionary(
			std_str_url_enc,
			{doc_id_, online_handler_category::webster_json, this, nullptr, std_str_url_enc, s});
		s.dict = dict_core::webster_dictionary::Collegiate_Thesaurus;
		m_.query_webster_Collegiate_Thesaurus(
			std_str_url_enc,
			{doc_id_, online_handler_category::webster_json, this, nullptr, std_str_url_enc, s});
		m_.notify_http_thread();
	}
	// check if user had selected any prediction
	auto scroll_bar_pos = get_scroll_bar_pos(ui->textBrowser->verticalScrollBar());
	if (pf_->selectionModelSelected()) {
#ifndef NDEBUG
		cout << "query " << std_str << " with prediction" << endl;
#endif
		auto iter = prediction_.find(std_str);
		if (iter != prediction_.end()) {
			offline_handler off_h{ui->textBrowser, this};
			m_.offline_query(iter->second, off_h);
		}
	} else {
#ifndef NDEBUG
		cout << "query " << std_str << " without prediction" << endl;
#endif
		offline_handler off_h{ui->textBrowser, this};
		m_.offline_query_str(std_str, off_h, get_ctx());
	}
	set_scroll_bar_pos(ui->textBrowser->verticalScrollBar(), scroll_bar_pos);
}

void MainWindow::lineEditFocus(bool focused) {
	if (!focused) pf_->hide();
}

void MainWindow::input_update(const QString& input) {
	text_edited_ = true;
	if (input.isEmpty()) {
		pf_->hide();
	} else {
		prediction_.clear();
		m_.predict<predict_limit>(prediction_, input.toStdString(), ctx_);
		pf_->show_prediction(prediction_);
		pf_->adjustListView();
		pf_->show_if_not_empty();
	}
}

// pform_ adjustsize work only after first actual show of pform_
// immediately call this after MainWindow::show()
void MainWindow::mainInit() {
	pf_->show();
	pf_->hide();
}

void MainWindow::workAfterSelect() {
	if (pf_->isSelected()) {
		// copy
		ui->lineEdit->setText(pmodel_->native_handle()->operator[](pf_->getSelectIndex().row())->word_);
	}
}

bool MainWindow::eventFilter(QObject* obj, QEvent* event) {
	if (obj == ui->lineEdit) {
		if (event->type() == QEvent::KeyPress) {
			QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
			if (keyEvent->key() == Qt::Key_Up) {
				pf_->selectUp();
				workAfterSelect();
				return true;
			} else if (keyEvent->key() == Qt::Key_Down) {
				pf_->selectDown();
				workAfterSelect();
				return true;
			}
		}
		return false;
	}
	return QMainWindow::eventFilter(obj, event);
}

void MainWindow::displayText() { ui->textBrowser->setHtml({}); }

string_view MainWindow::gen_res_id() {
	// update id
	auto pos = res_id_size - 2;
	for (;;) {
		if (res_id[pos] == '9') {
			res_id[pos--] = '0';
		} else {
			res_id[pos] += 1;
			break;
		}
		if (pos == (0 - 1)) break;
	}
	return {res_id, res_id_size};
}

void MainWindow::show_about() { show_about_box(about_box_); }

void MainWindow::init_about() { about_box_ = init_about_box(this); }

void MainWindow::init_import() { import_box_ = init_import_box(this); }

void MainWindow::show_dict_pref() {
#ifndef NDEBUG
	cout << "show_dict_pref()" << endl;
#endif
	if (!dict_pref->isVisible()) {
#ifndef NDEBUG
		cout << "open dict preference" << endl;
#endif
		dict_pref->open();
	}
}

void MainWindow::init_dict_pref() {
	dict_pref = new dict_pref_dialog(&m_, this);
	dict_pref->hide();
}

void MainWindow::init_menu() {
	helpMenu_ = menuBar()->addMenu(tr("&Help"));
	editMenu_ = menuBar()->addMenu(tr("&Edit"));
	toolsMenu_ = menuBar()->addMenu(tr("&Tools"));
	auto act_about = new QAction(tr("&About"));
	// auto act_preferences = new QAction(tr("&preferences"));
	auto act_dict_pref = new QAction("Dictionaries");
	auto act_option0 = new QAction("option0");
	act_option0->setCheckable(true);
	connect(act_about, &std::remove_pointer_t<decltype(act_about)>::triggered, this,
			&std::remove_pointer_t<decltype(this)>::show_about);
	//	connect(act_preferences, &std::remove_pointer_t<decltype(act_preferences)>::triggered, this,
	//			&std::remove_pointer_t<decltype(this)>::show_preferences);
	connect(act_dict_pref, &std::remove_pointer_t<decltype(act_dict_pref)>::triggered, this,
			&std::remove_pointer_t<decltype(this)>::show_dict_pref);
	helpMenu_->addAction(act_about);
	// toolsMenu_->addAction(act_preferences);
	editMenu_->addAction(act_dict_pref);
	// todo connect option0
	toolsMenu_->addAction(act_option0);
	//	auto list = mu_.get_menubar()->findChildren<QMenu*>();
	//	foreach (QMenu* m, list) { cout << m->title().toStdString() << endl; }
}

void MainWindow::dragEnterEvent(QDragEnterEvent* e) {
	if (!e->mimeData()->hasUrls()) return;
	auto urls{e->mimeData()->urls()};
	if (urls.size() == 1) {
		fs::path p{urls.first().toLocalFile().toStdString()};
		// todo win32?
		auto file = p.filename().string();
		auto ext_str = file.substr(file.find_first_of('.'));
		if (lexicon::supported_res_main_file_ext(ext_str)) e->acceptProposedAction();
	}
}

void MainWindow::dropEvent(QDropEvent* e) {
	if (!e->mimeData()->hasUrls()) return;
	auto urls{e->mimeData()->urls()};
	if (urls.size() == 1) {
		// post task
		dict_import_task t{this, &m_, {urls.first().toLocalFile().toStdString()}};
		boost::asio::post(*m_.get_io(), std::bind(&dict_import_task::operator(), t));
		m_.notify_http_thread();
		// show import msg box
		open_import_box(import_box_);
	}
}

void MainWindow::s_update_import_box_text(const QString& msg) { ::update_msgbox_text(import_box_, msg); }

void MainWindow::s_complete_import_box(const QString& msg) {
	::update_msgbox_text(import_box_, msg);
	::enable_msgbox_close(import_box_);
}

QTextBrowser* MainWindow::get_textbrowser() { return ui->textBrowser; }

MainWindow::~MainWindow() { delete ui; }