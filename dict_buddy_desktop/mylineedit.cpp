//
//  mylineedit.cpp
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/9.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include "mylineedit.h"

MyLineEdit::MyLineEdit(QWidget* parent) : QLineEdit(parent) {}

void MyLineEdit::focusInEvent(QFocusEvent* e) {
	QLineEdit::focusInEvent(e);
	emit(focused(true));
}

void MyLineEdit::focusOutEvent(QFocusEvent* e) {
	QLineEdit::focusOutEvent(e);
	emit(focused(false));
}

void MyLineEdit::dragEnterEvent(__attribute__((unused)) QDragEnterEvent* e) {}
void MyLineEdit::dragMoveEvent(__attribute__((unused)) QDragMoveEvent* e) {}
void MyLineEdit::dragLeaveEvent(__attribute__((unused)) QDragLeaveEvent* e) {}
