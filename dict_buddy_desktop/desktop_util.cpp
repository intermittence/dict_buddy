//
//  desktop_util.cpp
//  dict_buddy
//
//  Created by Lohengrin on 2019/5/17.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include "desktop_util.h"
#include <iostream>
#include <QScrollBar>
#include <QMessageBox>
#include <QPushButton>
#include <QImage>
#include <QPaintEngine>

#ifdef __linux__
#include <unistd.h>
#endif

#if (defined(__APPLE__) && defined(__MACH__))
#include "mactitlebar.h"
#include <libproc.h>
#endif

#ifdef _WIN32
#include <Windows.h>
#endif

fs::path get_exe_folder() {
// linux solution
#ifdef _linux__
	char buf[4096]{};
	auto ret = readlink("/proc/self/exe", buf, sizeof buf);
	if (ret == -1) return {};
	fs::path p{buf};
#endif
// macos solution
#if (defined(__APPLE__) && defined(__MACH__))
	auto pid = getpid();
	char buf[PROC_PIDPATHINFO_MAXSIZE];
	auto ret = proc_pidpath(pid, buf, sizeof buf);
	if (ret < 0) {
		std::cout << strerror(errno);
		return {};
	}
	fs::path p{buf};
#endif
// windows solution
#ifdef _WIN32
	WCHAR buf[MAX_PATH];
	auto ret = GetModuleFileName(NULL, buf, sizeof buf);
	if (ret == 0) return {};
	fs::path p{buf};
#endif
	return p.has_filename() ? p.parent_path() : p;
}

#if (defined(__APPLE__) && defined(__MACH__))
void init_dict_index_folder(const fs::path& exe_folder, fs::path& dict, fs::path& index, fs::path& cfg) {
	auto contents_path = exe_folder.parent_path();
	dict = contents_path / "Resources" / "dict";
	index = contents_path / "Resources" / "index";
	fs::path cfg_dir = contents_path / "Resources" / "cfg";
	cfg = contents_path / "Resources" / "cfg" / "cfg.xml";
	// check if directorys exist and create directory if not
	auto init_dir = [](const fs::path& p) {
		if (not(fs::directory_entry{p}.status().type() == fs::file_type::directory_file))
			fs::create_directory(p);
	};
	init_dir(dict);
	init_dir(index);
	init_dir(cfg_dir);
}
#endif

int get_scroll_bar_pos(QScrollBar* b) { return b->value(); }
void set_scroll_bar_pos(QScrollBar* b, int pos) { b->setValue(pos); }
void set_scroll_bar_style(QScrollBar* b, const char* style) { b->setStyleSheet(style); }

QMessageBox* init_about_box(QWidget* parent) {
	auto about_box_ = new QMessageBox{parent};
	about_box_->setStandardButtons(QMessageBox::Ok);
	auto ok_button_ = about_box_->button(QMessageBox::Ok);
	ok_button_->hide();
	about_box_->setWindowTitle("dict_buddy");
	about_box_->setText("\nauthor: Lohengrin");
	about_box_->setModal(false);
#if (defined(__APPLE__) && defined(__MACH__))
	MacManager::removeTitlebarFromWindow(about_box_->winId());
#endif
	return about_box_;
}

void show_about_box(QMessageBox* b) { b->show(); }
QMessageBox* init_import_box(QWidget* parent) {
	auto b = new QMessageBox{parent};
	b->setStandardButtons(QMessageBox::Ok);
	b->button(QMessageBox::Ok)->setEnabled(false);
	b->setWindowTitle("import dictionary");
	b->setModal(true);
#if (defined(__APPLE__) && defined(__MACH__))
	MacManager::removeTitlebarFromWindow(b->winId());
#endif
	// disable win close button
	b->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
	return b;
}

void open_import_box(QMessageBox* b) {
	b->button(QMessageBox::Ok)->setEnabled(false);
	b->setText("now importing...");
	b->show();
}

void update_msgbox_text(QMessageBox* b, const QString& msg) { b->setText(msg); }

void enable_msgbox_close(QMessageBox* b) {
	// enable win close button
	b->button(QMessageBox::Ok)->setEnabled(true);
}