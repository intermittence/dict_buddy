//
//  main.cpp
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/1.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include "mainwindow.h"
#include "desktop_util.h"
#include <QApplication>

void register_type() {
	qRegisterMetaType<uint8_t>("uint8_t");
	qRegisterMetaType<const char*>("const char*");
	qRegisterMetaType<string>("string");
	qRegisterMetaType<response_type>("response_type");
	qRegisterMetaType<online_handler::S>("online_handler::S");
	qRegisterMetaType<dict_core::webster_parser>("dict_core::webster_parser");
}

int main(int argc, char* argv[]) {
	register_type();
	fs::path exe_folder = get_exe_folder();
	fs::path dict{};
	fs::path index{};
	fs::path cfg{};
	if (exe_folder.empty()) std::exit(EXIT_FAILURE);
	// if (!get_dict_index_folder(exe_folder, dict, index, cfg)) exit(EXIT_FAILURE);
	init_dict_index_folder(exe_folder, dict, index, cfg);
	QApplication a(argc, argv);
#ifndef NDEBUG
	std::cout << "init MainWindow with:\n" << dict << "\n" << index << "\n" << cfg;
#endif
	MainWindow w{dict.c_str(), index.c_str(), cfg.c_str()};
	w.show();
	w.mainInit();
	w.displayText();
	return a.exec();
}
