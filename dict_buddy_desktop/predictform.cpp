//
//  predictform.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2019/3/3.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include <dict_buddy_core/predictmodel.h>
#include "predictform.h"
#include "ui_predictform.h"
#include <iostream>

PredictForm::PredictForm(QWidget* parent) : QWidget(parent), ui(new Ui::PredictForm) {
	ui->setupUi(this);
	listview_ = new PredictListView{this};
	listview_->setProperties_type1();
	listview_->setGeometry(0, 0, 10, 10);
	ui->verticalLayout->addWidget(listview_);
	listview_->setFrameShape(QFrame::NoFrame);
	// setStyleSheet("QWidget {border-radius: 4px;}");
	// setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
}

PredictForm::~PredictForm() {
	delete ui;
	delete predictmodel_;
}

void PredictForm::setModel(dict_core::PredictModel* model) {
	predictmodel_ = model;
	listview_->setModel(predictmodel_);
	delete selectionmodel_;
	selectionmodel_ = new QItemSelectionModel{predictmodel_};
	setSelectionModel(selectionmodel_);
}
void PredictForm::setSelectionModel(QItemSelectionModel* selectionModel) {
	listview_->setSelectionModel(selectionModel);
}

void PredictForm::show_prediction(const query_cluster::predict_type& prediction) {
	predictmodel_->regenerate(prediction);
}

bool PredictForm::selectionModelSelected() { return selectionmodel_->hasSelection(); }

void PredictForm::selectUp() {
	if (predictmodel_->rowCount() > 0) {
		if (selectionmodel_->hasSelection()) {
			auto s = selectionmodel_->currentIndex();
			if (s.row() > 0) {
				selectionmodel_->setCurrentIndex(s.siblingAtRow(s.row() - 1),
												 QItemSelectionModel::ClearAndSelect |
													 QItemSelectionModel::Rows);
			}
		} else {
		}
	} else {
		selectionmodel_->clear();
	}
}

void PredictForm::selectDown() {
	auto rowC = predictmodel_->rowCount();
	if (rowC > 0) {
		if (selectionmodel_->hasSelection()) {
			auto s = selectionmodel_->currentIndex();
			if (s.row() < rowC - 1) {
				selectionmodel_->setCurrentIndex(s.siblingAtRow(s.row() + 1),
												 QItemSelectionModel::ClearAndSelect |
													 QItemSelectionModel::Rows);
			}
		} else {
			selectionmodel_->setCurrentIndex(predictmodel_->index(0, 0),
											 QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
		}
	} else {
		selectionmodel_->clear();
	}
}

// QSize PredictForm::sizeHint() const {
//	return listview_->sizeHint();
//}