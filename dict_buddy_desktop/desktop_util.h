//
//  desktop_util.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/5/17.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef DESKTOP_UTIL_H
#define DESKTOP_UTIL_H
#include "lang_platform.h"
#include <QString>
#if Unix_Style
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;
using boost::system::error_code;
#else
#include <filesystem>
namespace fs = std::filesystem;
#endif

class QScrollBar;
class QWidget;
class QMessageBox;

///@brief call inside main
fs::path get_exe_folder();
///@brief use exe_folder to init dict, index folder and cfg path
void init_dict_index_folder(const fs::path& exe_folder, fs::path& dict, fs::path& index, fs::path& cfg);

int get_scroll_bar_pos(QScrollBar* b);
void set_scroll_bar_pos(QScrollBar* b, int pos);
void set_scroll_bar_style(QScrollBar* b, const char* style);

QMessageBox* init_about_box(QWidget* parent);
void show_about_box(QMessageBox* b);

QMessageBox* init_import_box(QWidget* parent);
void open_import_box(QMessageBox* b);
void update_msgbox_text(QMessageBox* b, const QString& msg);
void enable_msgbox_close(QMessageBox* b);
#endif // DESKTOP_UTIL_H