//
//  mactitlebar.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/2/28.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef MACTITLEBAR_H
#define MACTITLEBAR_H
#include <QGuiApplication>
#include <QWindow>
class MacManager
{
public:
	static void removeTitlebarFromWindow(long winId = -1);
};
#endif // MACTITLEBAR_H