//
//  use_dict.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/5/17.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef USE_DICT_H
#define USE_DICT_H
#include <string>
#include <lexicon_buddy/lexicon/buddy_forward.h>
#include <QUrl>
#include <dict_buddy_core/core_util_forward.h>
#include <dict_buddy_core/audio_system.h>
#include <dict_buddy_core/online_record_parser_forward.h>
#include <dict_buddy_core/types_literal.h>
#include "desktop_util.h"

#ifndef DOC_AU_SCHEME
#define DOC_AU_SCHEME "doc_au_data"
#endif
#ifndef DOC_IMG_SCHEME
#define DOC_IMG_SCHEME "doc_img_data"
#endif

class QTextDocument;
class QTextBrowser;
class MainWindow;

using lexicon::buddy::dict_meta;
using lexicon::buddy::mdx_record_bundle;
using lexicon::buddy::stardict_record_bundle;
using lexicon::buddy::query_cluster;
using std::string;
using std::string_view;

// io context type
using boost::asio::io_context;
// http engine type
template <class handler>
using http_engine = lexicon::online::control<handler>;

enum class offline_handler_category
{
	query_str, /*query with word str*/
	query_pre, /*query with preview selection*/
};

enum class online_handler_category
{
	// process webster json data
	webster_json,
	webster_img,
	// user request audio playback
	webster_audio,
};

class offline_handler
{
	static constexpr char offline_dictname_open[] = "<div style=\"margin-top:6px;\">" EMOJI_BOOK "<b>";
	static constexpr char offline_dictname_close[] = "</b></div>";
	static constexpr char record_opening_tag[] = "<div>";
	static constexpr char record_closing_tag[] = "<div>";
	// todo
	struct stardict
	{
		static constexpr char opening_tag[] = "<div>";
		static constexpr char closing_tag[] = "<br><div>";
		static constexpr char wav_opening_tag[] = "<a href=\"";
		static constexpr char wav_closing_tag[] = "\">wav_todo</a>";
		//		static constexpr char avi_opening_tag[] = "";
		//		static constexpr char avi_closing_tag[] = "\n";
		//		static constexpr char bin_opening_tag[] = "";
		//		static constexpr char bin_closing_tag[] = "\n";
		static constexpr char img_opening_tag[] = "<img src=\"";
		static constexpr char img_closing_tag[] = "\" alt=\"img_todo\">";
	};
	struct mdx
	{};
	QTextBrowser* t_;
	MainWindow* mw_;

public:
	offline_handler(QTextBrowser* t, MainWindow* mw) : t_{t}, mw_{mw} {};
	///@warning thread safety: not safe
	void operator()(const stardict_record_bundle& bundle);
	///@warning thread safety: not safe
	void operator()(const mdx_record_bundle& bundle);
};

const char* scheme_map(const string& src);

dict_core::audio_sys::ctx_t ctx_map(string_view scheme);

std::pair<dict_core::audio_sys::ctx_t, dict_core::audio_sys::name_view_t>
audio_ring_buffer_map(const string& src);

class online_handler
{
	uint8_t doc_id_;
	// operation category
	online_handler_category ca_;
	// gui pointer for adding text to window
	MainWindow* mw_;
	// audio system pointer
	dict_core::audio_sys* au_;
	string name_;
	static constexpr char online_dictname_open[] = "<div style=\"margin-top:6px;\">" EMOJI_EARTH "<b>";
	static constexpr char online_dictname_close[] = "</b></div>";

public:
	// state
	// for audio task, it's the destination of buffer element in audio_sys
	// for webster img task, it's the corresponding img url (actually pit in the img_cor_. as a destination,
	// and it's not url for download img from webster server) in the qt document object.
	// for webster json task, just append text and download img from urls in this fresh loaded text todo
	// webster parser add img list
	union S {
		std::size_t pos;
		void* p;
		dict_core::webster_dictionary dict;
	};

private:
	S state_;

public:
	// todo
	online_handler(uint8_t doc_id, online_handler_category ca, MainWindow* mw, dict_core::audio_sys* au,
				   string name, S state)
		: doc_id_{doc_id}, ca_{ca}, mw_{mw}, au_{au}, name_{name}, state_{state} {}
	// todo handle error

	void operator()(lexicon::online::response_type& http_response);
	void operator()(boost::system::error_code ec);

	// check if it's img extionsion and supported by this program
	static bool supported_img_ext(dict_core::extension ext);
	// check if it's audio extionsion and supported by this program
	static bool supported_audio_ext(dict_core::extension ext);
};

// dict manager type
using dict_manager0 = lexicon::buddy::manager<offline_handler, online_handler>;

class img_extract_tasks
{
	uint8_t doc_id_;
	// http engine pointer
	http_engine<online_handler>* http_client_;
	// gui pointer for adding text to window
	MainWindow* mw_;
	std::set<string> list_;

public:
	img_extract_tasks(uint8_t doc_id, http_engine<online_handler>* http_client, MainWindow* mw)
		: doc_id_{doc_id}, http_client_{http_client}, mw_{mw} {}

	void add_target(const string& doc_img_url_);

	void operator()();

	void clear() { list_.clear(); }
};

using dict_manager = lexicon::buddy::manager<offline_handler, online_handler>;

// including required pointers, states and online routine
class query_task
{
protected:
	uint8_t doc_id_;
	MainWindow* mw_;
	QTextBrowser* t_;
	dict_manager* dm_;
	http_engine<online_handler>* http_client_;
	void start_webster_chain(string word);
	void doc_id_update();
	// clear document and audio ring buffer
	void clear_all();
	query_task(MainWindow* mw, QTextBrowser* t, dict_manager* dm, http_engine<online_handler>* http_client)
		: mw_{mw}, t_{t}, dm_{dm}, http_client_{http_client} {
		doc_id_update();
	}
};

class str_query_task : private query_task
{
	string word_;

public:
	str_query_task(string word, MainWindow* mw, QTextBrowser* t, dict_manager* dm,
				   http_engine<online_handler>* http_client)
		: query_task{mw, t, dm, http_client}, word_{word} {}
	void start_chain_reaction();
};

class preview_query_task : private query_task
{
	string word_;
	// preview
	query_cluster::predict_type::mapped_type prediction_;

public:
	preview_query_task(string word, query_cluster::predict_type::mapped_type prediction, MainWindow* mw,
					   QTextBrowser* t, dict_manager* dm, http_engine<online_handler>* http_client)
		: query_task{mw, t, dm, http_client}, word_{word}, prediction_{prediction} {}
	void start_chain_reaction();
};

class dict_import_task
{
	MainWindow* mw_;
	dict_manager* dm_;
	fs::path src_;

public:
	dict_import_task(MainWindow* mw, dict_manager* dm, const fs::path& src) : mw_(mw), dm_(dm), src_(src) {}
	dict_import_task(MainWindow* mw, dict_manager* dm,  fs::path&& src) : mw_(mw), dm_(dm), src_(src) {}
	dict_import_task(const dict_import_task&) = default;
	void operator()();
};

#endif // USE_DICT_H