//
//  use_dict.cpp
//  dict_buddy
//
//  Created by Lohengrin on 2019/5/17.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include "use_dict.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTextBrowser>
#include <dict_buddy_core/img_util.h>
#include <dict_buddy_core/core_util.h>
#include <dict_buddy_core/online_record_parser.h>
#include <lexicon_buddy/lexicon/buddy.h>
using lexicon::online::URL;
using lexicon::online::urlstr_2_URL;
namespace http = boost::beast::http;
void offline_handler::operator()(const stardict_record_bundle& bundle) {
	// html snippet will be generated and appended to target
	string tmp;
	tmp += offline_dictname_open;
	tmp += bundle.d_mt.get_meta().alias_;
	tmp += offline_dictname_close;
	for (auto& i : bundle.record) {
		tmp += record_opening_tag;
		for (const auto& j : i.index_) {
			switch (j.type_) {
				case 'W': {
					string url{DOC_AU_SCHEME "://"};
					url += mw_->gen_res_id();
					QVariant res{QByteArray::fromRawData(j.begin_, j.size_)};
					// todo save resources in memory
					t_->document()->addResource(dict_core::custom_resource::res_audio,
												QString::fromStdString(url), res);
					tmp += stardict::wav_opening_tag;
					tmp += url;
					tmp += stardict::wav_closing_tag;
				} break;
				case 'P': {
					string url{DOC_IMG_SCHEME "://"};
					url += mw_->gen_res_id();
					QVariant res{QByteArray::fromRawData(j.begin_, j.size_)};
					// todo save resources in memory
					t_->document()->addResource(QTextDocument::ImageResource, QString::fromStdString(url),
												res);
					tmp += stardict::img_opening_tag;
					tmp += url;
					tmp += stardict::img_closing_tag;
				} break;
				case 'X':
					break;
				case 'r':
					// todo cache image and save audio path
					break;
				default:
					tmp += stardict::opening_tag;
					tmp.append(j.begin_, j.size_);
					tmp += stardict::closing_tag;
					break;
			}
		}
		tmp += record_closing_tag;
	}
	t_->append(QString::fromStdString(tmp));
}

void offline_handler::operator()(const mdx_record_bundle& bundle) {
	// html snippet will be generated and appended to target
	string tmp;
	tmp += offline_dictname_open;
	tmp += bundle.d_mt.get_meta().alias_;
	tmp += offline_dictname_close;
	for (auto& i : bundle.record) {
		tmp += record_opening_tag;
		tmp += i;
		tmp += record_closing_tag;
	}
#ifndef NDEBUG
	cout << bundle.d_mt.get_meta().alias_ << ": " << '\n' << tmp << endl;
#endif
	t_->append(QString::fromStdString(tmp));
}

const char* scheme_map(const string& src) {
	if (src == WEBSTER_S_SCHEME_QT)
		return WEBSTER_S_SCHEME;
	else if (src == WEBSTER_ART_SCHEME)
		return "https";
	else if (src == WEBSTER_ARTL_SCHEME)
		return "http";
	else
		return nullptr;
}

dict_core::audio_sys::ctx_t ctx_map(string_view scheme) {
	if (scheme == WEBSTER_S_SCHEME_QT)
		return 'w';
	else
		return 0;
};

std::pair<dict_core::audio_sys::ctx_t, dict_core::audio_sys::name_view_t>
audio_ring_buffer_map(const string& src) {
	string_view scheme{src.data(), src.find_first_of(':')};
	auto mapped_ctx = ctx_map(scheme);
	switch (mapped_ctx) {
		case 'w': {
			auto pos_b = src.find_last_of('/') + 1;
			return {mapped_ctx, {src.data() + pos_b, src.size() - pos_b}};
		}
		default:
			return {};
	}
}

bool online_handler::supported_img_ext(dict_core::extension ext) {
	switch (ext) {
		case dict_core::extension::gif:
			return true;
		case dict_core::extension::jpeg:
			return true;
		case dict_core::extension::png:
			return true;
		default:
			return false;
	}
}
bool online_handler::supported_audio_ext(dict_core::extension ext) {
	switch (ext) {
		case dict_core::extension::wav:
			return true;
		case dict_core::extension::mp3:
			return true;
		default:
			return false;
	}
}

void online_handler::operator()(__attribute__((unused)) boost::system::error_code ec) {
	if (doc_id_ != mw_->get_doc_id()) return;
	switch (ca_) {
		case online_handler_category::webster_img:
			break;
		case online_handler_category::webster_audio: {
			auto h_id = audio_ring_buffer_map(name_);
			if (au_->ring_.get_ctx(state_.pos) != h_id.first or
				au_->ring_.get_name(state_.pos) != h_id.second)
				break;
			au_->ring_.direct_write_identity(state_.pos, 0, string{});
			au_->rs_[state_.pos] = std::remove_pointer_t<std::decay_t<decltype(au_->rs_)>>::empty;
		} break;
		case online_handler_category::webster_json:
			break;
	}
}

// todo report http error
void online_handler::operator()(lexicon::online::response_type& http_response) {
	if (doc_id_ != mw_->get_doc_id()) return;
	if (http_response.result() != http::status::ok) return;
	auto get_url_last_ext = [this]() -> dict_core::string_view {
		auto pos_ = name_.find_last_of('.');
		if (pos_ != name_.npos) {
			return {name_.data() + pos_, name_.size() - pos_};
		} else {
			return {};
		}
	};
	auto mime_view = http_response[http::field::content_type];
	switch (ca_) {
		case online_handler_category::webster_img: {
			auto ext = dict_core::get_extension_from_literal(get_url_last_ext());
			if (!dict_core::is_MIME_valid(ext, {mime_view.data(), mime_view.size()})) return;
			auto format = dict_core::get_literal_from_extension(ext);
			// attach data to the doc
			if (format)
				emit mw_->remote_img_ready(doc_id_, QString::fromStdString(name_), format,
										   http_response.body());
		} break;
		case online_handler_category::webster_audio: {
			emit mw_->remote_audio_ready(doc_id_, state_, name_, http_response);
		} break;
		case online_handler_category::webster_json: {
			auto insert_record_head = [this](string& str, string::size_type& str_pos) {
				str.insert(str_pos, online_dictname_open, sizeof online_dictname_open - 1);
				str_pos += sizeof online_dictname_open - 1;
				constexpr char Collegiate[] = "Merriam-Webster's Collegiate® Dictionary with Audio";
				constexpr char Thesaurus[] = "Merriam-Webster's Collegiate® Thesaurus";
				constexpr char Learner[] = "Merriam-Webster's Learner's Dictionary with Audio";
				switch (state_.dict) {
					case dict_core::webster_dictionary::Collegiate:
						str.insert(str_pos, Collegiate, sizeof Collegiate - 1);
						str_pos += sizeof Collegiate - 1;
						break;
					case dict_core::webster_dictionary::Collegiate_Thesaurus:
						str.insert(str_pos, Thesaurus, sizeof Thesaurus - 1);
						str_pos += sizeof Thesaurus - 1;
						break;
					case dict_core::webster_dictionary::Learner_Dictionary:
						str.insert(str_pos, Learner, sizeof Learner - 1);
						str_pos += sizeof Learner - 1;
						break;
				}
				str.insert(str_pos, online_dictname_close, sizeof online_dictname_close - 1);
				str_pos += sizeof online_dictname_close - 1;
			};
			// content-type check
			if (mime_view.find("application/json") == mime_view.npos) {
				if (mime_view.find("text/html") == mime_view.npos) return;
				string tmp_err{};
				auto& json_dat = http_response.body();
				if (json_dat.empty()) return;
				// allocate space
				tmp_err.resize(json_dat.size() + 100);
				string::size_type tmp_err_pos = 0;
				insert_record_head(tmp_err, tmp_err_pos);
				tmp_err.replace(tmp_err_pos, sizeof("<div>") - 1, "<div>");
				tmp_err_pos += sizeof("<div>") - 1;
				tmp_err.replace(tmp_err_pos, json_dat.size(), json_dat);
				tmp_err_pos += json_dat.size();
				tmp_err.replace(tmp_err_pos, sizeof("</div>") - 1, "</div>");
				tmp_err_pos += sizeof("</div>") - 1;
				tmp_err.resize(tmp_err_pos);
				emit mw_->remote_webster_html_error(doc_id_, QString::fromStdString(tmp_err));
				return;
			}
			// parse
			dict_core::webster_parser wp{};
			wp.use_dict(state_.dict);
			string tmp{};
			auto& json_dat = http_response.body();
			if (json_dat.empty()) return;
			// allocate space
			tmp.resize(wp.buffer_s_required(json_dat.size()));
			string::size_type tmp_pos = 0;
			insert_record_head(tmp, tmp_pos);
			wp.parse_JSON(json_dat, tmp, tmp_pos);
			tmp.resize(tmp_pos);
			// send text to main thread
			if (name_[0] == '#')
				// todo test
				emit mw_->remote_webster_html_ready_t(doc_id_, QString::fromStdString(tmp), wp, name_);
			else
				emit mw_->remote_webster_html_ready(doc_id_, QString::fromStdString(tmp), wp);
		} break;
	}
}

void img_extract_tasks::add_target(const string& doc_img_url_) { list_.emplace(doc_img_url_); }

void img_extract_tasks::operator()() {
	// todo url encoding
	for (auto&& i : list_) {
		// parse url
		URL u = urlstr_2_URL(i);
		// "restore" url
		auto p_s = scheme_map(u.scheme);
		// ignore unknown scheme
		if (!p_s) continue;
		u.scheme = p_s;
		if (u.scheme == "http") {
			u.port = lexicon::online::PORT_HTTP;
		} else if (u.scheme == "https") {
			u.port = lexicon::online::PORT_HTTPS;
		} else {
			continue;
		}
		// remote img -> download task
		http_client_->task_enqueue(
			u, online_handler{doc_id_, online_handler_category::webster_img, mw_, nullptr, i, {}});
	}
}

void query_task::doc_id_update() { doc_id_ = mw_->update_doc_id(); }

void query_task::start_webster_chain(string word) {
	online_handler::S s;
	s.dict = dict_core::webster_dictionary::Collegiate;
	dm_->query_webster_Collegiate_Dictionary(
		word, {doc_id_, online_handler_category::webster_json, mw_, nullptr, word, s});
	s.dict = dict_core::webster_dictionary::Collegiate_Thesaurus;
	dm_->query_webster_Collegiate_Thesaurus(
		word, {doc_id_, online_handler_category::webster_json, mw_, nullptr, word, s});
	dm_->notify_http_thread();
}

void query_task::clear_all() {
	t_->document()->clear();
	mw_->get_audio_sys()->stop_and_clear();
}

void str_query_task::start_chain_reaction() {
	clear_all();
	doc_id_update();
	start_webster_chain(word_);
	offline_handler off_h{t_, mw_};
	dm_->offline_query_str(word_, off_h, mw_->get_ctx());
}

void preview_query_task::start_chain_reaction() {
	clear_all();
	doc_id_update();
	start_webster_chain(word_);
	offline_handler off_h{t_, mw_};
	dm_->offline_query(prediction_, off_h);
}

void dict_import_task::operator()() {
	// import
	dm_->import_dict(src_);
	// notify
	emit mw_->complete_import_box("import completes");
}