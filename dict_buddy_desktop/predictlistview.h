//
//  predictlistview.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/5.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef PREDICTLISTVIEW_H
#define PREDICTLISTVIEW_H

#include <QListView>
class PredictListView : public QListView
{
public:
	static constexpr int type1_width = 380;
	PredictListView(QWidget* parent = nullptr);
	void setProperties_type1();
	// QSize sizeHint() const override;
};

#endif // PREDICTLISTVIEW_H