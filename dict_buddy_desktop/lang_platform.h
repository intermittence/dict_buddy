//
//  lang_platform.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/5/17.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef LANG_PLATFORM_H
#define LANG_PLATFORM_H

#include <stdio.h>
// to check clang cpp standard
#ifndef __cplusplus
#define clang_cpp_version_flag __cplusplus
#endif // !__cplusplus
// http://nadeausoftware.com/articles/2012/01/c_c_tip_how_use_compiler_predefined_macros_detect_operating_system
#if !defined(_WIN32) && (defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__)))
/* UNIX-style OS. ------------------------------------------- */
#define Unix_Style true
#else
#define Unix_Style false
#endif

// TODO: file path cross platform
#if Unix_Style
#ifndef CHAR_TYPE
#define CHAR_TYPE char
#endif
#define STR_LITERAL_PREFIX(s) u8##s
#else
#ifndef CHAR_TYPE
#define CHAR_TYPE wchar_t
#endif
#define STR_LITERAL_PREFIX(s) L##s
#endif
// endianness
// this project require little endian machine

#endif // LANG_PLATFORM_H
