//
//  dict_pref_dialog.cpp
//
//  Created by Lohengrin on 2019/9/10.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include "dict_pref_dialog.h"
#include "ui_dict_pref_dialog.h"
#include <QHBoxLayout>
#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <lexicon_buddy/lexicon/buddy.h>
#include "use_dict.h"

#if (defined(__APPLE__) && defined(__MACH__))
#include "mactitlebar.h"
#endif

#ifndef Enable_Apply_if_Valid
#define Enable_Apply_if_Valid                                                                                \
	if (coll_key_validity and thes_key_validity)                                                             \
		apply_button->setEnabled(true);                                                                      \
	else                                                                                                     \
		apply_button->setEnabled(false)
#endif

void dict_pref_dialog::align() {
	// online part
	int left = 0;
	int top = 0;
	int right = 0;
	int bottom = 0;
	collegiate_apikey->getContentsMargins(&left, &top, &right, &bottom);
	right = 6;
	collegiate_apikey->setContentsMargins(left, top, right, bottom);
	thesaurus_apikey->setContentsMargins(left, top, right, bottom);
	// offline
	//	int offline_begin = SERVICE_ROWS_COUNT;
	//	int offline_end = v_layout->count() - TAIL_ROWS_COUNT - SERVICE_ROWS_COUNT;
	//	while (offline_begin != offline_end) {
	//		auto row_layout_item = v_layout->itemAt(offline_begin);
	//		auto row_layout = row_layout_item->layout();
	//		auto button = row_layout->itemAt(3)->widget();
	//		++offline_begin;
	//	}
}

void dict_pref_dialog::open() {
	load_webster();
	init_load_offline();
	align();
	apply_button->setEnabled(false);
	show();
}

void dict_pref_dialog::ok_pref() {
	if (apply_button->isEnabled()) apply_pref();
	close_pref();
}

dict_pref_dialog::dict_pref_dialog(dict_manager* d, QWidget* parent)
	: QDialog(parent), ui(new Ui::dict_pref_dialog), dm_p(d) {
	ui->setupUi(this);
	v_layout = new QVBoxLayout();
	int left = 0;
	int top = 0;
	int right = 0;
	int bottom = 0;
	ui->buttonBox->getContentsMargins(&left, &top, &right, &bottom);
	ui->buttonBox->setContentsMargins(9, top, 9, bottom);
	ui->scrollArea->setFrameShape(QFrame::NoFrame);
	ui->scrollAreaWidgetContents->setLayout(v_layout);
	// ui->scrollArea->setStyleSheet("QScrollArea {border: none;}");
	v_layout->setSpacing(3);
	v_layout->setContentsMargins(0, 0, 0, 0);
	// control widget
	auto collegiate_label = new QLabel(u8"Merriam-Webster's Collegiate® Dictionary with Audio");
	auto thesaurus_label = new QLabel(u8"Merriam-Webster's Collegiate® Thesaurus");
	collegiate_label->setTextInteractionFlags(Qt::TextSelectableByMouse);
	thesaurus_label->setTextInteractionFlags(Qt::TextSelectableByMouse);
	auto key_label0 = new QLabel("key:");
	auto key_label1 = new QLabel("key:");
	collegiate_activation = new QCheckBox();
	thesaurus_activation = new QCheckBox();
	collegiate_apikey = new QLineEdit();
	thesaurus_apikey = new QLineEdit();
	collegiate_apikey->setProperty("webster_dict", "coll");
	thesaurus_apikey->setProperty("webster_dict", "thes");
	// layout
	collegiate_hbox0 = new QHBoxLayout();
	collegiate_hbox1 = new QHBoxLayout();
	thesaurus_hbox0 = new QHBoxLayout();
	thesaurus_hbox1 = new QHBoxLayout();
	collegiate_hbox0->setAlignment(Qt::AlignLeft);
	collegiate_hbox1->setAlignment(Qt::AlignLeft);
	thesaurus_hbox0->setAlignment(Qt::AlignLeft);
	thesaurus_hbox1->setAlignment(Qt::AlignLeft);
	apply_button = ui->buttonBox->button(QDialogButtonBox::Apply);
	ok_button = ui->buttonBox->button(QDialogButtonBox::Ok);
	cancel_button = ui->buttonBox->button(QDialogButtonBox::Cancel);
	apply_button->setEnabled(false);
	// add control to layout
	collegiate_hbox0->addWidget(collegiate_activation);
	collegiate_hbox0->addWidget(collegiate_label);
	collegiate_hbox1->addWidget(key_label0);
	collegiate_hbox1->addWidget(collegiate_apikey);
	thesaurus_hbox0->addWidget(thesaurus_activation);
	thesaurus_hbox0->addWidget(thesaurus_label);
	thesaurus_hbox1->addWidget(key_label1);
	thesaurus_hbox1->addWidget(thesaurus_apikey);
	// add layout to layout
	v_layout->addLayout(collegiate_hbox0);
	v_layout->addLayout(collegiate_hbox1);
	v_layout->addLayout(thesaurus_hbox0);
	v_layout->addLayout(thesaurus_hbox1);
	v_layout->addStretch();
	// action
	connect(apply_button, &QPushButton::clicked, this, &std::remove_pointer_t<decltype(this)>::apply_pref);
	connect(ok_button, &QPushButton::clicked, this, &std::remove_pointer_t<decltype(this)>::ok_pref);
	connect(cancel_button, &QPushButton::clicked, this, &std::remove_pointer_t<decltype(this)>::close_pref);
	// window name
	setWindowTitle("Dictionaries");
	// window style
#if (defined(__APPLE__) && defined(__MACH__))
	MacManager::removeTitlebarFromWindow(winId());
#endif
}

void dict_pref_dialog::load_webster() {
	// load state from dict class
	// 1.load activation state
	using web = decltype(dm_p->webster_);
	collegiate_activation->setCheckState(
		dm_p->webster_.dict_activated(web::Collegiate_Dictionary) ? Qt::Checked : Qt::Unchecked);
	thesaurus_activation->setCheckState(
		dm_p->webster_.dict_activated(web::Collegiate_Thesaurus) ? Qt::Checked : Qt::Unchecked);
	// 2.load api key
	auto k = dm_p->webster_.get_access_key();
	collegiate_apikey->setText(QString::fromStdString(k.Collegiate_key));
	thesaurus_apikey->setText(QString::fromStdString(k.Thesaurus_key));
	// trigger
	connect(collegiate_activation, &std::remove_pointer_t<decltype(collegiate_activation)>::toggled, this,
			&std::remove_pointer_t<decltype(this)>::s_service_modified);
	connect(collegiate_apikey, &std::remove_pointer_t<decltype(collegiate_apikey)>::textChanged, this,
			&std::remove_pointer_t<decltype(this)>::s_service_modified);
	connect(thesaurus_activation, &std::remove_pointer_t<decltype(thesaurus_activation)>::toggled, this,
			&std::remove_pointer_t<decltype(this)>::s_service_modified);
	connect(thesaurus_apikey, &std::remove_pointer_t<decltype(thesaurus_apikey)>::textChanged, this,
			&std::remove_pointer_t<decltype(this)>::s_service_modified);
}

void dict_pref_dialog::clear_offline() {
	int offline_begin = SERVICE_ROWS_COUNT;
	int offline_count = v_layout->count() - TAIL_ROWS_COUNT - SERVICE_ROWS_COUNT;
	int count = 0;
	while (count != offline_count) {
		auto row_layout_item = v_layout->itemAt(offline_begin);
		auto row_layout = row_layout_item->layout();
		v_layout->removeItem(row_layout_item);
		QLayoutItem* child;
		while ((child = row_layout->takeAt(0)) != 0) {
			delete child->widget();
			delete child;
		}
		++count;
	}
	modified_keys.clear();
}

void dict_pref_dialog::close_pref() {
	clear_offline();
	hide();
}

void dict_pref_dialog::init_load_offline() {
	auto interface = dm_p->get_interface();
	int insert_pos = SERVICE_ROWS_COUNT;
	for (auto&& i : *interface) {
		auto layout_ = new QHBoxLayout();
		layout_->setAlignment(Qt::AlignLeft);
		v_layout->insertLayout(insert_pos, layout_);
		// key (filename with ext)
		auto off_filename = new QLabel(QString::fromStdString(i.first));
		off_filename->setTextInteractionFlags(Qt::TextSelectableByMouse);
		auto off_activation = new QCheckBox();
		auto off_alias = new QLineEdit();
		auto off_delete = new QPushButton("Delete");
		off_delete->setProperty("layout_item_p", QVariant::fromValue((void*)v_layout->itemAt(insert_pos)));
		layout_->addWidget(off_activation);
		layout_->addWidget(off_filename);
		layout_->addWidget(off_alias);
		layout_->addWidget(off_delete);
		off_activation->setCheckState(i.second.meta_p->enabled() ? Qt::Checked : Qt::Unchecked);
		off_alias->setText(QString::fromStdString(i.second.meta_p->alias()));
		auto key = QString::fromStdString(i.first);
		off_activation->setProperty("res_key", key);
		off_alias->setProperty("res_key", key);
		off_delete->setProperty("res_key", key);
		connect(off_activation, &std::remove_pointer_t<decltype(off_activation)>::toggled, this,
				&std::remove_pointer_t<decltype(this)>::s_res_modified);
		connect(off_alias, &std::remove_pointer_t<decltype(off_alias)>::textEdited, this,
				&std::remove_pointer_t<decltype(this)>::s_res_modified);
		// connect(off_alias, &std::remove_pointer_t<decltype(off_alias)>::textChanged, this,
		//&std::remove_pointer_t<decltype(this)>::on_alias_modified);
		connect(off_delete, &std::remove_pointer_t<decltype(off_delete)>::clicked, this,
				&std::remove_pointer_t<decltype(this)>::remove);
		++insert_pos;
	}
}

void dict_pref_dialog::s_res_modified() {
	modified_keys.emplace(sender()->property("res_key").toString());
	offline_dirty = true;
	Enable_Apply_if_Valid;
}

void dict_pref_dialog::s_service_modified() {
	service_dirty = true;
	Enable_Apply_if_Valid;
}

void dict_pref_dialog::s_apikey_modified(const QString& text) {
	service_dirty = true;
	// validation
	bool valid = text.size();
	auto webster_dict = sender()->property("webster_dict").toString();
	if (webster_dict == "coll")
		coll_key_validity = valid;
	else
		thes_key_validity = valid;
	Enable_Apply_if_Valid;
}

void dict_pref_dialog::apply_pref() {
	apply_button->setEnabled(false);
	// save
	decltype(dm_p->webster_)::api_key_t api_key{};
	// 1.coll
	auto coll_activation = ((QCheckBox*)collegiate_hbox0->itemAt(0)->widget())->isChecked();
	dm_p->webster_.dict_activation(decltype(dm_p->webster_)::Collegiate_Dictionary, coll_activation);
	api_key.Collegiate_key = ((QLineEdit*)collegiate_hbox1->itemAt(1)->widget())->text().toStdString();
	// 2.thes
	auto thes_activation = ((QCheckBox*)thesaurus_hbox0->itemAt(0)->widget())->isChecked();
	dm_p->webster_.dict_activation(decltype(dm_p->webster_)::Collegiate_Thesaurus, thes_activation);
	api_key.Thesaurus_key = ((QLineEdit*)thesaurus_hbox1->itemAt(1)->widget())->text().toStdString();
	dm_p->webster_.set_access_key(api_key);
	// 3.save coll and thes settings
	dm_p->save_webster_setting();
	// 4.offline dictionaries
	auto* interface = dm_p->get_interface();
	auto save_offline = [this, interface](int index) {
		auto layout = (QLayout*)v_layout->itemAt(index);
		auto id = ((QCheckBox*)layout->itemAt(0)->widget())->property("res_key").toString();
		auto ret0 = modified_keys.find(id);
		if (ret0 == modified_keys.end()) return;
		modified_keys.erase(ret0);
		auto activation = ((QCheckBox*)layout->itemAt(0)->widget())->isChecked();
		auto filename = ((QLabel*)layout->itemAt(1)->widget())->text().toStdString();
		auto alias = ((QLineEdit*)layout->itemAt(2)->widget())->text().toStdString();
		auto ret = interface->find(filename);
		if (ret != interface->end()) {
			auto p = ret->second.meta_p;
			p->set_enabled(activation);
			p->set_alias(alias);
		}
	};
	int row_end = v_layout->count() - TAIL_ROWS_COUNT;
	for (int i = SERVICE_ROWS_COUNT; i < row_end; ++i)
		save_offline(i);
	// marked as dirty
	dm_p->cfg_dirty();
}

void dict_pref_dialog::remove() {
	auto layout_item = (QLayoutItem*)(sender()->property("layout_item_p").value<void*>());
	auto row_layout = layout_item->layout();
	auto file_qstr = sender()->property("res_key").toString();
	auto file_str = file_qstr.toStdString();
	modified_keys.erase(file_qstr);
	// remove layout_item from layout
	v_layout->removeItem(layout_item);
	// remove widgets of the layout_item
	QLayoutItem* child;
	while ((child = row_layout->takeAt(0)) != 0) {
		delete child->widget();
		delete child;
	}
	// remove the layout_item
	delete layout_item;
	// remove res
	dm_p->remove_dict(file_str);
	dm_p->cfg_dirty();
}

dict_pref_dialog::~dict_pref_dialog() { delete ui; }