//
//  predictframe.cpp
//
//  Created by Lohengrin on 2019/9/17.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include "predictframe.h"
#include "ui_predictframe.h"
#include <dict_buddy_core/predictmodel.h>
#include "predictlistview.h"
#include <iostream>

PredictFrame::PredictFrame(QWidget* parent) : QFrame(parent), ui(new Ui::PredictFrame) {
	ui->setupUi(this);
	listview_ = new PredictListView{this};
	listview_->setProperties_type1();
	listview_->setGeometry(0, 0, 10, 10);
	ui->verticalLayout->addWidget(listview_);
	// listview_->setFrameShape(QFrame::Panel);
	// setStyleSheet("QFrame{border-radius: 20px;}");
	// setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
}

PredictFrame::~PredictFrame() {
	delete ui;
	delete predictmodel_;
}

void PredictFrame::setModel(dict_core::PredictModel* model) {
	predictmodel_ = model;
	listview_->setModel(predictmodel_);
	delete selectionmodel_;
	selectionmodel_ = new QItemSelectionModel{predictmodel_};
	setSelectionModel(selectionmodel_);
}

void PredictFrame::setSelectionModel(QItemSelectionModel* selectionModel) {
	listview_->setSelectionModel(selectionModel);
}

void PredictFrame::show_prediction(const query_cluster::predict_type& prediction) {
	predictmodel_->regenerate(prediction);
}

bool PredictFrame::selectionModelSelected() { return selectionmodel_->hasSelection(); }

void PredictFrame::selectUp() {
	if (predictmodel_->rowCount() > 0) {
		if (selectionmodel_->hasSelection()) {
			auto s = selectionmodel_->currentIndex();
			if (s.row() > 0) {
				selectionmodel_->setCurrentIndex(s.siblingAtRow(s.row() - 1),
												 QItemSelectionModel::ClearAndSelect |
													 QItemSelectionModel::Rows);
			}
		} else {
		}
	} else {
		selectionmodel_->clear();
	}
}

void PredictFrame::selectDown() {
	auto rowC = predictmodel_->rowCount();
	if (rowC > 0) {
		if (selectionmodel_->hasSelection()) {
			auto s = selectionmodel_->currentIndex();
			if (s.row() < rowC - 1) {
				selectionmodel_->setCurrentIndex(s.siblingAtRow(s.row() + 1),
												 QItemSelectionModel::ClearAndSelect |
													 QItemSelectionModel::Rows);
			}
		} else {
			selectionmodel_->setCurrentIndex(predictmodel_->index(0, 0),
											 QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
		}
	} else {
		selectionmodel_->clear();
	}
}

void PredictFrame::show_if_not_empty() {
	if (predictmodel_ and predictmodel_->rowCount()) show();
}
// QSize PredictForm::sizeHint() const {
//	return listview_->sizeHint();
//}