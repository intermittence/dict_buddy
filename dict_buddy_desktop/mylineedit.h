//
//  mylineedit.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/9.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef MYLINEEDIT_H
#define MYLINEEDIT_H
#include <QLineEdit>
class QTextEdit;
class QString;

class MyLineEdit : public QLineEdit
{
	Q_OBJECT

public:
    MyLineEdit(QWidget* parent = nullptr);
	//~MyLineEdit();

signals:
	void focused(bool focused);

protected:
	virtual void focusInEvent(QFocusEvent* e);
	virtual void focusOutEvent(QFocusEvent* e);
	virtual void dragEnterEvent(QDragEnterEvent* e);
	virtual void dragLeaveEvent(QDragLeaveEvent* e);
	virtual void dragMoveEvent(QDragMoveEvent* e);
    // target to display records
    QTextEdit* t_{};
};

#endif // MYLINEEDIT_H
