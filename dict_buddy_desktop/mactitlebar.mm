//
//  mactitlebar.mm
//  dict_buddy
//
//  Created by Lohengrin on 2019/2/28.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include "mactitlebar.h"
#include <Cocoa/Cocoa.h>
void MacManager::removeTitlebarFromWindow(long winId) {
	if (winId == -1) {
		QWindowList windows = QGuiApplication::allWindows();
		QWindow* win = windows.first();
		winId = win->winId();
	}

	NSView* nativeView = reinterpret_cast<NSView*>(winId);
	NSWindow* nativeWindow = [nativeView window];

	//	[nativeWindow
	//		setStyleMask:[nativeWindow styleMask] | NSFullSizeContentViewWindowMask | NSWindowTitleHidden];
	[nativeWindow
		setStyleMask:[nativeWindow styleMask] | NSWindowStyleMaskFullSizeContentView | NSWindowTitleHidden];
	[nativeWindow setTitlebarAppearsTransparent:YES];
	[nativeWindow setMovableByWindowBackground:FALSE];
}