//
//  predictform.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/3.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef PREDICTFORM_H
#define PREDICTFORM_H

#include <QWidget>
#include <QItemSelectionModel>
#include "predictlistview.h"
#include <lexicon_buddy/lexicon/buddy_forward.h>

namespace Ui
{
class PredictForm;
}

using lexicon::buddy::query_cluster;
namespace dict_core
{
class PredictModel;
} // namespace dict_core

class PredictForm : public QWidget
{
	Q_OBJECT

public:
	explicit PredictForm(QWidget* parent = nullptr);
	~PredictForm();
	void setModel(dict_core::PredictModel* model);
	void setSelectionModel(QItemSelectionModel* selectionModel);
	// QSize sizeHint() const override;
	void adjustListView() {
		//	listview_->adjustSize();
		this->adjustSize();
	}
	void selectUp();
	void selectDown();
	bool selectionModelSelected();
	QModelIndex getSelectIndex() { return selectionmodel_->currentIndex(); }
	void show_prediction(const query_cluster::predict_type& prediction);
	bool isSelected() { return selectionmodel_->hasSelection(); }

private:
	Ui::PredictForm* ui;
	dict_core::PredictModel* predictmodel_{};
	PredictListView* listview_{};
	QItemSelectionModel* selectionmodel_{};
};

#endif // PREDICTFORM_H