//
//  audio_system.cpp
//  dict_buddy
//
//  Created by Lohengrin on 2019/7/31.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include <dict_buddy_core/audio_system.h>
namespace dict_core
{
void audio_sys::stop_and_clear() {
	agent_.stop_strm();
	agent_.free_strm();
}
} // namespace dict_core