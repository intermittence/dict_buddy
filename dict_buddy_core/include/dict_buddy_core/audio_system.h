//
//  audio_system.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/7/31.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef AUDIO_SYSTEM_H
#define AUDIO_SYSTEM_H
#include <bass.h>
#include <limits>
#include <string>
#include <vector>
namespace dict_core
{
using std::string;
using std::string_view;

template <class ctx_type, class name_type, class name_view_type, class res_type, std::size_t size,
		  bool overwrite = false>
class ring_buffer
{
public:
	using ctx_t = ctx_type;
	using name_t = name_type;
	using name_view_t = name_view_type;
	using res_t = res_type;

private:
	// put key and data in separate array make key search operation cache friendly
	ctx_t ctxs_[size]{};
	name_t names_[size]{};
	res_t ress_[size]{};
	// the position after rightmost element (as a ring)
	std::size_t snake_head_;

public:
	static constexpr std::size_t end_ = std::numeric_limits<std::size_t>::max();
	// random access
	std::size_t get_head() { return snake_head_; }
	std::size_t update_head() {
		snake_head_ == size - 1 ? snake_head_ = 0 : ++snake_head_;
		return snake_head_;
	}

	std::size_t get_element_p(ctx_t ctx, name_view_type name) {
		std::size_t p{};
		do {
			if (ctxs_[p] == ctx && names_[p] == name) return p;
		} while (++p < size);
		return end_;
	}

	res_t& operator[](std::size_t pos) { return ress_[pos]; }
	const ctx_t& get_ctx(std::size_t pos) { return ctxs_[pos]; }
	const name_t& get_name(std::size_t pos) { return names_[pos]; }

	// record new one
	template <class name_type0, class res_type0>
	std::size_t cyclic_write(ctx_t ctx, name_type0&& name, res_type0&& res) {
		static_assert(std::is_same_v<name_type0, name_t>, "ring_buffer name type mismatch\n");
		static_assert(std::is_same_v<res_type0, res_t>, "ring_buffer resource type mismatch\n");
		auto p = get_element_p(ctx, name);
		if (p == end_) {
			ctxs_[snake_head_] = ctx;
			names_[snake_head_] = name;
			ress_[snake_head_] = res;
			auto tmp = snake_head_;
			snake_head_ = snake_head_ == size - 1 ? 0 : snake_head_ + 1;
			return tmp;
		} else {
			if constexpr (overwrite) {
				ress_[p] = res;
			}
			return p;
		}
	}

	template <class name_type0>
	void direct_write_identity(std::size_t pos, ctx_t ctx, name_type0&& name) {
		static_assert(std::is_same_v<name_type0, name_t>, "ring_buffer name type mismatch\n");
		ctxs_[pos] = ctx;
		names_[pos] = name;
	}
};

class sound_agent_bass
{
	struct devic_info_t
	{
		DWORD device_;
		BASS_DEVICEINFO info_;
	};
	using device_list_t = std::vector<devic_info_t>;
	device_list_t d_list_{};
	DWORD device_enabled = 0;
	HSTREAM strm_ = 0;
	// todo curent stream?

public:
	sound_agent_bass() {}

	void update_device_list() {
		d_list_.clear();
		devic_info_t d_;
		for (int a = 1; BASS_GetDeviceInfo(a, &d_.info_); a++) {
			d_.device_ = a;
			d_list_.emplace_back(d_);
		}
	}

	device_list_t device_list() { return d_list_; }

	bool init_device(DWORD device = -1) {
		if (BASS_Init(device, 44100, 0, 0, NULL)) {
			device_enabled = device;
			return true;
		}
		return false;
	}

	bool restart(DWORD device = -1) {
		BASS_Free();
		return BASS_Init(device, 44100, 0, 0, NULL);
	}

	bool select_device(DWORD device) { return BASS_SetDevice(device); }

	bool play_back(void* p, QWORD len) {
		if (strm_) {
			stop_strm();
			free_strm();
		}
		strm_ = BASS_StreamCreateFile(true, p, 0, len, 0);
		if (strm_) {
			return BASS_ChannelPlay(strm_, false);
		} else {
			return false;
		}
	}

	void set_strm(void* p, QWORD len) {
		if (strm_) {
			stop_strm();
			free_strm();
		}
		strm_ = BASS_StreamCreateFile(true, p, 0, len, 0);
	}

	bool play_current_strm() { return BASS_ChannelPlay(strm_, false); }

	void stop_strm() {
		if (strm_) BASS_ChannelStop(strm_);
	}

	void free_strm() {
		if (strm_) BASS_StreamFree(strm_);
		strm_ = 0;
	}

	~sound_agent_bass() {
		free_strm();
		BASS_Free();
	}
};

// provide resource container and playback system control
struct audio_sys
{
	static constexpr std::size_t ring_size = 10;
	using ctx_t = char;
	using name_t = string;
	using name_view_t = string_view;
	using audio_res_t = string;
	using ring_buffer_t = ring_buffer<char, string, name_view_t, audio_res_t, ring_size>;
	static constexpr std::size_t end_ = ring_buffer_t::end_;

	enum class res_state
	{
		empty,
		poll,
		ready
	};
	// buffer
	ring_buffer_t ring_;
	// work with ring buffer
	res_state rs_[ring_size]{res_state::empty};
	// play back backend
	sound_agent_bass agent_{};
	// position of the element which user wants to play in ring buffer
	std::size_t play_p_ = end_;
	void stop_and_clear();
};

} // namespace dict_core
#endif // AUDIO_SYSTEM_H