//
//  core_util.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/8/7.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef UTIL_H
#define UTIL_H
#include <string>
#include <string_view>
#include "core_util_forward.h"
namespace dict_core
{

using std::string_view;
using std::string;

// ".jpeg" to extension::jpeg
extension get_extension_from_literal(string_view ext_view);

const char* get_literal_from_extension(extension ext);

bool is_MIME_valid(extension ext, string_view MIME_view);

bool ascii_only(std::string_view str);

// url encoding function from wikipedia with modification
extern char rfc3986[256];
extern char html5[256];

void url_encoder_rfc_tables_init();

char* url_encode(const unsigned char* s, std::size_t len, char* enc, char* table = rfc3986);

string_view url_encode_view(const unsigned char* s, std::size_t len, char* enc,
									 char* table = rfc3986);
/// @note easy to use, but less efficient than url_encode_stdstringview(which requires an external buffer)
string url_encode_str(const unsigned char* s, std::size_t len, char* table = rfc3986);

struct encoder_init
{
	encoder_init() { url_encoder_rfc_tables_init(); }
};

extern encoder_init init__;

} // namespace dict_core
#endif // UTIL_H
