//
//  online_record_parser.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/5/16.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef ONLINE_RECORD_PARSER_H
#define ONLINE_RECORD_PARSER_H

#include "types_literal.h"
#include "online_record_parser_forward.h"
#include <string>
#include <QString>
#include <iostream>
#include <string_view>
#include <map>
#include <deque>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <exception>

namespace dict_core
{

using std::cout;
using std::endl;
using std::string;
using std::string_view;
using std::deque;

class webster_data_exception : public std::runtime_error
{
	using std::runtime_error::runtime_error;
};
class webster_token_exception : public std::runtime_error
{
	using std::runtime_error::runtime_error;
};
/// derived from @link https://www.dictionaryapi.com/products/json#sec-2
enum class webster_data_structure
{
	// ===== Merriam-Webster's Collegiate Dictionary =====
	meta = 0, /*Entry Metadata*/
	id,
	uuid,
	sort,
	src,
	section,
	stems,
	offensive,
	hom, /*Homograph*/
	hwi, /*Headword Information*/
	hw,
	ahws, /*Alternate Headwords*/
	vrs,  /*Variants*/
	va,
	vl,
	prs, /*Pronunciations*/
	mw,
	l,
	l2,
	pun,
	sound,
	// ----- labels -----
	fl,	/*Functional Label*/
	lbs,   /*General Labels*/
	sls,   /*Subject/Status Labels*/
	psl,   /*Parenthesized Subject/Status Label*/
	spl,   /*Sense-Specific Inflection Plural Label*/
	sgram, /*Sense-Specific Grammatical Label*/
	// ----- labels -----
	ins, /*Inflections*/
	if_ /*if*/,
	ifc,
	il,
	cxs, /*Cognate Cross-References*/
	cxl,
	cxtis,
	cxr,
	cxt,
	cxn,
	// ----- sense organization -----
	def,	 /*Definition Section*/
	vd,		 /*Verb Divider*/
	sseq,	/*Sense Sequence*/
	sense,   /*Sense*/
	sn,		 /*Sense Number*/
	dt,		 /*Defining Text*/
	sdsense, /*Divided Sense*/
	sd,
	sen,  /*Truncated Sense*/
	bs,   /*Binding Substitute*/
	pseq, /*Parenthesized Sense Sequence*/
	// ----- sense organization -----
	vis, /*Verbal Illustrations*/
	t,
	aq, /*Attribution of Quote*/
	auth,
	source,
	aqdate,
	subsource,
	ri, /*Run-In*/
	riw,
	rie,
	text,
	bnw, /*Biographical Name Wrap*/
	pname,
	sname,
	altname,
	ca, /*Called-Also Note*/
	intro,
	cats,
	cat,
	catref,
	pn,
	snote, /*Supplemental Information Note*/
	uns,   /*Usage Notes*/
	uros,  /*Undefined Run-Ons*/
	ure,
	utxt,
	dros, /*Defined Run-Ons*/
	drp,
	dxnls,  /*Directional Cross-Reference Section*/
	usages, /*Usage Section*/
	pl,
	pt,
	uarefs,
	uaref,
	syns, /*Synonyms Section*/
	sarefs,
	quotes, /*Quotations Section*/
	art,	/*Artwork*/
	artid,
	capt,
	table, /*Tables*/
	tableid,
	displayname,
	et, /*Etymology*/
	et_snote,
	date,	 /*First Known Use*/
	shortdef, /*Short Definition*/
	// ===== Merriam-Webster's Collegiate Dictionary =====

	// ===== OTHER DICTIONARIES AND THESAURUSES =====
	// meta (Metadata items unique to data sets other than Collegiate are described below)
	ants,
	target,
	tsrc,
	tuuid,
	// meta
	ipa,	  /*IPA PRONUNCIATION format*/
	wod,	  /*Word-OF-THE-DAY PRONUNCIATIONS format*/
	g,		  /*GEOGRAPHICAL DIRECTION*/
	syn_list, /*SYNONYM LISTS*/
	wd,
	wsls, /*SUBJECT/STATUS LABELS FOR THESAURUS WORD*/
	wsl,
	wvrs, /*VARIANTS OF THESAURUS WORD*/
	wvl,
	wva,
	wvbvrs, /*VERB VARIANTS OF THESAURUS WORD*/
	wvbvl,
	wvbva,
	rel_list,	/*RELATED WORD LISTS*/
	phrase_list, /*SYNONYMOUS PHRASE LISTS*/
	near_list,   /*NEAR ANTONYM LISTS*/
	ant_list,	/*ANTONYM LISTS*/
	srefs,		 /*SYNONYM CROSS-REFERENCES*/
	urefs,		 /*USAGE CROSS-REFERENCES*/
	list,		 /*SELF-EXPLANATORY LIST*/
	// ===== OTHER DICTIONARIES AND THESAURUSES =====

	// ===== Advanced English Learner's Dictionary =====
	// meta
	highlight,
	app_shortdef,
	// meta
	altprs, /*ALTERNATE PRONUNCIATIONS*/
	pr,
	gram,	 /*GRAMMATICAL NOTE*/
	wsgram,   /*GRAMMATICAL LABEL WITHIN A SENSE*/
	bnote,	/*BOLD-ITALIC NOTE*/
	snotebox, /*BOXED SUPPLEMENTAL INFORMATION NOTE*/
	phrasev,  /*PHRASAL VERBS*/
	phr,
	pva,
	pvl,
	sphrasev, /*PHRASAL VERBS*/
	phrs,
	phsls, /*PHRASAL VERB SUBJECT/STATUS LABELS*/
	rsl,   /*RUN-ON SUBJECT/STATUS LABEL*/
	artl,  /*LEARNER'S DICTIONARY ARTWORK*/
	dim,
	// ===== Advanced English Learner's Dictionary =====
	error_data_structure,
};

enum class webster_data_structure_meta
{
	pair,
	list,
	ctx,
	pair_require_ctx,
	// todo
	// context
	// require context
	// vis?
	// AHWS list
	// meta highlight

};

webster_data_structure_meta get_webster_data_structure_meta(webster_data_structure s);

webster_data_structure data_structure_type(const string& str);

/// derived from @link https://www.dictionaryapi.com/products/json#sec-2.tokens
enum class webster_tokens
{
	// ===== Merriam-Webster's Collegiate Dictionary =====
	// ----- Formatting and Punctuation Tokens -----
	b = 0, /*token pair, bold*/
	b__,
	bc,  /*ubiquitous bold colon (output a bold colon and a space)*/
	inf, /*token pair, subscript*/
	it,  /*token pair, italics*/
	inf__,
	it__,
	ldquo, /*left double quotes*/
	rdquo, /*right double quotes*/
	sc,	/*token pair, small capitals*/
	sup,   /*token pair, superscript*/
	// ----- Formatting and Punctuation Tokens -----
	// ----- Word-Marking and Gloss Tokens -----
	gloss,  /*token pair, within square brackets and in normal font*/
	parahw, /*token pair, bold smallcaps*/
	phrase, /*token pair, bold italics*/
	qword,  /*token pair, encloses an instance of the headword within a quote*/
	wi,		/*token pair, encloses an instance of the headword used in running text*/
	// ----- Word-Marking and Gloss Tokens -----
	// ----- Cross-Reference Grouping Tokens -----
	dx,		/*token pair, encloses introductory text and one or more {dxt} cross-reference tokens*/
	dx_def, /*token pair, encloses introductory text and one or more {dxt} cross-reference tokens*/
	dx_ety, /*token pair, encloses introductory text and one or more {dxt} cross-reference tokens*/
	ma,		/*token pair, encloses introductory text and one or more {mat} tokens*/
	sc__,
	sup__,
	gloss__,
	parahw__,
	phrase__,
	qword__,
	wi__,
	dx__,
	dx_def__,
	dx_ety__,
	ma__,
	// ----- Cross-Reference Grouping Tokens -----
	// ----- Cross-Reference Tokens -----
	a_link,  /*auto link, 2 fields*/
	d_link,  /*direct link, 3 fields*/
	dxt,	 /*directional cross-reference target, 4 fields*/
	et_link, /*etymology link, 3 fields*/
	i_link,  /*italicized link, 3 fields*/
	mat,	 /*more at target, 3 fields*/
	sx,		 /*synonymous cross-reference, 4 fields*/
	// ----- Cross-Reference Tokens -----
	// ----- Date Sense Token -----
	ds, /*5 fields*/

	// ----- Date Sense Token -----
	// ===== Merriam-Webster's Collegiate Dictionary =====

	// ===== (Merriam-Webster) OTHER DICTIONARIES AND THESAURUSES =====
	// ----- Formatting Tokens -----
	bit,  /*token pair, bold-italics*/
	itsc, /*token pair, italic small capitals*/
	rom,  /*token pair, normal font*/
	bit__,
	itsc__,
	rom__,
	// ----- Formatting Tokens -----
	// ===== (Merriam-Webster) OTHER DICTIONARIES AND THESAURUSES =====
	endofenum,

	error_token,

	pair00 = b,
	pair01 = b__,
	pair10 = inf,
	pair11 = inf__,
	pair20 = sc,
	pair21 = sc__,
	pair30 = bit,
	pair31 = bit__,
	pair00__ = b__,
	pair01__ = bc,
	pair10__ = inf__,
	pair11__ = ldquo,
	pair20__ = sc__,
	pair21__ = a_link,
	pair30__ = bit__,
	pair31__ = endofenum,
};

enum class webster_token_meta
{
	open,
	close,
	single
};

// compare string and return token
/// @note input string should not contain the '{' and '}'. e.g. for token "{\/abc}" @param
/// start points to '\' and @param len is 5.
webster_tokens token_type(const char* start, size_t len);
bool token_type_match(webster_tokens open_token, webster_tokens close_token);
webster_token_meta get_token_meta_type(webster_tokens t);
namespace pt = boost::property_tree;

// todo
#ifndef WEBSTER_OPEN
#define WEBSTER_OPEN "<div>"
#endif
#ifndef WEBSTER_CLOSE
#define WEBSTER_CLOSE "</div>"
#endif
// todo
#ifndef WEBSTER_ENTRY_OPEN
#define WEBSTER_ENTRY_OPEN R"xx(<div style="margin-top:7px;">)xx"
#endif
#ifndef WEBSTER_IDATT_SEPARATOR
#define WEBSTER_IDATT_SEPARATOR '.'
#endif
#ifndef WEBSTER_SUGGEST_SCHEME
#define WEBSTER_SUGGEST_SCHEME "wmsg"
#endif
#ifndef WEBSTER_ENTRY_IDATT_METAID_SCHEME
#define WEBSTER_ENTRY_IDATT_METAID_SCHEME "wmid"
//#define WEBSTER_ENTRY_IDATT_METAID_SCHEME_WCHAR u"wmid"
#endif
#ifndef WEBSTER_IDATT_ARTID_SCHEME
#define WEBSTER_IDATT_ARTID_SCHEME "waid"
#endif
#ifndef WEBSTER_IDATT_TABLEID_SCHEME
#define WEBSTER_IDATT_TABLEID_SCHEME "wtid"
#endif
#ifndef WEBSTER_ENTRY_OPEN_OPEN
#define WEBSTER_ENTRY_OPEN_OPEN R"xx(<div id=")xx" WEBSTER_ENTRY_IDATT_METAID_SCHEME
#endif
#ifndef WEBSTER_ENTRY_OPEN_CLOSE
#define WEBSTER_ENTRY_OPEN_CLOSE R"xx(">)xx"
#endif
#ifndef WEBSTER_ENTRY_CLOSE
#define WEBSTER_ENTRY_CLOSE "</div>"
#endif
#ifndef WEBSTER_S_SCHEME_QT
#define WEBSTER_S_SCHEME_QT "wmsound"
#endif
#ifndef WEBSTER_S_SCHEME
#define WEBSTER_S_SCHEME "https"
#endif
#ifndef WEBSTER_S_URL_PREFIX_QT
#define WEBSTER_S_URL_PREFIX_QT WEBSTER_S_SCHEME_QT "://media.merriam-webster.com/soundc11/"
#endif
#ifndef WEBSTER_S_URL_PREFIX
#define WEBSTER_S_URL_PREFIX WEBSTER_S_SCHEME "://media.merriam-webster.com/soundc11/"
#endif
// todo
#ifndef WEBSTER_S_LINK_TEXT
#define WEBSTER_S_LINK_TEXT EMOJI_SPEAKER
#endif
#ifndef WEBSTER_CXR_SCHEME
#define WEBSTER_CXR_SCHEME WEBSTER_ENTRY_IDATT_METAID_SCHEME
#endif
#ifndef WEBSTER_CXR_URL_PREFIX
#define WEBSTER_CXR_URL_PREFIX "#" WEBSTER_CXR_SCHEME
#endif
#ifndef WEBSTER_SAREFS_SCHEME
#define WEBSTER_SAREFS_SCHEME "wmsarefs"
#endif
#ifndef WEBSTER_SAREFS_URL_PREFIX
#define WEBSTER_SAREFS_URL_PREFIX WEBSTER_SAREFS_SCHEME ":///"
#endif
#ifndef WEBSTER_SREFS_SCHEME
#define WEBSTER_SREFS_SCHEME "wmsrefs"
#endif
#ifndef WEBSTER_SREFS_URL_PREFIX
#define WEBSTER_SREFS_URL_PREFIX WEBSTER_SREFS_SCHEME ":///"
#endif
#ifndef WEBSTER_UAREFS_SCHEME
#define WEBSTER_UAREFS_SCHEME "wmuarefs"
#endif
#ifndef WEBSTER_UAREFS_URL_PREFIX
#define WEBSTER_UAREFS_URL_PREFIX WEBSTER_UAREFS_SCHEME ":///"
#endif
#ifndef WEBSTER_UREFS_SCHEME
#define WEBSTER_UREFS_SCHEME "wmurefs"
#endif
#ifndef WEBSTER_UREFS_URL_PREFIX
#define WEBSTER_UREFS_URL_PREFIX WEBSTER_UREFS_SCHEME ":///"
#endif
#ifndef WEBSTER_CATREF_SCHEME
#define WEBSTER_CATREF_SCHEME "wmcatref"
#endif
#ifndef WEBSTER_CATREF_URL_PREFIX
#define WEBSTER_CATREF_URL_PREFIX WEBSTER_CATREF_SCHEME ":///"
#endif
#ifndef WEBSTER_TABLE_PAGE_OPEN
#define WEBSTER_TABLE_PAGE_OPEN "https://www.merriam-webster.com/table/collegiate/"
#endif
#ifndef WEBSTER_TABLE_PAGE_CLOSE
#define WEBSTER_TABLE_PAGE_CLOSE ".htm"
#endif
#ifndef WEBSTER_ART_PAGE_OPEN
#define WEBSTER_ART_PAGE_OPEN "https://www.merriam-webster.com/art/dict/"
#endif
#ifndef WEBSTER_ART_PAGE_CLOSE
#define WEBSTER_ART_PAGE_CLOSE ".htm"
#endif
#ifndef WEBSTER_ART_DIRECT_M
#define WEBSTER_ART_DIRECT_M "://www.merriam-webster.com/assets/mw/static/art/dict/"
#endif
#ifndef WEBSTER_ART_DIRECT_OPEN
#define WEBSTER_ART_DIRECT_OPEN "https" WEBSTER_ART_DIRECT_M
#endif
#ifndef WEBSTER_ART_DIRECT_CLOSE
#define WEBSTER_ART_DIRECT_CLOSE ".gif"
#endif
#ifndef WEBSTER_ART_SCHEME
#define WEBSTER_ART_SCHEME "wmart"
#endif
#ifndef WEBSTER_ART_URL_PREFIX
#define WEBSTER_ART_URL_PREFIX WEBSTER_ART_SCHEME WEBSTER_ART_DIRECT_M
#endif
#ifndef WEBSTER_ART_URL_SUFFIX
#define WEBSTER_ART_URL_SUFFIX WEBSTER_ART_DIRECT_CLOSE
#endif
#ifndef WEBSTER_ARTL_DIRECT_M
#define WEBSTER_ARTL_DIRECT_M "://www.learnersdictionary.com/art/ld/"
#endif
#ifndef WEBSTER_ARTL_DIRECT_OPEN
#define WEBSTER_ARTL_DIRECT_OPEN "http" WEBSTER_ARTL_DIRECT_M
#endif
#ifndef WEBSTER_ARTL_DIRECT_CLOSE
#define WEBSTER_ARTL_DIRECT_CLOSE ".gif"
#endif
#ifndef WEBSTER_ARTL_SCHEME
#define WEBSTER_ARTL_SCHEME "wmartl"
#endif
#ifndef WEBSTER_ARTL_URL_PREFIX
#define WEBSTER_ARTL_URL_PREFIX WEBSTER_ART_SCHEME WEBSTER_ARTL_DIRECT_M
#endif
#ifndef WEBSTER_ARTL_URL_SUFFIX
#define WEBSTER_ARTL_URL_SUFFIX WEBSTER_ARTL_DIRECT_CLOSE
#endif

/// @note for following APIs:
/// Merriam-Webster's Collegiate® Dictionary with Audio
/// Merriam-Webster's Collegiate® Thesaurus
// ????? todo Merriam-Webster's Learner's Dictionary with Audio
// todo encoding?
class webster_parser
{
private:
	deque<webster_tokens> parse_stack;
	using node_type = pt::ptree::iterator;
	struct node_range
	{
		node_type c; // current node
		node_type end;
		node_range(node_type n1, node_type n2) : c{n1}, end{n2} {}
	};
	struct doc_ctx
	{
		// === context of the whole document ===
		bool art_direct_link{true};
		bool print{}; // display medium: print or (default) electronic format
		std::map<string, string> a_link_map{};
		// list of qualified metaid with prefix '#'
		std::vector<string> qualified_metaid_v{};
		char dict{(char)webster_dictionary::Collegiate};
		// === context of the whole document ===
		// === context of entry ===
		pt::ptree* meta{};
		// === context of entry ===
		// encoded using url encoding rule
		string metaid_enc{};
		string headword{};
		void process_node_meta(pt::ptree& node);
		void do_a_link_map(pt::ptree& node);
		void do_a_link_map_meta(pt::ptree& node);
		void reset_entry_ctx() {
			meta = nullptr;
			headword = {};
			metaid_enc = {};
		}
		// clear all except the data required by the client
		void clear_all_temp() {
			reset_entry_ctx();
			a_link_map.clear();
		}
		// === node only context ===
		// node context which only effect the property tree under this node. it is set by entering specific
		// node and must be cleared when leaving this node
		// uint8_t node_ctx_count{};
		/* if it's set, PSEQ points to the node (actually the parent node). if not, PSEQ = nullptr
		 * https://www.dictionaryapi.com/products/json#sec-2.pseq */
		// pt::ptree* PSEQ{};
	};
	doc_ctx ctx_;
	string buffer_{};
	std::vector<string> img_url_v{};
	// process json top-level elements' data structure.
	// metaid, sense_number, tableid and artid must be encoded with url encoding rule
	// name of top level: <scheme><separator><dict><separator><metaid>
	// name of table: <table_scheme><separator><dict><separator><tableid>
	// name of art: <art_scheme><separator><dict><separator><artid>
	void process_entry(string& dst, string::size_type& dst_start, pt::ptree& node);
	// read meta node
	void process_entry_read_meta(pt::ptree& node);
	//	void process_node_meta(pt::ptree& node);
	void process_node_hom(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_node_hwi(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_prs(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_ahws(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_vrs(string& dst, string::size_type& dst_start, pt::ptree& node);

	// cxs link: #<scheme><separator><dict><separator><metaid>
	void process_array_cxs(string& dst, string::size_type& dst_start, pt::ptree& node);
	// macro PROCESS_NODE_GRAM in .cpp
	void process_array_ins(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_lbs(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_sls(string& dst, string::size_type& dst_start, pt::ptree& node);
	// todo
	// for sense json object
	void process_node_next2_sense(string& dst, string::size_type& dst_start, pt::ptree& node);
	// void process_node_next2_sen(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_node_next2_bs(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_next2_pseq(string& dst, string::size_type& dst_start, pt::ptree& node);
	// todo
	void process_array_sseq(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_def(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_uros(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_et(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_dros(string& dst, string::size_type& dst_start, pt::ptree& node);
	// ["vis", [{object}]]
	void process_array_next2_vis(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_node_aq(string& dst, string::size_type& dst_start, pt::ptree& node);
	// ["ri", [[array]]]
	void process_array_next2_ri(string& dst, string::size_type& dst_start, pt::ptree& node);
	// ["uns", [[array]]]
	void process_array_next2_uns(string& dst, string::size_type& dst_start, pt::ptree& node);
	// todo
	void process_array_utxt(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_dxnls(string& dst, string::size_type& dst_start, pt::ptree& node);
	// uaref link: scheme:///<dict_char>/id
	void process_array_usages(string& dst, string::size_type& dst_start, pt::ptree& node);
	// uaref link: scheme:///<dict_char>/id
	void process_array_next2_urefs(string& dst, string::size_type& dst_start, pt::ptree& node);
	// saref link: scheme:///<dict_char>/id
	void process_array_syns(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_wvbvrs(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_wvrs(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_node_wsls(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array___list(string& dst, string::size_type& dst_start, pt::ptree& node,
							  const char* node_name);
	void process_array_ant_list(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_near_list(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_phrase_list(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_rel_list(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_syn_list(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_snotebox(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_quotes(string& dst, string::size_type& dst_start, pt::ptree& node);
	// art link: scheme://xxxxx or normal page link
	void process_node_art(string& dst, string::size_type& dst_start, pt::ptree& node);
	// art link: scheme://xxxxx
	void process_node_artl(string& dst, string::size_type& dst_start, pt::ptree& node);
	// table link: normal page link
	void process_node_table(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_srefs(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_list(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_dt(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_node_next2_bnw(string& dst, string::size_type& dst_start, pt::ptree& node);
	// catref link: scheme:///<dict_char>/id
	void process_node_next2_ca(string& dst, string::size_type& dst_start, pt::ptree& node);
	// todo impl process_array_shortdef for preview
	void process_array_next2_snote(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_node_sdsense(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_node_altprs(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_node_phrasev(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_node_sphrasev(string& dst, string::size_type& dst_start, pt::ptree& node);
	void process_array_phsls(string& dst, string::size_type& dst_start, pt::ptree& node);

	// token
	void webster_token_render(webster_tokens t, const char* token_str_p, size_t token_str_len, string& dst,
							  string::size_type& dst_start);
	// render cross-reference token a_link with 2 fields
	void webster_CRF2_a_link_token_render(const char* token_str_p, size_t token_str_len, string& dst,
										  string::size_type& dst_start);
	// render cross-reference tokens with 3 fields
	void webster_CRF3_token_render(webster_tokens t, const char* token_str_p, size_t token_str_len,
								   string& dst, string::size_type& dst_start);
	// render cross-reference token sx with 4 fields
	void webster_CRF4_sx_token_render(const char* token_str_p, size_t token_str_len, string& dst,
									  string::size_type& dst_start);
	// render cross-reference token dxt with 4 fields
	void webster_CRF4_dxt_token_render(const char* token_str_p, size_t token_str_len, string& dst,
									   string::size_type& dst_start);

public:
	// todo src_ptr may contain utf-8 literals
	/// string to property tree
	/// @warning @param dst must pre-allocate enough space
	void parse_JSON(string_view src_view, string& dst, string::size_type& dst_start);
	void parse_JSON(std::istream& src_strm, string& dst, string::size_type& dst_start);

	/// RUNNING TEXT with tokens to text with a subset of HTML 4 markup
	/// more at @link https://doc.qt.io/qt-5/richtext-html-subset.html property tree to html text
	bool parse_RUNNING_TEXT(string& src_dst);
	// set dictionary as Collegiate (default)
	void use_dict(webster_dictionary d) { ctx_.dict = (char)d; }
	static string::size_type buffer_s_required(string::size_type json_size) {
		return (json_size << 2) + json_size + 200;
	}
	// clear temporary data during parsing (not including img urls)
	void clear_temp() {
		buffer_ = {};
		ctx_.clear_all_temp();
	}
	// used by client
	const auto& get_img_urls() const { return img_url_v; }
	const auto& get_qualified_metaids() const { return ctx_.qualified_metaid_v; }
};

} // namespace dict_core

#endif // ONLINE_RECORD_PARSER_H