//
//  dict_buddy_core_global.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/2.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef DICT_BUDDY_CORE_GLOBAL_H
#define DICT_BUDDY_CORE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(DICT_BUDDY_CORE_LIBRARY)
#  define DICT_BUDDY_CORESHARED_EXPORT Q_DECL_EXPORT
#else
#  define DICT_BUDDY_CORESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // DICT_BUDDY_CORE_GLOBAL_H
