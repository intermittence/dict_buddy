//
//  types_literal.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/8/1.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef TYPES_LITERAL_H
#define TYPES_LITERAL_H
// since clang format can't get emoji right
#ifndef EMOJI_EARTH
#define EMOJI_EARTH u8"🌐"
#endif
#ifndef EMOJI_BOOK
#define EMOJI_BOOK u8"📖"
#endif
#ifndef EMOJI_SPEAKER
#define EMOJI_SPEAKER u8"🔈"
#endif
namespace dict_core
{
enum custom_resource
{
	res_audio = 100
};
} // namespace dict_core
#endif // TYPES_LITERAL_H
