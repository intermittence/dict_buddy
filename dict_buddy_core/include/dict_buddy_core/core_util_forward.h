//
//  core_util_forward.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/8/7.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef CORE_UTIL_FORWARD_H
#define CORE_UTIL_FORWARD_H
namespace dict_core
{

enum class extension
{
	jpeg,
	png,
	gif,
	wav,
	mp3,
	empty
};
} // namespace dict_core
#endif // CORE_UTIL_FORWARD_H
