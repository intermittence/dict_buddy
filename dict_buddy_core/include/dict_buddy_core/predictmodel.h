//
//  predictmodel.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/2.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef PREDICTMODEL_H
#define PREDICTMODEL_H

#include <QAbstractListModel>
#include <vector>
#include <memory>
#include <lexicon_buddy/lexicon/buddy_forward.h>
namespace dict_core
{
using std::vector;
using std::unique_ptr;
using lexicon::buddy::query_cluster;
struct PredictItem
{
	QString word_;
	PredictItem(const PredictItem&) = default;
	PredictItem& operator=(const PredictItem&) = default;
	PredictItem(PredictItem&&) = default;
	PredictItem& operator=(PredictItem&&) = default;
	PredictItem(const QString& word) : word_{word} {}
	PredictItem(QString&& word) : word_{std::move(word)} {}
};

class PredictModel : public QAbstractListModel
{
	Q_OBJECT

public:
	explicit PredictModel(QObject* parent = nullptr);

	int rowCount(const QModelIndex& parent = QModelIndex()) const override;

	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

	bool setData(const QModelIndex& index, const QVariant& value, int role) override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;

    /// @brief this method copy contents from @param prediction
    void regenerate(const query_cluster::predict_type& prediction);

	const vector<unique_ptr<PredictItem>>* native_handle() { return &items_; }

private:
	vector<unique_ptr<PredictItem>> items_{};
    // uint32_t a{};
};
} // namespace dict_core
#endif // PREDICTMODEL_H
