//
//  img_util.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/8/2.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef IMG_UTIL_H
#define IMG_UTIL_H
#include <string>
#include <deque>
namespace dict_core
{
using std::string;
using std::deque;

template <std::size_t pits_size, std::size_t waiting_limit, class guy_type, bool reset_empty_pit = true>
struct public_toilet
{
	static constexpr std::size_t end_ = std::numeric_limits<std::size_t>::max();
	guy_type pits[pits_size]{};
	deque<std::size_t> pits_available{};
	deque<guy_type> people_waiting{};

	public_toilet() {
		std::size_t tmp = 0;
		do {
			pits_available.emplace_back(tmp);
		} while (++tmp != pits_size);
	}
	// return true if someone take the pit
	bool leave_pit(std::size_t p) {
		if (people_waiting.empty()) {
			if constexpr (reset_empty_pit) pits[p] = {};
			pits_available.emplace_back(p);
			return false;
		} else {
			pits[p] = people_waiting.front();
			people_waiting.pop_front();
			return true;
		}
	}
	// return position if this guy immediately take a pit and return end public_toilet::end_ if waiting
	std::size_t new_guy(guy_type t) {
		if (pits_available.empty()) {
			if (people_waiting.size() < waiting_limit) people_waiting.emplace_back(t);
			return end_;
		} else {
			auto f = pits_available.front();
			pits[f] = t;
			// pop after assignment since assignment may throw
			pits_available.pop_front();
			return f;
		}
	}
};

using img_coordinator = public_toilet<10, 20, string, false>;

} // namespace dict_core
#endif // IMG_UTIL_H
