//
//  online_record_parser_forward.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/8/9.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef ONLINE_RECORD_PARSER_FORWARD_H
#define ONLINE_RECORD_PARSER_FORWARD_H
namespace dict_core
{
enum class webster_dictionary
{
	Collegiate = 'c',
	Collegiate_Thesaurus = 't',
	Learner_Dictionary = 'l', // todo
};

inline const char* webster_dict_to_literal(webster_dictionary w) {
	switch (w) {
		case webster_dictionary::Collegiate:
			return "Merriam-Webster's Collegiate® Dictionary with Audio";
		case webster_dictionary::Collegiate_Thesaurus:
			return "Merriam-Webster's Collegiate® Thesaurus";
		case webster_dictionary::Learner_Dictionary:
			return "Merriam-Webster's Learner's Dictionary with Audio";
	}
}
} // namespace dict_core
#endif // ONLINE_RECORD_PARSER_FORWARD_H
