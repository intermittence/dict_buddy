//
//  dict_buddy_core.h
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/2.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef DICT_BUDDY_CORE_H
#define DICT_BUDDY_CORE_H

#include "dict_buddy_core_global.h"
#include <lexicon_buddy/lexicon/buddy.h>
namespace dict_core
{
using core_path_type = const char*;
}

#endif // DICT_BUDDY_CORE_H
