//
//  predictmodel.cpp
//  dict_buddy
//
//  Created by Lohengrin on 2019/3/2.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include <dict_buddy_core/predictmodel.h>
#include <iostream>
namespace dict_core
{
PredictModel::PredictModel(QObject* parent) : QAbstractListModel(parent) {}

int PredictModel::rowCount(const QModelIndex& parent) const {
	if (parent.isValid()) return 0;
	return items_.size();
}

QVariant PredictModel::data(const QModelIndex& index, int role) const {
	if (role != Qt::DisplayRole || !index.isValid()) return QVariant();
	return QVariant(items_[index.row()]->word_);
}

bool PredictModel::setData(const QModelIndex& index, const QVariant& value, int role) {
	items_[index.row()].reset(new PredictItem(value.toString()));
	emit dataChanged(index, index, QVector<int>() << role);
	return true;
}

Qt::ItemFlags PredictModel::flags(const QModelIndex& index) const {
	if (!index.isValid()) return Qt::NoItemFlags;
	return Qt::ItemIsSelectable;
}
void PredictModel::regenerate(const query_cluster::predict_type& prediction) {
	beginResetModel();
	items_.clear();
    for (auto& i : prediction) {
        items_.emplace_back(std::make_unique<PredictItem>(QString::fromStdString(i.first)));
    }
	endResetModel();
}
} // namespace dict_core
