//
//  core_util.cpp
//  dict_buddy
//
//  Created by Lohengrin on 2019/8/7.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include <dict_buddy_core/core_util.h>

namespace dict_core
{

extension get_extension_from_literal(string_view ext_view) {
	switch (ext_view.size()) {
		case 4:
			if (ext_view == ".png") return extension::png;
			if (ext_view == ".jpg") return extension::jpeg;
			if (ext_view == ".gif") return extension::gif;
			if (ext_view == ".wav") return extension::wav;
			if (ext_view == ".mp3") return extension::mp3;
			return extension::empty;
		case 5:
			if (ext_view == ".jpeg") return extension::jpeg;
			return extension::empty;
		default:
			return extension::empty;
	}
}

const char* get_literal_from_extension(extension ext) {
	switch (ext) {
		case extension::png:
			return "png";
		case extension::jpeg:
			return "jpeg";
		case extension::gif:
			return "gif";
		case extension::wav:
			return "wav";
		case extension::mp3:
			return "mp3";
		default:
			return nullptr;
	}
}

bool is_MIME_valid(extension ext, string_view MIME_view) {
	switch (ext) {
		case extension::wav:
			return MIME_view.find("audio/wav") != MIME_view.npos or
				   MIME_view.find("audio/x-wav") != MIME_view.npos or
				   MIME_view.find("audio/vnd.wave") != MIME_view.npos or
				   MIME_view.find("audio/wave") != MIME_view.npos or
				   MIME_view.find("audio/x-pn-wav") != MIME_view.npos;
		case extension::mp3:
			return MIME_view.find("audio/mpeg") != MIME_view.npos or
				   MIME_view.find("audio/mp3") != MIME_view.npos;
		case extension::gif:
			return MIME_view.find("image/gif") != MIME_view.npos;
		case extension::png:
			return MIME_view.find("image/png") != MIME_view.npos;
		case extension::jpeg:
			return MIME_view.find("image/jpeg") != MIME_view.npos;
		default:
			return false;
	}
}

bool ascii_only(std::string_view str) {
	for (auto&& i : str) {
		if ((unsigned char)i > 127) return false;
	}
	return true;
}

char rfc3986[256] = {0};
char html5[256] = {0};

void url_encoder_rfc_tables_init() {
	int i;
	for (i = 0; i < 256; i++) {
		rfc3986[i] = isalnum(i) || i == '~' || i == '-' || i == '.' || i == '_' ? i : 0;
		html5[i] = isalnum(i) || i == '*' || i == '-' || i == '.' || i == '_' ? i : (i == ' ') ? '+' : 0;
	}
}

///@note @param enc (buffer) should be large enough (e.g. 2048);
char* url_encode(const unsigned char* s, std::size_t len, char* enc, char* table) {
	auto s_end = s + len;
	for (; s != s_end; s++) {
		if (table[*s])
			sprintf(enc, "%c", table[*s]);
		else
			sprintf(enc, "%%%02X", *s);
		while (*++enc)
			;
	}
	return (enc);
}

string_view url_encode_view(const unsigned char* s, std::size_t len, char* enc, char* table) {
	return {enc, static_cast<string_view::size_type>(url_encode(s, len, enc, table) - enc)};
}

string url_encode_str(const unsigned char* s, std::size_t len, char* table) {
	char buf[2048]{};
	url_encode(s, len, buf, table);
	return {buf};
}

encoder_init init__{};
} // namespace dict_core