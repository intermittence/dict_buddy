//
//  online_record_parser.cpp
//  dict_buddy
//
//  Created by Lohengrin on 2019/5/16.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include <dict_buddy_core/online_record_parser.h>
#include <dict_buddy_core/core_util.h>
#include <QString>
#include <boost/iostreams/stream.hpp>
namespace dict_core
{

#ifndef liter_2_struct
#define liter_2_struct(s)                                                                                    \
	if (str == #s) return webster_data_structure::s
#endif

#ifndef format_case_str
#define format_case_str(token, l)                                                                            \
	case webster_tokens::token:                                                                              \
		dst.replace(dst_start, sizeof(l) - 1, (l));                                                          \
		dst_start += sizeof(l) - 1;                                                                          \
		break
#endif

#ifndef format_2case_str
#define format_2case_str(token0, token1, l)                                                                  \
	case webster_tokens::token0:                                                                             \
	case webster_tokens::token1:                                                                             \
		dst.replace(dst_start, sizeof(l) - 1, (l));                                                          \
		dst_start += sizeof(l) - 1;                                                                          \
		break
#endif

#ifndef format_3case_str
#define format_3case_str(token0, token1, token2, l)                                                          \
	case webster_tokens::token0:                                                                             \
	case webster_tokens::token1:                                                                             \
	case webster_tokens::token2:                                                                             \
		dst.replace(dst_start, sizeof(l) - 1, (l));                                                          \
		dst_start += sizeof(l) - 1;                                                                          \
		break
#endif

#ifndef str_literal_replace
#define str_literal_replace(str, p, literal)                                                                 \
	str.replace((p), sizeof(literal) - 1, (literal));                                                        \
	(p) += sizeof(literal) - 1
#endif
#ifndef str_str_replace
#define str_str_replace(str, p, str0)                                                                        \
	str.replace((p), (str0).size(), (str0));                                                                 \
	(p) += (str0).size()
#endif

#ifndef dst_l_replace
#define dst_l_replace(literal) str_literal_replace(dst, dst_start, (literal))
#endif
#ifndef dst_pn_replace
#define dst_pn_replace(p, c)                                                                                 \
	dst.replace((dst_start), (c), (p), (c));                                                                 \
	dst_start += (c)
#endif
#ifndef dst_s_replace
#define dst_s_replace(str0) str_str_replace(dst, dst_start, (str0))
#endif
#ifndef PROCESS_NODE_
#define PROCESS_NODE_(dst, dst_start, node, li_open, li_close)                                               \
	str_literal_replace((dst), (dst_start), (li_open));                                                      \
	str_str_replace((dst), (dst_start), (node.data()));                                                      \
	str_literal_replace((dst), (dst_start), (li_close))
#endif
#ifndef PROCESS_NODE_GRAM
#define PROCESS_NODE_GRAM(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "[<i>", "</i>]")
#endif
#ifndef PROCESS_NODE_HW
#define PROCESS_NODE_HW(dst, dst_start, node)                                                                \
	PROCESS_NODE_(dst, dst_start, node, R"xx(<span style="font-size:130%;">)xx", "</span>");                 \
	ctx_.headword = node.data()
#endif
#ifndef PROCESS_NODE_PSL
#define PROCESS_NODE_PSL(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "(<i>", "</i>)")
#endif
#ifndef PROCESS_NODE_SPL
#define PROCESS_NODE_SPL(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "<i>", "</i>")
#endif
#ifndef PROCESS_NODE_FL
#define PROCESS_NODE_FL(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "<i>", "</i>")
#endif
#ifndef PROCESS_NODE_VA
#define PROCESS_NODE_VA(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "<b>", "</b>")
#endif
#ifndef PROCESS_NODE_VD
#define PROCESS_NODE_VD(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "<i>", "</i>")
#endif
#ifndef PROCESS_NODE_VL
#define PROCESS_NODE_VL(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "<i>", "</i>")
#endif
#ifndef PROCESS_NODE_CXL
#define PROCESS_NODE_CXL(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "<i>", "</i>")
#endif
#ifndef PROCESS_NODE_SN
#define PROCESS_NODE_SN(dst, dst_start, node)                                                                \
	if ('2' <= node.data()[0] and node.data()[0] <= '9') dst_l_replace("<br>");                              \
	PROCESS_NODE_(dst, dst_start, node, "<b>", "</b>")
#endif
#ifndef PROCESS_NODE_SD
#define PROCESS_NODE_SD(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "<b>", "</b>")
#endif
#ifndef PROCESS_NODE_WD
#define PROCESS_NODE_WD(dst, dst_start, node) str_str_replace(dst, dst_start, node.data())
#endif
#ifndef PROCESS_NODE_WVBVA
#define PROCESS_NODE_WVBVA(dst, dst_start, node) PROCESS_NODE_WD(dst, dst_start, node)
#endif
#ifndef PROCESS_NODE_WVA
#define PROCESS_NODE_WVA(dst, dst_start, node) PROCESS_NODE_WD(dst, dst_start, node)
#endif
#ifndef PROCESS_NODE_WVBVL
#define PROCESS_NODE_WVBVL(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, " <i>", "</i> ")
#endif
#ifndef PROCESS_NODE_SGRAM
#define PROCESS_NODE_SGRAM(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "; <i>", "</i> ")
#endif
#ifndef PROCESS_NODE_BNOTE
#define PROCESS_NODE_BNOTE(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "<b><i>", "</i></b> ")
#endif
#ifndef PROCESS_NODE_DRP
#define PROCESS_NODE_DRP(dst, dst_start, node)                                                               \
	str_literal_replace(dst, dst_start, "\u2014");                                                           \
	PROCESS_NODE_(dst, dst_start, node, "<b>", "</b>")
#endif
// todo heading x??
#ifndef HEAD_OPEN_TAG
#define HEAD_OPEN_TAG R"xx(<h4 style="margin-top:3px;margin-bottom:3px;padding:0;">)xx"
#endif
#ifndef HEAD_CLOSE_TAG
#define HEAD_CLOSE_TAG "</h4>"
#endif

#ifndef PROCESS_NODE_PL
#define PROCESS_NODE_PL(dst, dst_start, node)                                                                \
	str_literal_replace(dst, dst_start, HEAD_OPEN_TAG);                                                      \
	str_str_replace(dst, dst_start, node.data());                                                            \
	str_literal_replace(dst, dst_start, HEAD_CLOSE_TAG);
#endif
#ifndef PROCESS_NODE_DATE
#define PROCESS_NODE_DATE(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "(", ")")
#endif
#ifndef PROCESS_NODE_WOD
#define PROCESS_NODE_WOD(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "(", ")")
#endif
#ifndef PROCESS_NODE_IPA
#define PROCESS_NODE_IPA(dst, dst_start, node) str_str_replace(dst, dst_start, node.data())
#endif
#ifndef PROCESS_NODE_NEXT2_G
#define PROCESS_NODE_NEXT2_G(dst, dst_start, node)                                                           \
	PROCESS_NODE_(dst, dst_start, node, R"xx(<span style="font-variant: small-caps;">)xx", "</span>")
#endif
#ifndef PROCESS_NODE_NEXT2_WSGRAM
#define PROCESS_NODE_NEXT2_WSGRAM(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "[<i>", "</i>]")
#endif
#ifndef PROCESS_NODE_PVA
#define PROCESS_NODE_PVA(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "<b>", "</b> ")
#endif
#ifndef PROCESS_NODE_PVL
#define PROCESS_NODE_PVL(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "<i>", "</i> ")
#endif
#ifndef PROCESS_NODE_RSL
#define PROCESS_NODE_RSL(dst, dst_start, node) PROCESS_NODE_(dst, dst_start, node, "(<i>", "</i>)")
#endif

#ifndef A_LINK_SCHEME
#define A_LINK_SCHEME "wmalink"
#endif
#ifndef D_LINK_SCHEME
#define D_LINK_SCHEME "wmdlink"
#endif
#ifndef DXT_SCHEME
#define DXT_SCHEME "wmdxt"
#endif
#ifndef ET_LINK_SCHEME
#define ET_LINK_SCHEME "wmetlink"
#endif
#ifndef I_LINK_SCHEME
#define I_LINK_SCHEME "wmilink"
#endif
#ifndef MAT_SCHEME
#define MAT_SCHEME "wmmat"
#endif
#ifndef SX_SCHEME
#define SX_SCHEME "wmsx"
#endif
// todo choose enum type based on the most distinct position
webster_data_structure data_structure_type(const string& str) {
	switch (str.size()) {
		case 1:
			liter_2_struct(l);
			liter_2_struct(t);
			liter_2_struct(g);
			break;
		case 2:
			liter_2_struct(id);
			liter_2_struct(hw);
			liter_2_struct(va);
			liter_2_struct(vl);
			liter_2_struct(mw);
			liter_2_struct(l2);
			liter_2_struct(fl);
			if (str == "if") return webster_data_structure::if_;
			liter_2_struct(il);
			liter_2_struct(vd);
			liter_2_struct(sn);
			liter_2_struct(dt);
			liter_2_struct(sd);
			liter_2_struct(bs);
			liter_2_struct(aq);
			liter_2_struct(ri);
			liter_2_struct(ca);
			liter_2_struct(pn);
			liter_2_struct(pl);
			liter_2_struct(pt);
			liter_2_struct(et);
			liter_2_struct(wd);
			liter_2_struct(pr);
			break;
		case 3:
			liter_2_struct(src);
			liter_2_struct(hom);
			liter_2_struct(hwi);
			liter_2_struct(vrs);
			liter_2_struct(prs);
			liter_2_struct(pun);
			liter_2_struct(lbs);
			liter_2_struct(sls);
			liter_2_struct(psl);
			liter_2_struct(spl);
			liter_2_struct(ins);
			liter_2_struct(ifc);
			liter_2_struct(cxs);
			liter_2_struct(cxl);
			liter_2_struct(cxr);
			liter_2_struct(cxt);
			liter_2_struct(cxn);
			liter_2_struct(def);
			liter_2_struct(sen);
			liter_2_struct(vis);
			liter_2_struct(riw);
			liter_2_struct(rie);
			liter_2_struct(bnw);
			liter_2_struct(cat);
			liter_2_struct(uns);
			liter_2_struct(ure);
			liter_2_struct(drp);
			liter_2_struct(art);
			liter_2_struct(ipa);
			liter_2_struct(wod);
			liter_2_struct(wsl);
			liter_2_struct(wvl);
			liter_2_struct(wva);
			liter_2_struct(phr);
			liter_2_struct(pva);
			liter_2_struct(pvl);
			liter_2_struct(rsl);
			liter_2_struct(dim);
			break;
		case 4:
			liter_2_struct(meta);
			liter_2_struct(uuid);
			liter_2_struct(sort);
			liter_2_struct(ahws);
			liter_2_struct(sseq);
			liter_2_struct(pseq);
			liter_2_struct(auth);
			liter_2_struct(text);
			liter_2_struct(cats);
			liter_2_struct(uros);
			liter_2_struct(utxt);
			liter_2_struct(dros);
			liter_2_struct(syns);
			liter_2_struct(capt);
			liter_2_struct(date);
			liter_2_struct(ants);
			liter_2_struct(tsrc);
			liter_2_struct(wsls);
			liter_2_struct(wvrs);
			liter_2_struct(list);
			liter_2_struct(gram);
			liter_2_struct(phrs);
			liter_2_struct(artl);
			break;
		case 5:
			liter_2_struct(stems);
			liter_2_struct(sound);
			liter_2_struct(sgram);
			liter_2_struct(cxtis);
			liter_2_struct(sense);
			liter_2_struct(pname);
			liter_2_struct(sname);
			liter_2_struct(intro);
			liter_2_struct(snote);
			liter_2_struct(dxnls);
			liter_2_struct(uaref);
			liter_2_struct(artid);
			liter_2_struct(table);
			liter_2_struct(tuuid);
			liter_2_struct(wvbvl);
			liter_2_struct(wvbva);
			liter_2_struct(srefs);
			liter_2_struct(urefs);
			liter_2_struct(bnote);
			liter_2_struct(phsls);
			break;
		case 6:
			liter_2_struct(source);
			liter_2_struct(aqdate);
			liter_2_struct(catref);
			liter_2_struct(usages);
			liter_2_struct(uarefs);
			liter_2_struct(sarefs);
			liter_2_struct(quotes);
			liter_2_struct(target);
			liter_2_struct(wvbvrs);
			liter_2_struct(altprs);
			liter_2_struct(wsgram);
			break;
		case 7:
			liter_2_struct(section);
			liter_2_struct(sdsense);
			liter_2_struct(altname);
			liter_2_struct(tableid);
			liter_2_struct(phrasev);
			break;
		case 8:
			liter_2_struct(et_snote);
			liter_2_struct(shortdef);
			liter_2_struct(syn_list);
			liter_2_struct(rel_list);
			liter_2_struct(ant_list);
			liter_2_struct(snotebox);
			liter_2_struct(sphrasev);
			break;
		case 9:
			liter_2_struct(offensive);
			liter_2_struct(subsource);
			liter_2_struct(near_list);
			liter_2_struct(highlight);
			break;
		case 10:
			break;
		case 11:
			liter_2_struct(displayname);
			liter_2_struct(phrase_list);
			break;
		case 12:
			liter_2_struct(app_shortdef);
			break;
		default:
			break;
	}
	return webster_data_structure::error_data_structure;
}

webster_token_meta get_token_meta_type(webster_tokens t) {
	if ((webster_tokens::pair00 <= t && t < webster_tokens::pair01) ||
		(webster_tokens::pair10 <= t && t < webster_tokens::pair11) ||
		(webster_tokens::pair20 <= t && t < webster_tokens::pair21) ||
		(webster_tokens::pair30 <= t && t < webster_tokens::pair31)) {
		return webster_token_meta::open;
	} else if ((webster_tokens::pair00__ <= t && t < webster_tokens::pair01__) ||
			   (webster_tokens::pair10__ <= t && t < webster_tokens::pair11__) ||
			   (webster_tokens::pair20__ <= t && t < webster_tokens::pair21__) ||
			   (webster_tokens::pair30__ <= t && t < webster_tokens::pair31__)) {
		return webster_token_meta::close;
	} else {
		return webster_token_meta::single;
	}
}
// todo choose enum type based on the most distinct position
webster_tokens token_type(const char* start, size_t len) {
	auto p = start;
	auto end = start + len;
	for (;;) {
		if (p == end || *p == '|') break;
		++p;
	}
	len = static_cast<string::size_type>(p - start);
	if (len == 0) return webster_tokens::error_token;
	string_view s{start, len};
	switch (len) {
		case 1:
			if (s == "b") return webster_tokens::b;
			break;
		case 2:
			if (s == "bc") return webster_tokens::bc;
			if (s == "it") return webster_tokens::it;
			if (s == "sc") return webster_tokens::sc;
			if (s == "wi") return webster_tokens::wi;
			if (s == "dx") return webster_tokens::dx;
			if (s == "ma") return webster_tokens::ma;
			if (s == "sx") return webster_tokens::sx;
			if (s == "dx") return webster_tokens::dx;
			if (s == "/b") return webster_tokens::b__;
			break;
		case 3:
			if (s == "inf") return webster_tokens::inf;
			if (s == "sup") return webster_tokens::sup;
			if (s == "dxt") return webster_tokens::dxt;
			if (s == "mat") return webster_tokens::mat;
			if (s == "bit") return webster_tokens::bit;
			if (s == "rom") return webster_tokens::rom;
			if (s == "/it") return webster_tokens::it__;
			if (s == "/sc") return webster_tokens::sc__;
			if (s == "/wi") return webster_tokens::wi__;
			if (s == "/dx") return webster_tokens::dx__;
			if (s == "/ma") return webster_tokens::ma__;
			break;
		case 4:
			if (s == "itsc") return webster_tokens::itsc;
			if (s == "/inf") return webster_tokens::inf__;
			if (s == "/sup") return webster_tokens::sup__;
			if (s == "/bit") return webster_tokens::bit__;
			if (s == "/rom") return webster_tokens::rom__;
			break;
		case 5:
			if (s == "ldquo") return webster_tokens::ldquo;
			if (s == "rdquo") return webster_tokens::rdquo;
			if (s == "gloss") return webster_tokens::gloss;
			if (s == "qword") return webster_tokens::qword;
			if (s == "/itsc") return webster_tokens::itsc__;
			break;
		case 6:
			if (s == "parahw") return webster_tokens::parahw;
			if (s == "phrase") return webster_tokens::phrase;
			if (s == "dx_def") return webster_tokens::dx_def;
			if (s == "dx_ety") return webster_tokens::dx_ety;
			if (s == "a_link") return webster_tokens::a_link;
			if (s == "d_link") return webster_tokens::d_link;
			if (s == "i_link") return webster_tokens::i_link;
			if (s == "/qword") return webster_tokens::qword__;
			if (s == "/gloss") return webster_tokens::gloss__;
			break;
		case 7:
			if (s == "et_link") return webster_tokens::et_link;
			if (s == "/parahw") return webster_tokens::parahw__;
			if (s == "/phrase") return webster_tokens::phrase__;
			if (s == "/dx_def") return webster_tokens::dx_def__;
			if (s == "/dx_ety") return webster_tokens::dx_ety__;
			break;
		case 8:
		default:
			break;
	}
	return webster_tokens::error_token;
}

bool token_type_match(webster_tokens open_token, webster_tokens close_token) {
	switch (close_token) {
		case webster_tokens::b__:
			return open_token == webster_tokens::b;
		case webster_tokens::it__:
			return open_token == webster_tokens::it;
		case webster_tokens::sc__:
			return open_token == webster_tokens::sc;
		case webster_tokens::wi__:
			return open_token == webster_tokens::wi;
		case webster_tokens::dx__:
			return open_token == webster_tokens::dx;
		case webster_tokens::ma__:
			return open_token == webster_tokens::ma;
		case webster_tokens::inf__:
			return open_token == webster_tokens::inf;
		case webster_tokens::sup__:
			return open_token == webster_tokens::sup;
		case webster_tokens::bit__:
			return open_token == webster_tokens::bit;
		case webster_tokens::rom__:
			return open_token == webster_tokens::rom;
		case webster_tokens::itsc__:
			return open_token == webster_tokens::itsc;
		case webster_tokens::qword__:
			return open_token == webster_tokens::qword;
		case webster_tokens::gloss__:
			return open_token == webster_tokens::gloss;
		case webster_tokens::phrase__:
			return open_token == webster_tokens::phrase;
		case webster_tokens::parahw__:
			return open_token == webster_tokens::parahw;
		case webster_tokens::dx_def__:
			return open_token == webster_tokens::dx_def;
		case webster_tokens::dx_ety__:
			return open_token == webster_tokens::dx_ety;
		default:
			return false;
	}
}

void webster_parser::webster_token_render(webster_tokens t, const char* token_str_p, size_t token_str_len,
										  string& dst, string::size_type& dst_start) {
	switch (t) {
		format_case_str(b, "<b>");
		format_case_str(b__, "</b>");
		format_case_str(bc, "<b>:</b> ");
		format_case_str(inf, "<sub>");
		format_case_str(it, "<i>");
		format_case_str(inf__, "</sub>");
		format_case_str(it__, "</i>");
		// todo escape '\'? size of?
		format_case_str(ldquo, "\\u201c");
		// todo escape '\'? size of?
		format_case_str(rdquo, "\\u201d");
		format_case_str(sc, R"xx(<span style="font-variant: small-caps;">)xx");
		format_case_str(sup, "<sup>");
		format_case_str(gloss, "[");
		format_case_str(parahw, R"xx(<span style="font-variant: small-caps;font-weight: bold;">)xx");
		format_case_str(phrase, "<b><i>");
		format_2case_str(qword, wi, "<i>");
		format_2case_str(dx, dx_ety, "—");
		format_case_str(ma, "—more at ");
		format_case_str(dx_def, "(");
		format_2case_str(sc__, parahw__, "</span>");
		format_case_str(sup__, "</sup>");
		format_case_str(gloss__, "]");
		format_case_str(phrase__, "</i></b>");
		format_2case_str(qword__, wi__, "</i>");
		case webster_tokens::dx__:
		case webster_tokens::dx_ety__:
		case webster_tokens::ma__:
			break;
			format_case_str(dx_def__, ")");
		case webster_tokens::a_link:
			webster_CRF2_a_link_token_render(token_str_p, token_str_len, dst, dst_start);
			break;
		case webster_tokens::d_link:
		case webster_tokens::et_link:
		case webster_tokens::i_link:
		case webster_tokens::mat:
			webster_CRF3_token_render(t, token_str_p, token_str_len, dst, dst_start);
			break;
		case webster_tokens::dxt:
			webster_CRF4_dxt_token_render(token_str_p, token_str_len, dst, dst_start);
			break;
		case webster_tokens::sx:
			webster_CRF4_sx_token_render(token_str_p, token_str_len, dst, dst_start);
			break;
		case webster_tokens::ds:
			dst[dst_start] = '{';
			++dst_start;
			dst.replace(dst_start, token_str_len, token_str_p);
			dst_start += token_str_len;
			dst[dst_start] = '}';
			++dst_start;
			break;
			format_case_str(bit, R"xx(<span style="font-style: italic;font-weight: bold;">)xx");
			format_case_str(itsc, "<span style=\"font-style: italic;font-variant: small-caps;\">");
			format_case_str(
				rom, R"xx(<span style="font-style: normal;font-weight: normal;font-variant: normal;">)xx");
			format_case_str(bit__, "</span>");
			format_case_str(itsc__, "/span");
			format_case_str(rom__, "/span");
		default:
			break;
	}
}

void webster_parser::webster_CRF2_a_link_token_render(const char* token_str_p, size_t token_str_len,
													  string& dst, string::size_type& dst_start) {
	dst_l_replace(R"xx(<a href=")xx");
	// token field 2
	auto* ptr_f2 = token_str_p + sizeof "a_link";
	string::size_type n = token_str_len - sizeof "a_link";
	auto iter = ctx_.a_link_map.find({ptr_f2, n});
	// === link or not ===
	if (iter != ctx_.a_link_map.end()) {
		dst[dst_start++] = '#';
		dst_l_replace(WEBSTER_ENTRY_IDATT_METAID_SCHEME);
		dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
		dst[dst_start++] = ctx_.dict;
		dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
		dst_s_replace(iter->second);
	} else {
	}
	// === link or not ===
	dst_l_replace(R"xx(">)xx");
	// === link text ===
	dst_pn_replace(ptr_f2, n);
	// === link text ===
	dst_l_replace("</a>");
}

void webster_parser::webster_CRF3_token_render(webster_tokens t, const char* token_str_p,
											   size_t token_str_len, string& dst,
											   string::size_type& dst_start) {
	switch (t) {
		case webster_tokens::d_link:
			dst_l_replace(R"xx(<a href="#)xx");
			break;
		case webster_tokens::i_link:
			dst_l_replace(R"xx(<a style="font-style: italic;" href="#)xx");
			break;
		case webster_tokens::et_link:
		case webster_tokens::mat:
			dst_l_replace(R"xx(<a style="font-variant: small-caps;" href="#)xx");
			break;
		default:
			throw webster_token_exception{"incorrect token type"};
	}
	// === link ===
	dst_l_replace(WEBSTER_ENTRY_IDATT_METAID_SCHEME);
	dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
	dst[dst_start++] = ctx_.dict;
	dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
	// token field 2
	auto ptr_f2 = token_str_p;
	switch (t) {
		case webster_tokens::d_link:
			// dst_l_replace(D_LINK_SCHEME ":///");
			ptr_f2 += sizeof "d_link";
			break;
		case webster_tokens::i_link:
			// dst_l_replace(I_LINK_SCHEME ":///");
			ptr_f2 += sizeof "i_link";
			break;
		case webster_tokens::et_link:
			// dst_l_replace(ET_LINK_SCHEME ":///");
			ptr_f2 += sizeof "et_link";
			break;
		case webster_tokens::mat:
			// dst_l_replace(MAT_SCHEME ":///");
			ptr_f2 += sizeof "mat";
			break;
		default:
			throw webster_token_exception{"incorrect token type"};
	}
	//	dst[dst_start++] = ctx_.dict;
	//	dst[dst_start++] = '/';
	auto ptr_f2_end = ptr_f2;
	// end of field 2
	while (*ptr_f2_end != '|') {
		++ptr_f2_end;
	}
	auto ptr_f3 = ptr_f2_end + 1;
	auto ptr_f3_end = ptr_f3;
	auto token_end = token_str_p + token_str_len;
	// end of field 3
	while (ptr_f3_end != token_end) {
		++ptr_f3_end;
	}
	char buf[128]{};
	if (ptr_f3 != ptr_f3_end) {
		// dst_pn_replace(ptr_f3, ptr_f3_end - ptr_f3);
		auto view_enc = url_encode_view((const unsigned char*)ptr_f3, ptr_f3_end - ptr_f3, buf);
		dst_s_replace(view_enc);
	} else {
		// dst_pn_replace(ptr_f2, ptr_f2_end - ptr_f2);
		auto view_enc = url_encode_view((const unsigned char*)ptr_f2, ptr_f2_end - ptr_f2, buf);
		dst_s_replace(view_enc);
	}
	// === link ===
	dst_l_replace(R"xx(">)xx");
	// === link text ===
	dst_pn_replace(ptr_f2, ptr_f2_end - ptr_f2);
	// === link text ===
	dst_l_replace("</a>");
}

void webster_parser::webster_CRF4_sx_token_render(const char* token_str_p, size_t token_str_len, string& dst,
												  string::size_type& dst_start) {
	dst_l_replace(R"xx(<a style="font-variant: small-caps;" href="#)xx");
	// === link ===
	// scheme
	// todo context
	dst_l_replace(WEBSTER_ENTRY_IDATT_METAID_SCHEME);
	dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
	dst[dst_start++] = ctx_.dict;
	dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
	// token field 2
	auto ptr_f2 = token_str_p + sizeof "sx";
	auto ptr_f2_end = ptr_f2;
	// end of field 2
	while (*ptr_f2_end != '|') {
		++ptr_f2_end;
	}
	auto ptr_f3 = ptr_f2_end + 1;
	auto ptr_f3_end = ptr_f3;
	// end of field 3
	while (*ptr_f3_end != '|') {
		++ptr_f3_end;
	}
	auto token_end = token_str_p + token_str_len;
	auto ptr_f4 = ptr_f3_end + 1;
	auto ptr_f4_end = ptr_f4;
	// end of field 4
	while (ptr_f4_end != token_end) {
		++ptr_f4_end;
	}
	char buf[128]{};
	if (ptr_f3 != ptr_f3_end) {
		// field 3 as target id
		// dst_pn_replace(ptr_f3, ptr_f3_end - ptr_f3);
		auto view_enc = url_encode_view((const unsigned char*)ptr_f3, ptr_f3_end - ptr_f3, buf);
		dst_s_replace(view_enc);
	} else {
		// dst_pn_replace(ptr_f2, ptr_f2_end - ptr_f2);
		auto view_enc = url_encode_view((const unsigned char*)ptr_f2, ptr_f2_end - ptr_f2, buf);
		dst_s_replace(view_enc);
	}
	// === link ===
	dst_l_replace(R"xx(">)xx");
	// === link text ===
	dst_pn_replace(ptr_f2, ptr_f2_end - ptr_f2);
	// === link text ===
	dst_l_replace("</a>");
	// extra info after link and link text, e.g. sense number
	if (ptr_f4 != ptr_f4_end) {
		dst[dst_start++] = ' ';
		dst_pn_replace(ptr_f4, ptr_f4_end - ptr_f4);
	}
}

void webster_parser::webster_CRF4_dxt_token_render(const char* token_str_p, size_t token_str_len, string& dst,
												   string::size_type& dst_start) {
	dst_l_replace(R"xx(<a style="font-variant: small-caps;" href="#)xx");
	// token field 2
	auto ptr_f2 = token_str_p + sizeof "dxt";
	auto ptr_f2_end = ptr_f2;
	// end of field 2
	while (*ptr_f2_end != '|') {
		++ptr_f2_end;
	}
	auto ptr_f3 = ptr_f2_end + 1;
	auto ptr_f3_end = ptr_f3;
	// end of field 3
	while (*ptr_f3_end != '|') {
		++ptr_f3_end;
	}
	auto token_end = token_str_p + token_str_len;
	auto ptr_f4 = ptr_f3_end + 1;
	auto ptr_f4_end = ptr_f4;
	// end of field 4
	while (ptr_f4_end != token_end) {
		++ptr_f4_end;
	}
	bool f4_display{true};
	char buf[128]{};

	if (ptr_f4 != ptr_f4_end) {
		string_view::size_type f4_len = ptr_f4_end - ptr_f4;
		string_view field4{ptr_f4, f4_len};
		// === link ===
		if (field4 == "illustration") {
			//			if (ctx_.art_direct_link) {
			//				dst_l_replace(WEBSTER_ART_URL_PREFIX);
			//			} else {
			//				dst_l_replace(WEBSTER_ART_PAGE_OPEN);
			//			}
			dst_l_replace(WEBSTER_IDATT_ARTID_SCHEME);
			dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
			dst[dst_start++] = ctx_.dict;
			dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
			// dst_pn_replace(ptr_f3, ptr_f3_end - ptr_f3);
			auto view_enc = url_encode_view((const unsigned char*)ptr_f3, ptr_f3_end - ptr_f3, buf);
			dst_s_replace(view_enc);
			// dst_l_replace(WEBSTER_ART_PAGE_CLOSE);
		} else if (field4 == "table") {
			f4_display = false;
			//			dst_l_replace(WEBSTER_TABLE_PAGE_OPEN);
			//			dst_pn_replace(ptr_f3, ptr_f3_end - ptr_f3);
			//			dst_l_replace(WEBSTER_TABLE_PAGE_CLOSE);
			dst_l_replace(WEBSTER_IDATT_TABLEID_SCHEME);
			dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
			dst[dst_start++] = ctx_.dict;
			dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
			// dst_pn_replace(ptr_f3, ptr_f3_end - ptr_f3);
			auto view_enc = url_encode_view((const unsigned char*)ptr_f3, ptr_f3_end - ptr_f3, buf);
			dst_s_replace(view_enc);
		} else {
			//			dst_l_replace(DXT_SCHEME ":///");
			//			dst[dst_start++] = ctx_.dict;
			//			dst[dst_start++] = '/';
			dst_l_replace(WEBSTER_ENTRY_IDATT_METAID_SCHEME);
			dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
			dst[dst_start++] = ctx_.dict;
			dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
			if (ptr_f3 != ptr_f3_end) {
				// dst_pn_replace(ptr_f3, ptr_f3_end - ptr_f3);
				auto view_enc = url_encode_view((const unsigned char*)ptr_f3, ptr_f3_end - ptr_f3, buf);
				dst_s_replace(view_enc);
			} else {
				// dst_pn_replace(ptr_f2, ptr_f2_end - ptr_f2);
				auto view_enc = url_encode_view((const unsigned char*)ptr_f2, ptr_f2_end - ptr_f2, buf);
				dst_s_replace(view_enc);
			}
		}
	} else {
		f4_display = false;
		//		dst_l_replace(DXT_SCHEME ":///");
		//		dst[dst_start++] = ctx_.dict;
		//		dst[dst_start++] = '/';
		dst_l_replace(WEBSTER_ENTRY_IDATT_METAID_SCHEME);
		dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
		dst[dst_start++] = ctx_.dict;
		dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
		if (ptr_f3 != ptr_f3_end) {
			// dst_pn_replace(ptr_f3, ptr_f3_end - ptr_f3);
			auto view_enc = url_encode_view((const unsigned char*)ptr_f3, ptr_f3_end - ptr_f3, buf);
			dst_s_replace(view_enc);
		} else {
			// dst_pn_replace(ptr_f2, ptr_f2_end - ptr_f2);
			auto view_enc = url_encode_view((const unsigned char*)ptr_f2, ptr_f2_end - ptr_f2, buf);
			dst_s_replace(view_enc);
		}
	}
	// === link ===
	dst_l_replace(R"xx(">)xx");
	// === link text ===
	dst_pn_replace(ptr_f2, ptr_f2_end - ptr_f2);
	// === link text ===
	dst_l_replace("</a>");
	if (f4_display) {
		dst[dst_start++] = ' ';
		dst_pn_replace(ptr_f4, ptr_f4_end - ptr_f4);
	}
}

// transform unicode code point literal to utf8 encoding
bool webster_parser::parse_RUNNING_TEXT(string& src_dst) {
	try {
		parse_stack.clear();
		auto src_p_start = static_cast<const char*>(src_dst.data());
		auto src_p_end = src_p_start + src_dst.size();
		// check for token
		bool open_brace{};
		bool close_brace{};
		auto src_p_start_c = src_p_start;
		auto src_p_end_c = src_p_end;
		while (src_p_start_c != src_p_end_c) {
			auto c = *src_p_start_c;
			if (c == '{') {
				open_brace = true;
			} else if (c == '}') {
				close_brace = true;
			}
			++src_p_start_c;
		}
		if (not(open_brace and close_brace)) return true;
		// string tmp;
		auto size_predict = static_cast<string::size_type>(src_p_end - src_p_start) * 3 + 500;
		if (buffer_.size() < size_predict) buffer_.resize(size_predict);
		string::size_type tmp_i{};
		auto src_p = src_p_start;
		char c;
		for (;;) {
			// step 1. copy text
			// boundary check
			if (src_p == src_p_end) {
				if (!parse_stack.empty()) {
					// an error occurred
					cout << src_dst << endl;
					cout << "stack size: " << parse_stack.size() << endl;
					throw webster_data_exception{"reach end of running text with unfinished parse stack when "
												 "process running text (normal)"};
				}
				break;
			}
			c = *src_p;
			if (c == '{') {
				++src_p;
			} else {
				buffer_[tmp_i] = c;
				++tmp_i;
				++src_p;
				continue;
			}
			// step 2. handle token
			auto token_p = src_p;
			for (;;) {
				// boundary check
				if (src_p == src_p_end) {
					if (!parse_stack.empty())
						// an error occurred
						throw webster_data_exception{
							"reach end of running text with unfinished parse stack when "
							"process running text (token)"};
					break;
				}
				if (*src_p != '}') {
					++src_p;
					continue;
				}
				auto token_p_end = src_p;
				auto token_len = static_cast<size_t>(token_p_end - token_p);
				++src_p;
				// process the token (or half of the token pair)
				auto t = token_type(token_p, token_len);
				switch (get_token_meta_type(t)) {
					case webster_token_meta::open:
						webster_token_render(t, token_p, token_len, buffer_, tmp_i);
						parse_stack.emplace_front(t);
						break;
					case webster_token_meta::close:
						// expect matching top of the stack
						// todo match
						if (!parse_stack.empty() && token_type_match(parse_stack.front(), t)) {
							parse_stack.pop_front();
							webster_token_render(t, token_p, token_len, buffer_, tmp_i);
						} else {
							throw webster_data_exception{"close token and stack top token mismatch"};
						}
						break;
					case webster_token_meta::single:
						webster_token_render(t, token_p, token_len, buffer_, tmp_i);
						break;
				}
				break;
			}
		}
		if (!parse_stack.empty())
			throw webster_data_exception{"running text parsing routine end with not-empty parse stack"};
		src_dst = string_view{buffer_.data(), tmp_i}; // std::move(tmp);
		return true;
	} catch (const std::exception& e) {
		cout << "parsing running text error: " << e.what() << endl;
		cout << "the text causes exception: " << src_dst << endl;
		return false;
	}
}

void webster_parser::process_entry(string& dst, string::size_type& dst_start, pt::ptree& node) {
	process_entry_read_meta(node);
	// todo separator?
	if (ctx_.meta and ctx_.dict != 0) {
		auto&& meta_id = ctx_.meta->get<string>("id", {});
		if (meta_id.empty()) {
			dst_l_replace(WEBSTER_ENTRY_OPEN);
		} else {
			char buf[128]{};
			auto view_enc = url_encode_view((const unsigned char*)meta_id.data(), meta_id.size(), buf);
			dst_l_replace(WEBSTER_ENTRY_OPEN_OPEN);
			dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
			dst[dst_start++] = ctx_.dict;
			dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
			dst_s_replace(view_enc);
			dst_l_replace(WEBSTER_ENTRY_OPEN_CLOSE);
		}
	} else {
		dst_l_replace(WEBSTER_ENTRY_OPEN);
	}
	for (auto&& i : node) {
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::meta:
				// process_node_meta(i.second);
				break;
			case webster_data_structure::hom:
				process_node_hom(dst, dst_start, i.second);
				break;
			case webster_data_structure::hwi:
				process_node_hwi(dst, dst_start, i.second);
				dst_l_replace("<br>");
				break;
			case webster_data_structure::gram:
				PROCESS_NODE_GRAM(dst, dst_start, i.second);
				break;
			case webster_data_structure::ahws:
				process_array_ahws(dst, dst_start, i.second);
				break;
			case webster_data_structure::vrs:
				process_array_vrs(dst, dst_start, i.second);
				break;
			case webster_data_structure::fl:
				PROCESS_NODE_FL(dst, dst_start, i.second);
				dst_l_replace("<br>");
				break;
			case webster_data_structure::cxs:
				process_array_cxs(dst, dst_start, i.second);
				break;
			case webster_data_structure::ins:
				process_array_ins(dst, dst_start, i.second);
				dst_l_replace("<br>");
				break;
			case webster_data_structure::lbs:
				process_array_lbs(dst, dst_start, i.second);
				dst_l_replace("<br>");
				break;
			case webster_data_structure::sls:
				process_array_sls(dst, dst_start, i.second);
				break;
			case webster_data_structure::def:
				process_array_def(dst, dst_start, i.second);
				break;
			case webster_data_structure::uros:
				dst_l_replace("<br>");
				process_array_uros(dst, dst_start, i.second);
				break;
			case webster_data_structure::dros:
				process_array_dros(dst, dst_start, i.second);
				break;
			case webster_data_structure::dxnls:
				process_array_dxnls(dst, dst_start, i.second);
				break;
			case webster_data_structure::usages:
				process_array_usages(dst, dst_start, i.second);
				break;
			case webster_data_structure::syns:
				process_array_syns(dst, dst_start, i.second);
				break;
			case webster_data_structure::quotes:
				process_array_quotes(dst, dst_start, i.second);
				break;
			case webster_data_structure::art:
				process_node_art(dst, dst_start, i.second);
				break;
			case webster_data_structure::artl:
				process_node_artl(dst, dst_start, i.second);
				break;
			case webster_data_structure::table:
				process_node_table(dst, dst_start, i.second);
				break;
			case webster_data_structure::srefs:
				process_array_srefs(dst, dst_start, i.second);
				break;
			case webster_data_structure::list:
				process_array_list(dst, dst_start, i.second);
				break;
			case webster_data_structure::et:
				dst_l_replace("<br>");
				process_array_et(dst, dst_start, i.second);
				break;
			case webster_data_structure::date:
				PROCESS_NODE_DATE(dst, dst_start, i.second);
				break;
			case webster_data_structure::shortdef:
				// todo impl shortdef
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first +
											 "\" in top level entry"};
		}
	}
	dst_l_replace(WEBSTER_ENTRY_CLOSE);
}

void webster_parser::process_entry_read_meta(pt::ptree& node) {
	for (auto&& i : node) {
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::meta:
				ctx_.process_node_meta(i.second);
				return;
			case webster_data_structure::hom:
			case webster_data_structure::hwi:
			case webster_data_structure::gram:
			case webster_data_structure::ahws:
			case webster_data_structure::vrs:
			case webster_data_structure::fl:
			case webster_data_structure::cxs:
			case webster_data_structure::ins:
			case webster_data_structure::lbs:
			case webster_data_structure::sls:
			case webster_data_structure::def:
			case webster_data_structure::uros:
			case webster_data_structure::dros:
			case webster_data_structure::dxnls:
			case webster_data_structure::usages:
			case webster_data_structure::syns:
			case webster_data_structure::quotes:
			case webster_data_structure::art:
			case webster_data_structure::artl:
			case webster_data_structure::table:
			case webster_data_structure::srefs:
			case webster_data_structure::list:
			case webster_data_structure::et:
			case webster_data_structure::date:
			case webster_data_structure::shortdef:
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first +
											 "\" in top level entry"};
		}
	}
}

void webster_parser::doc_ctx::do_a_link_map(pt::ptree& node) {
	for (auto&& i : node) {
		if (i.first == "") {
			for (auto&& j : i.second) {
				auto ds_t = data_structure_type(j.first);
				switch (ds_t) {
					case webster_data_structure::meta:
						do_a_link_map_meta(j.second);
						continue;
					default:
						break;
				}
			}
		}
	}
	reset_entry_ctx();
}

void webster_parser::doc_ctx::do_a_link_map_meta(pt::ptree& node) {
	meta = &node;
	for (auto&& i : node) {
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::id: {
				char buf[128]{};
				auto view_enc = url_encode_view((const unsigned char*)i.second.data().data(),
												i.second.data().size(), buf);
				metaid_enc = view_enc;
				qualified_metaid_v.emplace_back(string{'#'} + WEBSTER_ENTRY_IDATT_METAID_SCHEME +
												WEBSTER_IDATT_SEPARATOR + dict + WEBSTER_IDATT_SEPARATOR +
												string{view_enc});
			} break;
			case webster_data_structure::stems:
				if (!metaid_enc.empty())
					for (const auto& j : i.second) {
						if (j.first == "" and !j.second.data().empty()) {
							a_link_map.try_emplace(j.second.data(), metaid_enc);
						}
					}
				break;
			default:
				break;
				// throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"meta\""};
		}
	}
}

void webster_parser::doc_ctx::process_node_meta(pt::ptree& node) {
	meta = &node;
	for (auto&& i : node) {
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::id: {
				char buf[128]{};
				auto view_enc = url_encode_view((const unsigned char*)i.second.data().data(),
												i.second.data().size(), buf);
				metaid_enc = view_enc;
			} break;
			case webster_data_structure::uuid:
			case webster_data_structure::sort:
			case webster_data_structure::src:
			/* {
				auto& s = i.second.data();
				if (s == WEBSTER_Collegiate_src) {
					ctx_.dict = WEBSTER_Collegiate;
				} else if (s == WEBSTER_Collegiate_Thesaurus_src) {
					ctx_.dict = WEBSTER_Collegiate_Thesaurus;
				} else if (s == WEBSTER_Learner_Dictionary_src) {
					ctx_.dict = WEBSTER_Learner_Dictionary;
				} else {
					throw webster_data_exception{"unknown src \"" + s + "\" in \"meta\""};
				}
			} */
			case webster_data_structure::section:
			case webster_data_structure::offensive:
			case webster_data_structure::syns:
			case webster_data_structure::ants:
			case webster_data_structure::target:
			case webster_data_structure::app_shortdef:
				// todo and "shortdef" outside of meta node
			case webster_data_structure::stems:
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"meta\""};
		}
	}
}

void webster_parser::process_node_hom(string& dst, string::size_type& dst_start, pt::ptree& node) {
	str_literal_replace(dst, dst_start, "<sup>");
	auto& v = node.data();
	dst_s_replace(v);
	str_literal_replace(dst, dst_start, "</sup>");
}

void webster_parser::process_array_prs(string& dst, string::size_type& dst_start, pt::ptree& node) {
	// pronunciation objects separator with default value
	string separator{", "};
	// search "pun"
	for (auto&& i : node) {
		for (auto&& j : i.second) {
			if (j.first == "pun") {
				separator = j.second.data() + ' ';
				goto separator_got;
			}
		}
	}
separator_got:
	dst[dst_start++] = '\\';
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) dst_s_replace(separator);
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::mw:
					dst_s_replace(j.second.data());
					break;
				case webster_data_structure::l:
					dst_s_replace(j.second.data());
					dst[dst_start++] = ' ';
					break;
				case webster_data_structure::l2:
					dst[dst_start++] = ' ';
					dst_s_replace(j.second.data());
					break;
				case webster_data_structure::ipa:
					PROCESS_NODE_IPA(dst, dst_start, j.second);
					break;
				case webster_data_structure::wod:
					PROCESS_NODE_WOD(dst, dst_start, j.second);
					break;
				case webster_data_structure::pun:
					break;
				case webster_data_structure::sound:
					// <a href="url">link text</a>
					// dst_l_replace(" <a style=\"text-decoration:none;\" href=\"");
					dst_l_replace(R"xx( <a style="text-decoration:none;" href=")xx");
					// build url
					// reference https://www.dictionaryapi.com/products/json#sec-2.prs
					// structure https://media.merriam-webster.com/soundc11/[subdirectory]/[base
					// filename].wav
					{
						dst_l_replace(WEBSTER_S_URL_PREFIX_QT);
						for (auto&& k : j.second) {
							if (k.first == "audio") {
								auto& audio_v = k.second.data();
								// url subdirectory
								switch (audio_v[0]) {
									case 'b':
										if (audio_v.size() > 2 and audio_v[1] == 'i' and audio_v[2] == 'x') {
											dst_l_replace("bix/");
										} else {
											dst[dst_start++] = audio_v[0];
											dst[dst_start++] = '/';
										}
										break;
									case 'g':
										if (audio_v.size() > 1 and audio_v[1] == 'g') {
											dst_l_replace("gg/");
										} else {
											dst[dst_start++] = audio_v[0];
											dst[dst_start++] = '/';
										}
										break;
									default:
										if (('a' <= audio_v[0] and audio_v[0] <= 'z') or
											('A' <= audio_v[0] and audio_v[0] <= 'Z')) {
											dst[dst_start++] = audio_v[0];
											dst[dst_start++] = '/';
										} else {
											dst_l_replace("number/");
										}
										break;
								}
								// url filename encodig
								char buf[128]{};
								auto view_enc = url_encode_view((const unsigned char*)audio_v.data(),
																audio_v.size(), buf);
								dst_s_replace(view_enc);
								dst_l_replace(".wav");
								break;
							}
						}
					}
					// dst_l_replace("\">");
					dst_l_replace(R"xx(">)xx");
					// link text
					dst_l_replace(WEBSTER_S_LINK_TEXT);
					dst_l_replace("</a>");
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"prs\""};
			}
		}
	}
	dst[dst_start++] = '\\';
}

void webster_parser::process_array_ahws(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) dst[dst_start++] = ' ';
		uint16_t m = 0;
		for (auto&& j : i.second) {
			if (m++) dst[dst_start++] = ' ';
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::hw:
					PROCESS_NODE_HW(dst, dst_start, j.second);
					break;
				case webster_data_structure::prs:
					// separator between hw and prs
					dst[dst_start++] = ' ';
					process_array_prs(dst, dst_start, j.second);
					break;
				case webster_data_structure::psl:
					dst[dst_start++] = ' ';
					PROCESS_NODE_PSL(dst, dst_start, j.second);
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"ahws\""};
			}
		}
	}
}

void webster_parser::process_node_hwi(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::hw:
				PROCESS_NODE_HW(dst, dst_start, i.second);
				break;
			case webster_data_structure::prs:
				// separator between hw and prs
				dst[dst_start++] = ' ';
				process_array_prs(dst, dst_start, i.second);
				break;
			case webster_data_structure::altprs:
				// todo separator?
				dst[dst_start++] = ' ';
				process_node_altprs(dst, dst_start, i.second);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"hwi\""};
		}
	}
}

void webster_parser::process_array_vrs(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	for (auto&& i : node) {
		uint16_t m = 0;
		if (n++) dst[dst_start++] = ' ';
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			if (m++) dst[dst_start++] = ' ';
			switch (ds_t) {
				case webster_data_structure::va:
					PROCESS_NODE_VA(dst, dst_start, j.second);
					break;
				case webster_data_structure::vl:
					PROCESS_NODE_VL(dst, dst_start, j.second);
					break;
				case webster_data_structure::prs:
					process_array_prs(dst, dst_start, j.second);
					break;
				case webster_data_structure::altprs:
					process_node_altprs(dst, dst_start, j.second);
					break;
				case webster_data_structure::spl:
					PROCESS_NODE_SPL(dst, dst_start, j.second);
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"vrs\""};
			}
		}
	}
}

// todo check all separator
void webster_parser::process_array_cxs(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) {
			dst[dst_start++] = ',';
			dst[dst_start++] = ' ';
		}
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::cxl:
					PROCESS_NODE_CXL(dst, dst_start, j.second);
					dst[dst_start++] = ' ';
					break;
				case webster_data_structure::cxtis: {
					for (auto&& k : j.second) {
						pt::ptree* node_cxr{};
						for (auto&& l : k.second) {
							if (l.first == "cxr") {
								node_cxr = &l.second;
								break;
							}
						}
						for (auto&& l : k.second) {
							auto ds_t0 = data_structure_type(l.first);
							switch (ds_t0) {
								case webster_data_structure::cxl:
									PROCESS_NODE_CXL(dst, dst_start, l.second);
									dst[dst_start++] = ' ';
									break;
								case webster_data_structure::cxr:
									break;
								case webster_data_structure::cxt: {
									dst_l_replace(R"xx(<a style="font-variant: small-caps;" href=")xx");
									// === link ===
									dst_l_replace(WEBSTER_CXR_URL_PREFIX);
									dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
									dst[dst_start++] = ctx_.dict;
									dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
									// dst[dst_start++] = '/';
									char buf[128]{};
									if (node_cxr) {
										auto view_enc =
											url_encode_view((const unsigned char*)node_cxr->data().data(),
															node_cxr->data().size(), buf);
										dst_s_replace(view_enc);
									} else {
										auto view_enc =
											url_encode_view((const unsigned char*)l.second.data().data(),
															l.second.data().size(), buf);
										dst_s_replace(view_enc);
									}
									// === link ===
									dst_l_replace("\">");
									dst_s_replace(l.second.data()); // link text
									dst_l_replace("</a>");
								} break;
								case webster_data_structure::cxn:
									// sense number is not part of link or link text!
									dst[dst_start++] = ' ';
									dst_s_replace(l.second.data());
									break;
								default:
									throw webster_data_exception{"unexpected data structure \"" + l.first +
																 "\" in \"cxtis\""};
							}
						}
					}
				} break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"cxs\""};
			}
		}
	}
}

void webster_parser::process_array_ins(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	for (auto&& i : node) {
		/* Inflection objects should be separated by a semicolon and space unless the second object of a pair
		   contains il, in which case they should be separated by a space.*/
		bool il_flag{};
		bool use_if{true};
		uint16_t if_ifc_flag = 0;
		for (auto&& j : i.second) {
			if (j.first == "il") {
				il_flag = true;
			} else if (j.first == "if") {
				if_ifc_flag |= 0b01;
			} else if (j.first == "ifc") {
				if_ifc_flag |= 0b10;
			}
		}
		switch (if_ifc_flag) {
			case 1:
				break;
			case 2:
				use_if = false;
				break;
			case 3:
				if (ctx_.print) use_if = false;
				break;
			default:
				// if and ifc node are optional
				// throw webster_data_exception{"missing if or ifc node in \"ins\""};
				break;
		}

		if (n++) {
			if (not il_flag) dst[dst_start++] = ';';
			dst[dst_start++] = ' ';
		}
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::if_:
					if (use_if) PROCESS_NODE_(dst, dst_start, j.second, "<b>", "</b>");
					break;
				case webster_data_structure::ifc:
					if (not use_if) PROCESS_NODE_(dst, dst_start, j.second, "<b>", "</b>");
					break;
				case webster_data_structure::il:
					dst_s_replace(j.second.data());
					dst[dst_start++] = ' ';
					break;
				case webster_data_structure::prs:
					dst[dst_start++] = ' ';
					process_array_prs(dst, dst_start, j.second);
					break;
				case webster_data_structure::altprs:
					dst[dst_start++] = ' ';
					process_node_altprs(dst, dst_start, j.second);
					break;
				case webster_data_structure::spl:
					dst[dst_start++] = ' ';
					PROCESS_NODE_SPL(dst, dst_start, j.second);
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"ins\""};
			}
		}
	}
}

void webster_parser::process_array_lbs(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) {
			dst[dst_start++] = ',';
			dst[dst_start++] = ' ';
		}
		dst_l_replace("<i>");
		dst_s_replace(i.second.data());
		dst_l_replace("</i>");
	}
}

void webster_parser::process_array_sls(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) {
			dst[dst_start++] = ',';
			dst[dst_start++] = ' ';
		}
		dst_l_replace("<i>");
		dst_s_replace(i.second.data());
		dst_l_replace("</i>");
	}
}

void webster_parser::process_node_aq(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) {
			dst[dst_start++] = ',';
			dst[dst_start++] = ' ';
		}
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::auth:
			case webster_data_structure::source:
			case webster_data_structure::aqdate:
				dst_s_replace(i.second.data());
				break;
			case webster_data_structure::subsource: {
				uint16_t m = 0;
				for (auto&& j : i.second) {
					if (m++) {
						dst[dst_start++] = ',';
						dst[dst_start++] = ' ';
					}
					auto ds_t0 = data_structure_type(j.first);
					switch (ds_t0) {
						case webster_data_structure::source:
						case webster_data_structure::aqdate:
							dst_s_replace(j.second.data());
							break;
						default:
							throw webster_data_exception{"unexpected data structure \"" + j.first +
														 "\" in \"subsource\""};
					}
				}
			} break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"aq\""};
		}
	}
}

void webster_parser::process_array_next2_vis(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		uint16_t m = 0;
		// todo encoding?
		dst_l_replace("&lt;");
		for (auto&& j : i.second) {
			if (m++) dst[dst_start++] = ' ';
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::t:
					dst_s_replace(j.second.data());
					break;
				case webster_data_structure::aq:
					process_node_aq(dst, dst_start, j.second);
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"vis\""};
			}
		}
		// todo encoding?
		dst_l_replace("&gt;");
	}
}

// todo obscure data structure?
// todo api document indention error like "sarefs"
// based on example from webster api document
void webster_parser::process_array_next2_ri(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		auto it = i.second.begin();
		auto ds_t = data_structure_type(it->second.data());
		++it;
		auto& node_ = it->second;
		switch (ds_t) {
			case webster_data_structure::text:
				dst_s_replace(node_.data());
				break;
			case webster_data_structure::g:
				PROCESS_NODE_NEXT2_G(dst, dst_start, node_);
				break;
			case webster_data_structure::riw:
				// expect one child (based on example)
				if (node_.size() == 1) {
					if (node_.begin()->first == "rie") {
						dst_l_replace("<b>");
						auto& v = node_.begin()->second.data();
						dst_s_replace(v);
						dst_l_replace("</b>");
						break;
					}
				}
				throw webster_data_exception{
					"obscure data structure in \"ri\", plz contact author who wrote webster json parse code"};
			default:
				throw webster_data_exception{"unexpected data structure \"" +
											 i.second.begin()->second.data() + "\" in \"ri\""};
		}
	}
}

void webster_parser::process_array_next2_uns(string& dst, string::size_type& dst_start, pt::ptree& node) {
	// todo encoding?
	dst_l_replace(" \u2014"); // em-dash
	for (auto&& i : node) {
		for (auto&& j : i.second) {
			if (j.second.size() == 2) {
				auto it = j.second.begin();
				auto ds_t = data_structure_type(it->second.data());
				++it;
				auto& node_ = it->second;
				switch (ds_t) {
					case webster_data_structure::text:
						dst_s_replace(node_.data());
						break;
					case webster_data_structure::ri:
						process_array_next2_ri(dst, dst_start, node);
						break;
					case webster_data_structure::vis:
						process_array_next2_vis(dst, dst_start, node_);
						break;
					case webster_data_structure::g:
						PROCESS_NODE_NEXT2_G(dst, dst_start, node_);
						break;
					default:
						throw webster_data_exception{"unexpected data structure \"" +
													 j.second.begin()->second.data() + "\" in \"uros\""};
				}
			}
		}
	}
}

// todo separator
void webster_parser::process_array_utxt(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		auto it = i.second.begin();
		auto ds_t = data_structure_type(it->second.data());
		++it;
		auto& node_ = it->second;
		switch (ds_t) {
			case webster_data_structure::vis:
				process_array_next2_vis(dst, dst_start, node_);
				break;
			case webster_data_structure::uns:
				process_array_next2_uns(dst, dst_start, node_);
				break;
			case webster_data_structure::snotebox:
				process_array_snotebox(dst, dst_start, node_);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" +
											 i.second.begin()->second.data() + "\" in \"utxt\""};
		}
	}
}

void webster_parser::process_array_uros(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) {
			dst[dst_start++] = ',';
			dst[dst_start++] = ' ';
		}
		uint16_t m = 0;
		for (auto&& j : i.second) {
			if (m++) dst[dst_start++] = ' ';
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::ure:
					// todo encoding?
					dst_l_replace("\u2014"); // em-dash
					dst_l_replace("<b>");
					dst_s_replace(j.second.data());
					dst_l_replace("</b>");
					// dst[dst_start++] = ' ';
					break;
				case webster_data_structure::fl:
					PROCESS_NODE_FL(dst, dst_start, j.second);
					break;
				case webster_data_structure::utxt:
					process_array_utxt(dst, dst_start, j.second);
					break;
				case webster_data_structure::ins:
					process_array_ins(dst, dst_start, j.second);
					break;
				case webster_data_structure::lbs:
					process_array_lbs(dst, dst_start, j.second);
					break;
				case webster_data_structure::prs:
					process_array_prs(dst, dst_start, j.second);
					break;
				case webster_data_structure::altprs:
					process_node_altprs(dst, dst_start, j.second);
					break;
				case webster_data_structure::psl:
					PROCESS_NODE_PSL(dst, dst_start, j.second);
					break;
				case webster_data_structure::sls:
					process_array_sls(dst, dst_start, j.second);
					break;
				case webster_data_structure::vrs:
					process_array_vrs(dst, dst_start, j.second);
					break;
				case webster_data_structure::gram:
					PROCESS_NODE_GRAM(dst, dst_start, j.second);
					break;
				case webster_data_structure::rsl:
					PROCESS_NODE_RSL(dst, dst_start, j.second);
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"uros\""};
			}
		}
	}
}

void webster_parser::process_array_et(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		auto it = i.second.begin();
		auto ds_t = data_structure_type(it->second.data());
		++it;
		auto& node_ = it->second;
		switch (ds_t) {
			case webster_data_structure::text:
				dst_s_replace(node_.data());
				break;
			case webster_data_structure::et_snote: {
				uint16_t n = 0;
				for (auto&& j : node_) {
					auto it0 = j.second.begin();
					if (it0->second.data() == "t") {
						if (n++) dst[dst_start++] = ' ';
						++it0;
						dst_s_replace(it0->second.data());
					} else {
						throw webster_data_exception{"unexpected data structure \"" + it0->second.data() +
													 "\" in \"et_snote\""};
					}
				}
			} break;
			default:
				throw webster_data_exception{"unexpected data structure \"" +
											 i.second.begin()->second.data() + "\" in \"et\""};
		}
	}
}

void webster_parser::process_array_dros(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::drp:
					PROCESS_NODE_DRP(dst, dst_start, j.second);
					break;
				case webster_data_structure::def:
					// todo impl def
					break;
				case webster_data_structure::et:
					process_array_et(dst, dst_start, j.second);
					break;
				case webster_data_structure::lbs:
					process_array_lbs(dst, dst_start, j.second);
					break;
				case webster_data_structure::prs:
					process_array_prs(dst, dst_start, j.second);
					break;
				case webster_data_structure::psl:
					PROCESS_NODE_PSL(dst, dst_start, j.second);
					break;
				case webster_data_structure::sls:
					process_array_sls(dst, dst_start, j.second);
					break;
				case webster_data_structure::vrs:
					process_array_vrs(dst, dst_start, j.second);
					break;
				case webster_data_structure::gram:
					PROCESS_NODE_GRAM(dst, dst_start, j.second);
					break;
				case webster_data_structure::rsl:
					PROCESS_NODE_RSL(dst, dst_start, j.second);
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"dros\""};
			}
		}
	}
}

void webster_parser::process_array_dxnls(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace("<p>");
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) dst[dst_start++] = ' ';
		dst_s_replace(i.second.data());
	}
	dst_l_replace("</p>");
}

void webster_parser::process_array_next2_urefs(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace("Usage see: ");
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) {
			dst[dst_start++] = ',';
			dst[dst_start++] = ' ';
		}
		dst_l_replace(R"xx(<a style="font-variant: small-caps;" href=")xx");
		// === url ===
		dst_l_replace(WEBSTER_UREFS_URL_PREFIX);
		dst[dst_start++] = ctx_.dict;
		dst[dst_start++] = '/';
		dst_s_replace(i.second.data());
		// === url ===
		dst_l_replace("\">");
		dst_s_replace(i.second.data());
		dst_l_replace("</a>");
	}
}

void webster_parser::process_array_usages(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		pt::ptree* uarefs_node{};
		for (auto&& j : i.second) {
			if (j.first == "uarefs") {
				uarefs_node = &j.second;
				break;
			}
		}
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::pl:
					PROCESS_NODE_PL(dst, dst_start, j.second);
					break;
				case webster_data_structure::pt:
					dst_l_replace("<p>");
					{
						for (auto&& k : j.second) {
							auto it = k.second.begin();
							auto ds_t0 = data_structure_type(it->second.data());
							++it;
							switch (ds_t0) {
								case webster_data_structure::text:
									dst_s_replace(it->second.data());
									break;
								case webster_data_structure::vis:
									process_array_next2_vis(dst, dst_start, it->second);
									break;
								default:
									throw webster_data_exception{"unexpected data structure \"" + k.first +
																 "\" in \"pt\""};
							}
						}
					}
					if (uarefs_node) {
						auto& node_ = *uarefs_node;
						dst_l_replace("<b>usage </b>see in addition ");
						uint16_t n = 0;
						for (auto&& k : node_) {
							if (n++) dst[dst_start++] = ' ';
							dst_l_replace(R"xx(<a href=")xx");
							// === url ===
							dst_l_replace(WEBSTER_UAREFS_URL_PREFIX);
							dst[dst_start++] = ctx_.dict;
							dst[dst_start++] = '/';
							dst_s_replace(k.second.data());
							// === url ===
							dst_l_replace("\">");
							dst_s_replace(k.second.data());
							dst_l_replace("</a>");
						}
					}
					dst_l_replace("</p>");
					break;
				case webster_data_structure::uarefs:
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first +
												 "\" in \"usages\""};
			}
		}
	}
}

void webster_parser::process_array_wvbvrs(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::wvbvl:
					PROCESS_NODE_WVBVL(dst, dst_start, j.second);
					break;
				case webster_data_structure::wvbva:
					PROCESS_NODE_WVBVA(dst, dst_start, j.second);
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first +
												 "\" in \"wvbvrs\""};
			}
		}
	}
}
void webster_parser::process_array_wvrs(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace("(");
	for (auto&& i : node) {
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::wvl:
					PROCESS_NODE_(dst, dst_start, j.second, "<i>", "</i> ");
					break;
				case webster_data_structure::wva:
					PROCESS_NODE_WVA(dst, dst_start, j.second);
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"wvrs\""};
			}
		}
	}
	dst_l_replace(")");
}

void webster_parser::process_node_wsls(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace("(<i>");
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) {
			dst[dst_start++] = ',';
			dst[dst_start++] = ' ';
		}
		if (i.first != "wsl" and !i.first.empty())
			throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"wsls\""};
		dst_s_replace(i.second.data());
	}
	dst_l_replace("</i>)");
}
void webster_parser::process_array___list(string& dst, string::size_type& dst_start, pt::ptree& node,
										  const char* node_name) {
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) {
			dst_l_replace("<br>");
		}
		uint16_t m = 0;
		for (auto&& j : i.second) {
			if (m++) {
				dst[dst_start++] = ',';
				dst[dst_start++] = ' ';
			}
			for (auto&& k : j.second) {
				auto ds_t = data_structure_type(k.first);
				switch (ds_t) {
					case webster_data_structure::wd:
						PROCESS_NODE_WD(dst, dst_start, k.second);
						break;
					case webster_data_structure::wvbvrs:
						process_array_wvbvrs(dst, dst_start, k.second);
						break;
					case webster_data_structure::wvrs:
						process_array_wvrs(dst, dst_start, k.second);
						break;
					case webster_data_structure::wsls:
						process_node_wsls(dst, dst_start, k.second);
						break;
					default:
						throw webster_data_exception{"unexpected data structure \"" + k.first + "\" in \"" +
													 node_name + "\""};
				}
			}
		}
	}
}

void webster_parser::process_array_ant_list(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace(HEAD_OPEN_TAG "Antonyms of ");
	dst_s_replace(ctx_.headword);
	dst_l_replace(HEAD_CLOSE_TAG);
	process_array___list(dst, dst_start, node, "ant_list");
}
void webster_parser::process_array_near_list(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace(HEAD_OPEN_TAG "Near Antonyms of ");
	dst_s_replace(ctx_.headword);
	dst_l_replace(HEAD_CLOSE_TAG);
	process_array___list(dst, dst_start, node, "near_list");
}
void webster_parser::process_array_phrase_list(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace(HEAD_OPEN_TAG "Phrases Synonymous with ");
	dst_s_replace(ctx_.headword);
	dst_l_replace(HEAD_CLOSE_TAG);
	process_array___list(dst, dst_start, node, "phrase_list");
}

void webster_parser::process_array_rel_list(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace(HEAD_OPEN_TAG "Words Related to ");
	dst_s_replace(ctx_.headword);
	dst_l_replace(HEAD_CLOSE_TAG);
	process_array___list(dst, dst_start, node, "rel_list");
}

void webster_parser::process_array_syn_list(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace(HEAD_OPEN_TAG "Synonyms of ");
	dst_s_replace(ctx_.headword);
	dst_l_replace(HEAD_CLOSE_TAG);
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) {
			dst_l_replace("<br>");
		}
		uint16_t m = 0;
		for (auto&& j : i.second) {
			if (m++) {
				dst[dst_start++] = ',';
				dst[dst_start++] = ' ';
			}
			for (auto&& k : j.second) {
				auto ds_t = data_structure_type(k.first);
				switch (ds_t) {
					case webster_data_structure::wd:
						PROCESS_NODE_WD(dst, dst_start, k.second);
						break;
					case webster_data_structure::wvrs:
						process_array_wvrs(dst, dst_start, k.second);
						break;
					case webster_data_structure::wsls:
						process_node_wsls(dst, dst_start, k.second);
						break;
					default:
						throw webster_data_exception{"unexpected data structure \"" + k.first +
													 "\" in \"syn_list\""};
				}
			}
		}
	}
}
void webster_parser::process_array_syns(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		pt::ptree* sarefs_node{};
		for (auto&& j : i.second) {
			if (j.first == "sarefs") {
				sarefs_node = &j.second;
				break;
			}
		}
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::pl:
					PROCESS_NODE_PL(dst, dst_start, j.second);
					break;
				case webster_data_structure::pt:
					dst_l_replace("<p>");
					{
						for (auto&& k : j.second) {
							auto it = k.second.begin();
							auto ds_t0 = data_structure_type(it->second.data());
							++it;
							switch (ds_t0) {
								case webster_data_structure::text:
									dst_s_replace(it->second.data());
									break;
								case webster_data_structure::vis:
									process_array_next2_vis(dst, dst_start, it->second);
									break;
								default:
									throw webster_data_exception{"unexpected data structure \"" + k.first +
																 "\" in \"pt\", contact author"};
							}
						}
					}
					if (sarefs_node) {
						auto& node_ = *sarefs_node;
						dst_l_replace("<b>synonyms </b>see in addition ");
						uint16_t n = 0;
						for (auto&& k : node_) {
							if (n++) dst[dst_start++] = ' ';
							dst_l_replace("<a href=\"");
							// === url ===
							dst_l_replace(WEBSTER_SAREFS_URL_PREFIX);
							dst[dst_start++] = ctx_.dict;
							dst[dst_start++] = '/';
							dst_s_replace(k.second.data());
							// === url ===
							dst_l_replace("\">");
							dst_s_replace(k.second.data());
							dst_l_replace("</a>");
						}
					}
					dst_l_replace("</p>");
					break;
				case webster_data_structure::sarefs:
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"syns\""};
			}
		}
	}
}

void webster_parser::process_array_snotebox(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace(R"xx(<div style="border: 1px solid black;">)xx");
	dst_l_replace(u8"◆");
	for (auto&& i : node) {
		auto it = i.second.begin();
		auto ds_t = data_structure_type(it->second.data());
		++it;
		auto& node_ = it->second;
		switch (ds_t) {
			case webster_data_structure::t:
				dst_s_replace(node_.data());
				break;
			case webster_data_structure::vis:
				process_array_next2_vis(dst, dst_start, node_);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" +
											 i.second.begin()->second.data() + "\" in \"snotebox\""};
		}
	}
	dst_l_replace("</div>");
}

void webster_parser::process_array_quotes(string& dst, string::size_type& dst_start, pt::ptree& node) {
	// "Examples of [headword] in a Sentence"
	dst_l_replace(HEAD_OPEN_TAG "Examples of ");
	dst_s_replace(ctx_.headword);
	dst_l_replace(" in a Sentence</h5>");
	dst_l_replace("<p>");
	for (auto&& i : node) {
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::t:
					dst_s_replace(j.second.data());
					dst[dst_start++] = ' ';
					break;
				case webster_data_structure::aq:
					process_node_aq(dst, dst_start, j.second);
					// todo separator?
					dst[dst_start++] = '.';
					dst[dst_start++] = ' ';
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first +
												 "\" in \"quotes\""};
			}
		}
	}
	dst_l_replace("</p>");
}

// todo img and capt?
void webster_parser::process_node_art(string& dst, string::size_type& dst_start, pt::ptree& node) {
	auto it = node.begin();
	if (it->first != "artid")
		throw webster_data_exception{"unexpected data structure \"" + it->first + "\" in \"art\""};
	auto& node_ = it->second;
	char buf[128]{};
	auto view_enc = url_encode_view((const unsigned char*)node_.data().data(), node_.data().size(), buf);
	// add link
	dst_l_replace(R"xx(<div align="center"><img style="img{width: 50%;}" src=")xx");
	if (ctx_.art_direct_link) {
		dst_l_replace(WEBSTER_ART_URL_PREFIX);
		dst_s_replace(view_enc);
		dst_l_replace(WEBSTER_ART_URL_SUFFIX);
		img_url_v.emplace_back(WEBSTER_ART_URL_PREFIX + string{view_enc} + WEBSTER_ART_URL_SUFFIX);
	} else {
		dst_l_replace(WEBSTER_ART_PAGE_OPEN);
		dst_s_replace(view_enc);
		dst_l_replace(WEBSTER_ART_PAGE_CLOSE);
	}
	// add id
	dst_l_replace(R"xx(" id=")xx");
	dst_l_replace(WEBSTER_IDATT_ARTID_SCHEME);
	dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
	dst[dst_start++] = ctx_.dict;
	dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
	dst_s_replace(view_enc);
	dst_l_replace(R"xx("><br>)xx");
	// todo link text with capt or not?
	++it;
	if (it != node.end()) {
		if (it->first != "capt")
			throw webster_data_exception{"unexpected data structure \"" + it->first + "\" in \"art\""};
		auto& capt_str = it->second.data();
		dst_s_replace(capt_str);
	}
	dst_l_replace("</div>");
}

void webster_parser::process_node_artl(string& dst, string::size_type& dst_start, pt::ptree& node) {
	auto it = node.begin();
	if (it->first != "artid")
		throw webster_data_exception{"unexpected data structure \"" + it->first + "\" in \"artl\""};
	auto& node_ = it->second;
	dst_l_replace(R"xx(<div align="center"><img style="img{width: 50%;}" src=")xx");
	// === link ===
	auto link_b = dst_start;
	dst_l_replace(WEBSTER_ARTL_URL_PREFIX);
	auto& artid = node_.data();
	auto p = artid.find_last_of('.');
	char buf[128]{};
	auto view_enc = url_encode_view((const unsigned char*)node_.data().data(), p, buf);
	decltype(view_enc)::size_type p0{};
	// copy without artid extension
	while (p0 != view_enc.size()) {
		dst[dst_start++] = view_enc[p0++];
	}
	dst_l_replace(WEBSTER_ARTL_URL_SUFFIX);
	img_url_v.emplace_back(dst.data() + link_b, dst_start - link_b);
	// === link ===
	// add id attribute
	dst_l_replace(R"xx(" id=")xx");
	dst_l_replace(WEBSTER_IDATT_ARTID_SCHEME);
	dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
	dst[dst_start++] = ctx_.dict;
	dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
	dst_s_replace(view_enc);
	dst_l_replace(R"xx("><br>)xx");
	// === image caption ===
	while (++it != node.end()) {
		if (it->first == "capt") {
			dst_s_replace(it->second.data());
			// == image caption ==
			// dst_l_replace("</a>");
			return;
		}
	}
	// artid as image caption
	// copy without artid extension
	decltype(p) p00{};
	while (p00 != p) {
		dst[dst_start++] = artid[p00++];
	}
	// === image caption ===
	dst_l_replace("</div>");
}

void webster_parser::process_node_table(string& dst, string::size_type& dst_start, pt::ptree& node) {
	auto it = node.begin();
	if (it->first != "tableid")
		throw webster_data_exception{"unexpected data structure \"" + it->first + "\" in \"table\""};
	auto& node_ = it->second;
	dst_l_replace(R"xx(<p><a href=")xx");
	// === link ===
	dst_l_replace(WEBSTER_TABLE_PAGE_OPEN);
	char buf[128]{};
	// auto p = node_.data().find_last_of('.');
	auto view_enc = url_encode_view((const unsigned char*)node_.data().data(), node_.data().size(), buf);
	dst_s_replace(view_enc);
	dst_l_replace(WEBSTER_TABLE_PAGE_CLOSE);
	// === link ===
	// add id attribute
	dst_l_replace(R"xx(" id=")xx");
	dst_l_replace(WEBSTER_IDATT_TABLEID_SCHEME);
	dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
	dst[dst_start++] = ctx_.dict;
	dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
	dst_s_replace(view_enc);
	dst_l_replace(R"xx(">)xx");
	// === link text ===
	if (++it != node.end()) {
		// expect a "displayname"
		if (it->first == "displayname") {
			// displayname as link text
			dst_s_replace(it->second.data());
			// == link text ==
			dst_l_replace("</a></p>");
			return;
		} else {
			throw webster_data_exception{"unexpected data structure \"" + it->first + "\" in \"table\""};
		}
	}
	// tableid as link text
	dst_s_replace(node_.data());
	// === link text ===
	dst_l_replace("</a></p>");
}

void webster_parser::process_array_srefs(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace("Synonyms see: ");
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) {
			dst[dst_start++] = ',';
			dst[dst_start++] = ' ';
		}
		dst_l_replace(R"xx(<a style="font-variant: small-caps;" href=")xx");
		// === link ===
		dst_l_replace(WEBSTER_SREFS_URL_PREFIX);
		dst[dst_start++] = ctx_.dict;
		dst[dst_start++] = '/';
		dst_s_replace(i.second.data());
		// === link ===
		dst_l_replace(R"xx(">)xx");
		dst_s_replace(i.second.data());
		dst_l_replace("</a>");
	}
}

void webster_parser::process_array_list(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst_l_replace("<ul>");
	for (auto&& i : node) {
		dst_l_replace("<li>");
		dst_s_replace(i.second.data());
		dst_l_replace("</li>");
	}
	dst_l_replace("</ul>");
}

void webster_parser::process_array_dt(string& dst, string::size_type& dst_start, pt::ptree& node) {
	// todo separator?
	// uint16_t n = 0;
	for (auto&& i : node) {
		auto it = i.second.begin();
		auto ds_t = data_structure_type(it->second.data());
		++it;
		auto& node_ = it->second;
		// if (n++) dst[dst_start++] = ' ';
		switch (ds_t) {
			case webster_data_structure::text:
				dst_s_replace(node_.data());
				break;
			case webster_data_structure::bnw:
				process_node_next2_bnw(dst, dst_start, node_);
				break;
			case webster_data_structure::ca:
				process_node_next2_ca(dst, dst_start, node_);
				break;
			case webster_data_structure::ri:
				process_array_next2_ri(dst, dst_start, node_);
				break;
			case webster_data_structure::wsgram:
				PROCESS_NODE_NEXT2_WSGRAM(dst, dst_start, node_);
				break;
			case webster_data_structure::snote:
				process_array_next2_snote(dst, dst_start, node_);
				break;
			case webster_data_structure::uns:
				process_array_next2_uns(dst, dst_start, node_);
				break;
			case webster_data_structure::vis:
				process_array_next2_vis(dst, dst_start, node_);
				break;
			case webster_data_structure::g:
				PROCESS_NODE_NEXT2_G(dst, dst_start, node_);
				break;
			case webster_data_structure::urefs:
				process_array_next2_urefs(dst, dst_start, node_);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" +
											 i.second.begin()->second.data() + "\" in \"dt\""};
		}
	}
}

void webster_parser::process_array_next2_snote(string& dst, string::size_type& dst_start, pt::ptree& node) {
	dst[dst_start++] = '\n';
	dst_l_replace("Note: ");
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) dst[dst_start++] = ' ';
		auto it = i.second.begin();
		auto ds_t = data_structure_type(it->second.data());
		++it;
		auto& node_ = it->second;
		switch (ds_t) {
			case webster_data_structure::t:
				dst_s_replace(node_.data());
				break;
			case webster_data_structure::ri:
				process_array_next2_ri(dst, dst_start, node_);
				break;
			case webster_data_structure::vis:
				process_array_next2_vis(dst, dst_start, node_);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" +
											 i.second.begin()->second.data() + "\" in \"snote\""};
		}
	}
}
// void webster_parser::process_node_next2_sen(string& dst, string::size_type& dst_start, pt::ptree& node) {
//    process_node_next2_sense(dst,dst_start,node);
//}

void webster_parser::process_node_next2_bs(string& dst, string::size_type& dst_start, pt::ptree& node) {
	auto it = node.begin();
	auto& sense_node = it->second;
	if (it->first != "sense")
		throw webster_data_exception{"unexpected data structure \"" + it->first + "\" in \"bs\""};
	process_node_next2_sense(dst, dst_start, sense_node);
}

void webster_parser::process_array_next2_pseq(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		auto it = i.second.begin();
		auto ds_t = data_structure_type(it->second.data());
		++it;
		auto& node_ = it->second;
		switch (ds_t) {
			case webster_data_structure::sense:
			case webster_data_structure::sen:
				process_node_next2_sense(dst, dst_start, node_);
				break;
			case webster_data_structure::bs:
				process_node_next2_bs(dst, dst_start, node_);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" +
											 i.second.begin()->second.data() + "\" in \"pseq\""};
		}
	}
}

void webster_parser::process_node_next2_sense(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	for (auto&& i : node) {
		auto ds_t = data_structure_type(i.first);
		if (n++) dst[dst_start++] = ' ';
		switch (ds_t) {
			case webster_data_structure::sn:
				PROCESS_NODE_SN(dst, dst_start, i.second);
				break;
			case webster_data_structure::dt:
				process_array_dt(dst, dst_start, i.second);
				break;
			case webster_data_structure::et:
				process_array_et(dst, dst_start, i.second);
				break;
			case webster_data_structure::ins:
				process_array_ins(dst, dst_start, i.second);
				break;
			case webster_data_structure::lbs:
				process_array_lbs(dst, dst_start, i.second);
				break;
			case webster_data_structure::prs:
				process_array_prs(dst, dst_start, i.second);
				break;
			case webster_data_structure::sdsense:
				process_node_sdsense(dst, dst_start, i.second);
				break;
			case webster_data_structure::sgram:
				PROCESS_NODE_SGRAM(dst, dst_start, i.second);
				break;
			case webster_data_structure::sls:
				process_array_sls(dst, dst_start, i.second);
				break;
			case webster_data_structure::vrs:
				process_array_vrs(dst, dst_start, i.second);
				break;
			case webster_data_structure::syn_list:
				process_array_syn_list(dst, dst_start, i.second);
				break;
			case webster_data_structure::rel_list:
				process_array_rel_list(dst, dst_start, i.second);
				break;
			case webster_data_structure::phrase_list:
				process_array_phrase_list(dst, dst_start, i.second);
				break;
			case webster_data_structure::near_list:
				process_array_near_list(dst, dst_start, i.second);
				break;
			case webster_data_structure::ant_list:
				process_array_ant_list(dst, dst_start, i.second);
				break;
			case webster_data_structure::bnote:
				PROCESS_NODE_BNOTE(dst, dst_start, i.second);
				break;
			case webster_data_structure::snotebox:
				process_array_snotebox(dst, dst_start, i.second);
				break;
			case webster_data_structure::phrasev:
				process_node_phrasev(dst, dst_start, i.second);
				break;
			case webster_data_structure::sphrasev:
				process_node_sphrasev(dst, dst_start, i.second);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"sense\""};
		}
	}
}

void webster_parser::process_node_sdsense(string& dst, string::size_type& dst_start, pt::ptree& node) {
	// todo separator??
	for (auto&& i : node) {
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::sd:
				PROCESS_NODE_SD(dst, dst_start, i.second);
				break;
			case webster_data_structure::et:
				process_array_et(dst, dst_start, i.second);
				break;
			case webster_data_structure::ins:
				process_array_ins(dst, dst_start, i.second);
				break;
			case webster_data_structure::lbs:
				process_array_lbs(dst, dst_start, i.second);
				break;
			case webster_data_structure::prs:
				process_array_prs(dst, dst_start, i.second);
				break;
			case webster_data_structure::sgram:
				PROCESS_NODE_SGRAM(dst, dst_start, i.second);
				break;
			case webster_data_structure::sls:
				process_array_sls(dst, dst_start, i.second);
				break;
			case webster_data_structure::vrs:
				process_array_vrs(dst, dst_start, i.second);
				break;
			case webster_data_structure::dt:
				process_array_dt(dst, dst_start, i.second);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"sdsense\""};
		}
	}
}

void webster_parser::process_array_sseq(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		uint16_t n = 0;
		for (auto&& j : i.second) {
			if (n++) dst[dst_start++] = ' ';
			auto it = j.second.begin();
			auto ds_t = data_structure_type(it->second.data());
			++it;
			auto& node_ = it->second;
			switch (ds_t) {
				case webster_data_structure::sense:
				case webster_data_structure::sen:
					process_node_next2_sense(dst, dst_start, node_);
					break;
				case webster_data_structure::pseq:
					process_array_next2_pseq(dst, dst_start, node_);
					break;
				case webster_data_structure::bs:
					process_node_next2_bs(dst, dst_start, node_);
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" +
												 j.second.begin()->second.data() + "\" in \"sseq\""};
			}
		}
	}
}

void webster_parser::process_array_def(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::vd:
					PROCESS_NODE_VD(dst, dst_start, j.second);
					dst_l_replace("<br>");
					break;
				case webster_data_structure::sls:
					process_array_sls(dst, dst_start, j.second);
					break;
				case webster_data_structure::sseq:
					process_array_sseq(dst, dst_start, j.second);
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first + "\" in \"def\""};
			}
		}
	}
}

void webster_parser::process_node_next2_bnw(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) dst[dst_start++] = ' ';
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::pname:
			case webster_data_structure::sname:
				dst_s_replace(i.second.data());
				break;
			case webster_data_structure::altname:
				PROCESS_NODE_(dst, dst_start, i.second, "<i>", "</i>");
				break;
			case webster_data_structure::prs:
				process_array_prs(dst, dst_start, i.second);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"bnw\""};
		}
	}
	dst[dst_start++] = ',';
	dst[dst_start++] = ' ';
}

void webster_parser::process_node_next2_ca(string& dst, string::size_type& dst_start, pt::ptree& node) {
	// uint16_t n=0;
	for (auto&& i : node) {
		// if(n++)  dst[dst_start++]=' ';
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::intro:
				dst_s_replace(i.second.data());
				break;
			case webster_data_structure::cats:
				for (auto&& j : i.second) {
					pt::ptree* node_catref{};
					for (auto&& k : j.second) {
						if (k.first == "catref") {
							node_catref = &k.second;
							break;
						}
					}

					for (auto&& k : j.second) {
						auto ds_t0 = data_structure_type(k.first);
						switch (ds_t0) {
							case webster_data_structure::cat:
								if (node_catref) {
									dst_l_replace(R"xx(<a style="font-style: italic;" href=")xx");
									// === link ===
									dst_l_replace(WEBSTER_CATREF_URL_PREFIX);
									dst[dst_start++] = ctx_.dict;
									dst[dst_start++] = '/';
									dst_s_replace(k.second.data());
									// === link ===
									dst_l_replace(R"xx(">)xx");
									dst_s_replace(k.second.data()); // link text
									dst_l_replace("</a>");
								} else {
									PROCESS_NODE_(dst, dst_start, k.second, "<i>", "</i>");
								}
								break;
							case webster_data_structure::catref:
								break;
							case webster_data_structure::pn:
								PROCESS_NODE_(dst, dst_start, k.second, "(", ") ");
								break;
							case webster_data_structure::prs:
								process_array_prs(dst, dst_start, k.second);
								break;
							case webster_data_structure::psl:
								PROCESS_NODE_PSL(dst, dst_start, k.second);
								break;
							default:
								throw webster_data_exception{"unexpected data structure \"" + k.first +
															 "\" in \"cats\""};
						}
					}
				}
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"ca\""};
		}
	}
}

void webster_parser::process_node_altprs(string& dst, string::size_type& dst_start, pt::ptree& node) {
	if (ctx_.print) return;
	// pronunciation objects separator with default value
	string separator{", "};
	// search "pun"
	for (auto&& i : node) {
		for (auto&& j : i.second) {
			if (j.first == "pun") {
				separator = j.second.data() + ' ';
				goto separator_got;
			}
		}
	}
separator_got:
	dst[dst_start++] = '\\';
	uint16_t n = 0;
	for (auto&& i : node) {
		if (n++) dst_s_replace(separator);
		for (auto&& j : i.second) {
			auto ds_t = data_structure_type(j.first);
			switch (ds_t) {
				case webster_data_structure::mw:
					dst_s_replace(j.second.data());
					break;
				case webster_data_structure::l:
					dst_s_replace(j.second.data());
					dst[dst_start++] = ' ';
					break;
				case webster_data_structure::l2:
					dst[dst_start++] = ' ';
					dst_s_replace(j.second.data());
					break;
				case webster_data_structure::ipa:
					PROCESS_NODE_IPA(dst, dst_start, j.second);
					break;
				case webster_data_structure::wod:
					PROCESS_NODE_WOD(dst, dst_start, j.second);
					break;
				case webster_data_structure::pun:
					break;
				case webster_data_structure::sound:
					// <a href="url">link text</a>
					dst_l_replace(R"xx( <a href=")xx");
					// build url
					// reference https://www.dictionaryapi.com/products/json#sec-2.prs
					// structure https://media.merriam-webster.com/soundc11/[subdirectory]/[base
					// filename].wav
					{
						dst_l_replace(WEBSTER_S_URL_PREFIX_QT);
						for (auto&& k : j.second) {
							if (k.first == "audio") {
								auto& audio_v = k.second.data();
								// url subdirectory
								switch (audio_v[0]) {
									case 'b':
										if (audio_v.size() > 2 and audio_v[1] == 'i' and audio_v[2] == 'x') {
											dst_l_replace("bix/");
										} else {
											dst[dst_start++] = audio_v[0];
											dst[dst_start++] = '/';
										}
										break;
									case 'g':
										if (audio_v.size() > 1 and audio_v[1] == 'g') {
											dst_l_replace("gg/");
										} else {
											dst[dst_start++] = audio_v[0];
											dst[dst_start++] = '/';
										}
										break;
									default:
										if (('a' <= audio_v[0] and audio_v[0] <= 'z') or
											('A' <= audio_v[0] and audio_v[0] <= 'Z')) {
											dst[dst_start++] = audio_v[0];
											dst[dst_start++] = '/';
										} else {
											dst_l_replace("number/");
										}
										break;
								}
								// url filename
								dst_s_replace(audio_v);
								dst_l_replace(".wav");
								break;
							}
						}
					}
					dst_l_replace("\">");
					// link text
					dst_l_replace(WEBSTER_S_LINK_TEXT);
					dst_l_replace("</a>");
					break;
				default:
					throw webster_data_exception{"unexpected data structure \"" + j.first +
												 "\" in \"altprs\""};
			}
		}
	}
	dst[dst_start++] = '\\';
}

void webster_parser::process_node_phrasev(string& dst, string::size_type& dst_start, pt::ptree& node) {
	auto& node_phr = node.begin()->second;
	for (auto&& i : node_phr) {
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::pva:
				PROCESS_NODE_PVA(dst, dst_start, i.second);
				break;
			case webster_data_structure::pvl:
				PROCESS_NODE_PVL(dst, dst_start, i.second);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"phr\""};
		}
	}
}

void webster_parser::process_node_sphrasev(string& dst, string::size_type& dst_start, pt::ptree& node) {
	for (auto&& i : node) {
		auto ds_t = data_structure_type(i.first);
		switch (ds_t) {
			case webster_data_structure::phrs:
				for (auto&& j : i.second) {
					for (auto&& k : j.second) {
						auto ds_t = data_structure_type(k.first);
						switch (ds_t) {
							case webster_data_structure::pva:
								PROCESS_NODE_PVA(dst, dst_start, k.second);
								break;
							case webster_data_structure::pvl:
								PROCESS_NODE_PVL(dst, dst_start, k.second);
								break;
							default:
								throw webster_data_exception{"unexpected data structure \"" + k.first +
															 "\" in \"phrs\""};
						}
					}
				}
				break;
			case webster_data_structure::phsls:
				process_array_phsls(dst, dst_start, i.second);
				break;
			default:
				throw webster_data_exception{"unexpected data structure \"" + i.first + "\" in \"sphrasev\""};
		}
	}
}

void webster_parser::process_array_phsls(string& dst, string::size_type& dst_start, pt::ptree& node) {
	uint16_t n = 0;
	dst_l_replace("<i>");
	for (auto&& i : node) {
		if (n++) {
			dst[dst_start++] = ',';
			dst[dst_start++] = ' ';
		}
		dst_s_replace(i.second.data());
	}
	dst_l_replace("</i>");
}

void webster_parser::parse_JSON(string_view src_view, string& dst, string::size_type& dst_start) {
	boost::iostreams::stream<boost::iostreams::array_source> stream{src_view.data(), src_view.size()};
	return parse_JSON(stream, dst, dst_start);
}

void webster_parser::parse_JSON(std::istream& src_strm, string& dst, string::size_type& dst_start) {
	// convert JSON to ptree
	pt::ptree pt_;
	pt::read_json(src_strm, pt_);
	// test if it's a word suggest
	bool suggest_flag = true;
	for (auto&& i : pt_) {
		if (!i.second.get_child("meta", {}).empty()) {
			suggest_flag = false;
			break;
		}
	}
	if (suggest_flag) {
		dst_l_replace(WEBSTER_ENTRY_OPEN);
		dst_l_replace("Suggestion: ");
		uint16_t n = 0;
		for (auto&& i : pt_) {
			if (i.first == "") {
				if (n++) {
					dst[dst_start++] = ',';
					dst[dst_start++] = ' ';
				}
				auto& str = i.second.data();
				char buf[128]{};
				auto view_enc = url_encode_view((const unsigned char*)str.data(), str.size(), buf);
				// no jump no prefix '#'
				dst_l_replace(R"xx(<a href=")xx" WEBSTER_SUGGEST_SCHEME);
				dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
				dst[dst_start++] = ctx_.dict;
				dst[dst_start++] = WEBSTER_IDATT_SEPARATOR;
				dst_s_replace(view_enc);
				dst_l_replace(R"xx(">)xx");
				dst_s_replace(str);
				dst_l_replace("</a>");
			}
		}
		dst_l_replace(WEBSTER_ENTRY_CLOSE);
		return;
	}
	// a_link mapping by read every meta node in the document
	ctx_.do_a_link_map(pt_);
	// ptree to html4
	// dst_l_replace(WEBSTER_OPEN);
	for (auto&& i : pt_) {
		ctx_.reset_entry_ctx();
		auto& entry = i.second;
		// todo ? (remove) read or not read meta
		// process_entry_read_meta(entry);
		deque<node_range> traversal_stack;
		// parse RUNNING TEXT of each node
		// ptree preorder traversal: initialization
		// todo check before parse?
		if (!entry.data().empty()) parse_RUNNING_TEXT(entry.data());
		auto begin = entry.begin();
		auto end = entry.end();
		if (begin != end) traversal_stack.emplace_front(begin, end);
		// ptree preorder traversal: iteration
		while (!traversal_stack.empty()) {
			auto& f = traversal_stack.front();
			// true if traversal_stack does not emplace a new node
			bool same_front = true;
			while (f.c != f.end) {
				// todo check before parse?
				if (!f.c->second.data().empty()) parse_RUNNING_TEXT(f.c->second.data());
				auto child_c = f.c->second.begin();
				auto child_end = f.c->second.end();
				++(f.c);
				if (child_c != child_end) {
					traversal_stack.emplace_front(child_c, child_end);
					same_front = false;
					break;
				}
			}
			if (same_front) traversal_stack.pop_front();
		}
		process_entry(dst, dst_start, i.second);
	}
	// dst_l_replace(WEBSTER_CLOSE);
	clear_temp();
	return;
}

} // namespace dict_core